﻿using UnityEngine;
using System.Collections;

public class TranslationTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.RightArrow))
			transform.Translate(new Vector3(0.1f, 0, 0));
		else if(Input.GetKeyDown(KeyCode.LeftArrow))
			transform.Translate(new Vector3(-0.1f, 0, 0));
	}
}
