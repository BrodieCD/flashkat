﻿using UnityEngine;
 
public class GradientTest : MonoBehaviour 
{ 
	[Range(0, 1)]
	public float Ratio = 0;

    public Material GradientMaterial;

	public Color BaseColorLeft = Color.yellow;
    public Color BaseColorRight = Color.red;

	private const float _RATIO_TO_FULL_COLOR = 2;
	private const float _HALF_RATIO = 0.5f;

	private Color _lerpedColorLeft = Color.yellow;
    private Color _lerpedColorRight = Color.red;

    void Start () 
	{
		_lerpedColorLeft = BaseColorLeft;
		_lerpedColorRight = BaseColorRight;
		updateMaterial();
    }

    void Update() 
	{
		if(Ratio <= _HALF_RATIO)
		{
        	_lerpedColorLeft = Color.Lerp(BaseColorLeft, BaseColorRight, Ratio * _RATIO_TO_FULL_COLOR);  // So at HALF_RATIO we have full color of 1
			_lerpedColorRight = BaseColorLeft;
		}
		else 
		{
			_lerpedColorLeft = BaseColorRight;
			_lerpedColorRight = Color.Lerp(BaseColorLeft, BaseColorRight, (Ratio - _HALF_RATIO) * _RATIO_TO_FULL_COLOR); // So at 1 we have HALF_RATIO and full color of 1
		}

 		updateMaterial();
    }

	private void updateMaterial()
	{
		GradientMaterial.SetColor("_Color", _lerpedColorLeft);
		GradientMaterial.SetColor("_Color2", _lerpedColorRight);
	}
 }