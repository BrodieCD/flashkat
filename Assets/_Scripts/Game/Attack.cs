﻿using UnityEngine;

[System.Serializable]
public class Attack
{
	public enum Names
	{
		None,
		PunchL,
		PunchR,
		KickL,
		KickR,
		FlipKick,
		Dash,
		FlyingKick,
		SlidePunch,
		Raiden,
		ElectricShoot,
		DragonRaidenPunch1,
		DragonRaidenPunch2,
		DragonRaidenPunch3,
		FlamingFlash1,
		FlamingFlash2,
		FlamingFlash3,
		DragonKick,
		RageKick,
		TornadoKick
	}

	public Names Name = Names.None;

	public float Damage = 1;
	public float StunTime = 0.5f;
	public Vector2 KnockBackVelocity = new Vector2(1, 0);
	public float KnockBackFriction = 0.01f;

	public Vector2 Velocity = new Vector2(1, 0);
	public float Friction = 0.1f;

	public FxAndSounds.FxsNames HitFx;

	public FxAndSounds.SoundsNames SwingOneShotSoundFx;
	public float SwingOneShotSoundVol = 1;

	public FxAndSounds.SoundsNames SwingManualSoundFx;	
	public float SwingManualSoundVol = 1;
	
	public FxAndSounds.SoundsNames ImpactSoundFx;
	public float ImpactSoundVol = 1;

	public AnimTriggers AnimTrigger;
}
