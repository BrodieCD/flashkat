using System; 
using UnityEngine;

public class SignalDamageArea : Signal<Bounds, float>
{
	public SignalDamageArea(String eventName) : base(eventName){}
	
	public void EmitParams(object dispatcher, Bounds damageArea, float damage)
	{
		Emit(dispatcher, damageArea, damage);
	}
}

public class SignalSceneName : Signal<String>
{
	public SignalSceneName(String eventName) : base(eventName){}
	
	public void EmitParams(object dispatcher, String sceneName)
	{
		Emit(dispatcher, sceneName);
	}	
}

public class SignalBulletShot : Signal<GameObject, Vector3, Vector3, float, float, float, float, BulletBehaviorOption>  
{
	public SignalBulletShot(String eventName) : base(eventName) {}	
	
	public void EmitParams(object dispatcher, GameObject bulletBodyPrefab, Vector3 startPoint, Vector3 endPoint, float speed, float lifeTime, float acceleration, float rotationSpeed, BulletBehaviorOption bulletBehavior)
	{
		Emit(dispatcher, bulletBodyPrefab, startPoint, endPoint, speed, lifeTime, acceleration, rotationSpeed, bulletBehavior);
	}
	
}

public class SignalReusableBulletShot : Signal<Vector3, Vector3, BulletNames, GetReusableBulletCallback>
{
	public SignalReusableBulletShot(String eventName) : base(eventName)
	{
		
	}
	
	public void EmitParams(object dispatcher, Vector3 position, Vector3 direction, BulletNames bulletName, GetReusableBulletCallback getBulletCallback)
	{
		Emit(dispatcher, position, direction, bulletName, getBulletCallback);
	}	
}	
