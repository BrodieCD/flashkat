﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class LevelCompletionJacket : MonoBehaviour 
{
	private	SpriteRenderer _spriteRenderer;

	void Awake()
	{
		_spriteRenderer = GetComponent<SpriteRenderer>();
		gameObject.SetActive(false);
	}

	void OnEnable()
	{
		FxManager.EnableFx(this, FxAndSounds.FxsNames.FireworksYellow, transform.position, Quaternion.identity);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(_spriteRenderer.enabled)
		{
			print("Got Jacket");
			_spriteRenderer.enabled = false;
		}
	}
}
