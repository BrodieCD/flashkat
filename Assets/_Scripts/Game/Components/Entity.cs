﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

    public SpawnablePrefabs.PrefabNames prefabName;
    EntityData myData;

    [Header("Bounding variables")]
    public bool boundPosition;
    SpriteRenderer mySpriteRenderer;
    float boundLeft;
    float boundRight;
    public float extendFactor;

    void RegisterToEntityDictionary()
    {
        myData = new EntityData(EntityManager.instance.GenerateUniqueKey(gameObject.name), SpawnablePrefabs.instance.FindPrefabUsingPrefabName(prefabName), transform.position, transform.eulerAngles);
        EntityManager.instance.AddEntityDataToDictionary(myData.dictionaryKey, myData);
        gameObject.name = myData.dictionaryKey;
    }

    void Start()
    {
        RegisterToEntityDictionary();

        if (boundPosition)
        {
            mySpriteRenderer = GetComponentInChildren<SpriteRenderer>();
            LevelBounds.GetSpriteLimits(mySpriteRenderer, extendFactor, out boundLeft, out boundRight);
        }
    }

    void Update()
    {
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, boundLeft, boundRight);
        transform.position = pos;
    }

    void OnDestroy()
    {
        EntityManager.instance.RemoveEntityFromDictionary(myData.dictionaryKey);
    }
}
