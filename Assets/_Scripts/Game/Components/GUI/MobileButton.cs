﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MobileButton : MobileArea
{
	public enum Names
	{
		Punch,
		Kick,
		Special,
		Defense,
		Jump,
		Options,
		Stick,
	}

	public Names ButtonName;
	public Image IconChild;

	public Sprite[] SpritesPerSet;

	public bool IsActive 
	{ 
		get { return (gameObject.activeSelf);  }
		set { gameObject.SetActive(value); }
	}

	public bool IsIdle { get { return (State == ButtonStates.Idle); } }
	public bool IsDown { get { return (State == ButtonStates.Down); } }
	public bool IsPressed { get { return (State == ButtonStates.Pressed); } }
	public bool IsUp { get { return (State == ButtonStates.Up); } }

	public ButtonStates State { get { return _state; } }	

	private const float  _BUTTON_ACTIVE_ALPHA = 0.75f;

	private float _baseImageAlpha = 0.0f;
	private ButtonStates _state = ButtonStates.Idle; 

	override public void Awake()
	{
		base.Awake();
		_baseImageAlpha = _image.color.a;
	}
	
	public void SetDown()
	{
		//print("ButtonDown: " + button.ToString());
		_state = ButtonStates.Down;
		_image.color = new Color(_image.color.r, _image.color.g, _image.color.b, _BUTTON_ACTIVE_ALPHA);
	}
	
//	public void SetPressed()
//	{
//		//print("ButtonPressed: " + button.ToString());
//		_state = ButtonStates.Pressed;		
//	}
	
	public void SetUp()
	{
		//print("ButtonUp: " + button.ToString());
		_state = ButtonStates.Up;
		_image.color = new Color(_image.color.r, _image.color.g, _image.color.b, _baseImageAlpha);
	}
	
	public void HandleTransitoryStates()
	{
		if(IsDown)
			_state = ButtonStates.Pressed;
		else if(IsUp)
			_state = ButtonStates.Idle;
	}
}
