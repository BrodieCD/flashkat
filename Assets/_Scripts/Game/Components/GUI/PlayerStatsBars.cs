﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStatsBars : MonoBehaviour 
{
	public Image EnergyEmptyBar;
	public float EnergyEmptyBarLeftLimit;
	public float EnergyEmptyBarRightLimit;

	public Image AngerEmptyBar;
	public float AngerEmptyBarLeftLimit;
	public float AngerEmptyBarRightLimit;

	public Animator EnergyBarBlinkAnimator;

	private float _energyToBarPosFactor;
	private float _angerToBarPosFactor;

	public void ResetEnergyBar(float currentEnergy, float MaxEnergy)
	{
		_energyToBarPosFactor = (EnergyEmptyBarRightLimit - EnergyEmptyBarLeftLimit) / MaxEnergy;
		UpdateEnergyBar(currentEnergy);		
	}

	public void ResetAngerBar(float currentAnger, float MaxAnger)
	{
		_angerToBarPosFactor = (AngerEmptyBarRightLimit - AngerEmptyBarLeftLimit) / MaxAnger;
		UpdateAngerBar(currentAnger);	
	}
	
	public void UpdateEnergyBar(float currentEnergy)
	{
		Vector3 pos = EnergyEmptyBar.rectTransform.localPosition;
		EnergyEmptyBar.rectTransform.localPosition = new Vector3(currentEnergy * _energyToBarPosFactor, pos.y, pos.z);  	
		//print("EnergyEmptyBar pos: " + EnergyEmptyBar.rectTransform.localPosition.x);
	}

	public void UpdateAngerBar(float currentAnger)
	{
		Vector3 pos = AngerEmptyBar.rectTransform.localPosition;
		AngerEmptyBar.rectTransform.localPosition = new Vector3(currentAnger * _angerToBarPosFactor, pos.y, pos.z);  	
		//print("AngerEmptyBar pos: " + AngerEmptyBar.rectTransform.localPosition.x);
	}

	public void HandleNotEnoughEnergy()
	{
		EnergyBarBlinkAnimator.SetTrigger("Blink");
	}
}
