﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject[] screens;

    public Item[] items;

    [Header("Menu Element References")]
    public Slider musicSlider;
    public Slider sfxSlider;
    public Text marsBucksText;

    void Start()
    {
        musicSlider.value = PlayerPrefs.GetFloat("MusicVol", 1);
        sfxSlider.value = PlayerPrefs.GetFloat("SFXVol", 1);
    }

    void Update()
    {
        marsBucksText.text = "You have " + Save.instance.saves[Save.instance.currentSaveIndex].marsBucks.ToString() + " mars bucks.";
    }

    void UpdateMarsMarketItems()
    {
        for (int i = 0; i < items.Length; i++)
        {
            int timesPurchased = Save.instance.saves[Save.instance.currentSaveIndex].timesPurchasedItems[i];
            items[i].SetTexts(timesPurchased);
        }
    }

    #region buttons
    public void OpenScreen(int index)
    {
        for (int i = 0; i < screens.Length; i ++)
        {
            if (i != index)
            {
                screens[i].SetActive(false);
            }
            else if (i == index)
            {
                screens[i].SetActive(true);
            }
        }
    }

    public void OpenPage(GameObject show)
    {
        show.SetActive(true);
    }
    public void ClosePage(GameObject hide)
    {
        hide.SetActive(false);
    }

    public void SelectDifficulty(int difficulty)
    {
        Save.instance.saves[Save.instance.currentSaveIndex].difficultyValue = difficulty;
    }

    public void SelectStage(int stage)
    {
        if (stage > Save.instance.saves[Save.instance.currentSaveIndex].highestLevelCompleted)
            return;

		int nextStage = stage + 1;

		GameManager.Level = nextStage;

        SceneManager.LoadScene(nextStage);
    }

    public void SelectSave(int save)
    {
        Save.instance.currentSaveIndex = save;
        UpdateMarsMarketItems();
    }

    public void ClearCurrentSaveSlot()
    {
        Save.instance.saves[Save.instance.currentSaveIndex].Clear();
    }

    public void BuyItem(int item)
    {
        bool purchased = items[item].purchased;
        bool canBePurchasedMoreThanOnce = items[item].canBePurchasedMoreThanOnce;
        if (purchased && canBePurchasedMoreThanOnce || !purchased && !canBePurchasedMoreThanOnce || !purchased && canBePurchasedMoreThanOnce)
        {
            if (Save.instance.saves[Save.instance.currentSaveIndex].marsBucks >= items[item].cost)
            {
                Debug.Log("You bought an item for " + items[item].cost);
                
                Save.instance.saves[Save.instance.currentSaveIndex].marsBucks -= items[item].cost;
                Save.instance.saves[Save.instance.currentSaveIndex].timesPurchasedItems[item]++;

                items[item].timesPurchased++;
                items[item].purchased = true;
                items[item].SetTexts();
            } else {
                Debug.Log("Cannot afford");
            }
        }  
    }

    public void UpdateSFXVolume()
    {
        PlayerPrefs.SetFloat("SFXVol", sfxSlider.value);
        AudioManager.instance.UpdateVolume();
    }

    public void UpdateMusicVolume()
    {
        PlayerPrefs.SetFloat("MusicVol", musicSlider.value);
        AudioManager.instance.UpdateVolume();
    }

    #endregion

    [System.Serializable]
    public struct Item
    {
        public bool purchased;
        public bool canBePurchasedMoreThanOnce;
        public int timesPurchased;

        public string name;
        public int cost;

        public Text nameText;
        public Text costText;

        public void SetTexts(int _timesPurchased)
        {
            if (_timesPurchased >= 1)
            {
                purchased = true;
                timesPurchased = _timesPurchased;
            } else {
                purchased = false;
                timesPurchased = 0;
            }

            if (!purchased)
            {
                nameText.text = name;
                costText.text = cost.ToString();
            }else if (purchased) {
                if (canBePurchasedMoreThanOnce)
                {
                    nameText.text = "X" + timesPurchased.ToString();
                    costText.text = cost.ToString();
                } else {
                    nameText.text = "PURCHASED ";
                    costText.text = "";
                }
            } 
        }

        public void SetTexts()
        {
            if (!purchased)
            {
                nameText.text = name;
                costText.text = cost.ToString();
            } else if (purchased) {
                if (canBePurchasedMoreThanOnce)
                {
                    nameText.text = "X" + timesPurchased.ToString();
                    costText.text = cost.ToString();
                }
                else
                {
                    nameText.text = "PURCHASED ";
                    costText.text = "";
                }
            }

        }
    }
}


