﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Image))]
public class MobileArea : MonoBehaviour 
{
	protected Image _image;
	protected RectTransform _rectTransform;
	protected Rect _screenRect;

	private bool _isScreenRectReady = false;

	public Rect ScreenRect { get{ return _screenRect; } }

	virtual public void Awake()
	{
		_image = GetComponent<Image>();
		_rectTransform = GetComponent<RectTransform>();

		//UpdateScreenRect();  //Transformation to world coordinates returns zero if called here
		//FwSignalEvents.Instance.OnPostStart.Connect(this, HandlePostStart);
	}

	void OnDestroy()
	{
		//FwSignalEvents.Instance.OnPostStart.Disconnect(this);
	}

//	public void HandlePostStart(object dispatch)
//	{
//		UpdateScreenRect();
//	}

	void Update()
	{
		if(!_isScreenRectReady)
		{
			UpdateScreenRect();

			if(_screenRect.width != 0)
			{
				_isScreenRectReady = true;
				//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "_screenRect " + _screenRect.ToString(), false);
			}
		}
	}

	public bool Contains(Vector2 pos)
	{
		return _screenRect.Contains(pos);
	}

	public void SetPosition(Vector2 screenPos)
	{
		UnityUiUtils.SetRectTransformPosFromScreenPos(_rectTransform, screenPos);
		UpdateScreenRect();
	}

	public void UpdateScreenRect()
	{
		//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "_rectTransform " + _rectTransform.rect.ToString(), false);
		_screenRect = UnityUiUtils.GetScreenRectFromRectTransform(_rectTransform);
		//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "_screenRect " + _screenRect.ToString(), false);
	}
}
