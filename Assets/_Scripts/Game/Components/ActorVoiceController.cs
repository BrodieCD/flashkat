﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class ActorVoiceController : MonoBehaviour 
{
	public List<VoiceData> HurtSounds;
	public List<VoiceData> AttackSounds;

	private AudioSource _audio;

	void Awake()
	{
		_audio = GetComponent<AudioSource>();
		_audio.loop = false;
	} 

	public void PlayRandomHurtSound()
	{		
		playRandomVoice(HurtSounds);
	}

	public void PlayRandomAttackSound()
	{
		playRandomVoice(AttackSounds);
	}

	private void playRandomVoice(List<VoiceData> voiceDatas)
	{
		playVoice(GeneralUtils.GetRandomItemFromList<VoiceData>(voiceDatas));
	}

	private void playVoice(VoiceData voiceData)
	{
		_audio.Stop();

		_audio.pitch = Random.Range(voiceData.MinPitch, voiceData.MaxPitch);
		_audio.volume = Random.Range(voiceData.MinVolume, voiceData.MaxVolume);

		_audio.clip = voiceData.Clip;
		_audio.Play();
	}

	[System.Serializable]
	public class VoiceData
	{
		public AudioClip Clip;
		public float MinVolume = 0.3f;
		public float MaxVolume = 0.8f;
		public float MinPitch = 0.8f;
		public float MaxPitch = 1.2f;
	}
}
