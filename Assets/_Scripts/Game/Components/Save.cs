﻿using UnityEngine;
using System.Collections;

public class Save : MonoBehaviour {

    public int currentSaveIndex = 0;

    public static Save instance;
    [SerializeField] public SaveData[] saves;

    void Awake()
    {
        DontDestroyOnLoad(this);
        instance = this;

        for (int i = 0; i < saves.Length; i++)
        {
            saves[i].timesPurchasedItems = new int[16];
            saves[i].uniqueSaveNumber = i;
            saves[i].GetData();
        }
    }
    
   void OnApplicationQuit()
    {
        for (int i = 0; i < saves.Length; i ++)
        {
            saves[i].StoreData();
        }
    }

    [System.Serializable]
    public struct SaveData
    {
        public int uniqueSaveNumber;

        //Enemy
        public int numberOfEnemiesKilled;

        //Player
        public int numberOfJumps;

        //Game
        public int difficultyValue;
        public int highestLevelCompleted;
        public int marsBucks;
        public int[] timesPurchasedItems;

        //Methods
        public void Clear()
        {
            //Enemy
            numberOfEnemiesKilled = 0;

            //Player
            numberOfJumps = 0;

            //Game
            difficultyValue = 0;
            highestLevelCompleted = 0;
            marsBucks = 0;

            for (int i = 0; i < timesPurchasedItems.Length; i++)
            {
                timesPurchasedItems[i] = 0;
            }

            StoreData();
        }

        public void StoreData()
        {
            //Enemy
            PlayerPrefs.SetInt("_numberOfEnemiesKilled" + uniqueSaveNumber.ToString(), numberOfEnemiesKilled);

            //Player
            PlayerPrefs.SetInt("_numberOfJumps" + uniqueSaveNumber.ToString(), numberOfJumps);

            //Game
            PlayerPrefs.SetInt("_difficultyValue" + uniqueSaveNumber.ToString(), difficultyValue);
            PlayerPrefs.SetInt("_highestLevelCompleted" + uniqueSaveNumber.ToString(), highestLevelCompleted);
            PlayerPrefs.SetInt("_marsBucks" + uniqueSaveNumber.ToString(), marsBucks);

            for (int i = 0; i < timesPurchasedItems.Length; i++)
            {
                PlayerPrefs.SetInt("_timesPurchasedItem" + uniqueSaveNumber.ToString() + i.ToString(), timesPurchasedItems[i]);
            }
        }

        public void GetData()
        {
            //Enemy
            numberOfEnemiesKilled = PlayerPrefs.GetInt("_numberOfEnemiesKilled" + uniqueSaveNumber.ToString(), 0);

            //Player
            numberOfJumps = PlayerPrefs.GetInt("_numberOfJumps" + uniqueSaveNumber.ToString(), 0);

            //Game
            difficultyValue = PlayerPrefs.GetInt("_difficultyValue" + uniqueSaveNumber.ToString(), 0);
            highestLevelCompleted = PlayerPrefs.GetInt("_highestLevelCompleted" + uniqueSaveNumber.ToString(), 0);
            marsBucks = PlayerPrefs.GetInt("_marsBucks" + uniqueSaveNumber.ToString(), 0);

            for (int i = 0; i < timesPurchasedItems.Length; i ++)
            {
                timesPurchasedItems[i] = PlayerPrefs.GetInt("_timesPurchasedItem" + uniqueSaveNumber.ToString() + i.ToString(), 0);
            }
        }
    }
}
