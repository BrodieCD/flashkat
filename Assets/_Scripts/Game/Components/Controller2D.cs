﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (BoxCollider2D))]
public class Controller2D : RaycastController {

    /*
    float maxClimbAngle = 80;
    float maxDescendAngle = 80;
    */
	public bool UseHorizontalLimits = false;

    public CollisionInfo Collisions;
	public bool UsesInput;
    public bool DeactivateHorizontalCollisionRaycasting;
    public float FallingThroughPlatformDuration = 0.125f;

	public float LimitExtentFactor = 0.5f;
	public SpriteRenderer UserSpriteRenderer;

    [HideInInspector]
    public Vector2 PlayerInput;

	public bool IsGrounded{ get { return Collisions.Below; } }

	private float _leftLimit;
	private float _rightLimit;

    public override void Start()
    { 
        base.Start();
        Collisions.FaceDir = 1;

		if(UseHorizontalLimits)
			LevelBounds.GetSpriteLimits(UserSpriteRenderer, LimitExtentFactor, out _leftLimit, out _rightLimit);
    }

	public void Move(Vector3 movement, int facing)
	{
		Move(movement, Vector2.zero, facing);
	}

	public void Move(Vector3 movement, Vector2 playerInput, int facing, bool isJumpButtonDown = false, bool standingOnPlatform = false, bool isUsingGravity = true)
    {
        UpdateRaycastOrigins();
        Collisions.Reset();
        Collisions.MovementOld = movement;

		if(UsesInput)
        	PlayerInput = playerInput;

        if (movement.x != 0)
        {
            Collisions.FaceDir = (int)Mathf.Sign(movement.x);
        }
		
        /*
        if (movement.y < 0)
        {
            DescendSlope(ref movement);
        }
        */

        HandleHorizontalCollisions(ref movement, isJumpButtonDown, isUsingGravity);
        if (movement.y != 0)
        {
			HandleVerticalCollisions(ref movement, isJumpButtonDown);
        }

		if(facing == GameConstants.DIR_LEFT)  //This is necessary since Unity 5.4.0f3 as movement is affected by scale. 
			movement.x *= -1;

        transform.Translate(movement);

        if (standingOnPlatform)
        {
            Collisions.Below = true;
        }

		if(UseHorizontalLimits)
		{
			Vector3 pos = transform.position;
			pos.x = Mathf.Clamp(pos.x, _leftLimit, _rightLimit);
			transform.position = pos;
		}
    }

	void HandleHorizontalCollisions(ref Vector3 movement, bool isJumpButtonDown, bool isUsingGravity)
    {
        if (!DeactivateHorizontalCollisionRaycasting)
        {
            float directionX = Collisions.FaceDir;
            float rayLength = Mathf.Abs(movement.x) + skinWidth;

            if (Mathf.Abs(movement.x) < skinWidth)
            {
                rayLength = 2 * skinWidth;
            }

            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

                if (hit)
                {
					string colliderTag = hit.collider.tag;
	
                    if (hit.distance == 0)
                    {
                        continue;
                    }

                    if (colliderTag == "Building")
                    {
                        HandleVerticalCollisions(ref movement, isJumpButtonDown);
                        if (Collisions.Below || !isUsingGravity)
                            continue;
                    }

                    /*
                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

                    if (i == 0 && slopeAngle <= maxClimbAngle)
                    {
                        if (collisions.descendingSlope)
                        {
                            collisions.descendingSlope = false;
                            movement = collisions.velocityOld;
                        }
                        float distanceToSlopeStart = 0;
                        if (slopeAngle != collisions.slopeAngleOld)
                        {
                            distanceToSlopeStart = hit.distance - skinWidth;
                            movement.x -= distanceToSlopeStart * directionX;
                        }
                        ClimbSlope(ref movement, slopeAngle);
                        movement.x += distanceToSlopeStart * directionX;
                    }


                    if (!collisions.climbingSlope  || slopeAngle > maxClimbAngle )
                    {

                        if (collisions.climbingSlope)
                        {
                            movement.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(movement.x);
                        }
                        */
                    movement.x = (hit.distance - skinWidth) * directionX;
                    rayLength = hit.distance;

					if(colliderTag != "Through")
					{
                    	Collisions.Left = directionX == -1;
                    	Collisions.Right = directionX == 1;
					}
                    //}
                }
            }
        }
    }

	void HandleVerticalCollisions(ref Vector3 movement, bool isPressingDown)
    {
        float directionY = Mathf.Sign(movement.y);
        float rayLength = Mathf.Abs(movement.y) + skinWidth;

        for (int i = 0; i < verticalRayCount; i++)
        {

            Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
            rayOrigin += Vector2.right * (verticalRaySpacing * i + movement.x);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

            if (hit)
            {
                if (hit.collider.tag == "Through")
                {
                    if (directionY == 1 || hit.distance == 0)
                    {
                        continue;
                    }
                    if (Collisions.FallingThroughPlatform)
                    {
                        continue;
                    }

                    if (UsesInput && isPressingDown && (PlayerInput.y == -1))
                    {
                        Collisions.FallingThroughPlatform = true;
                        Invoke("ResetFallingThroughPlatform", FallingThroughPlatformDuration);
                        continue;
                    }
                }

                if (hit.collider.tag == "Building")
                {
                    if (directionY == -1 || hit.distance == 0)
                        continue;
                }

                movement.y = (hit.distance - skinWidth) * directionY;
                rayLength = hit.distance;

                /*
                if (collisions.climbingSlope)
                {
                    movement.x = movement.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(movement.x);
                }
                */

                Collisions.Below = directionY == -1;
                Collisions.Above = directionY == 1;
            }
        }
        /*
        if (collisions.climbingSlope)
        {
            float directionX = Mathf.Sign(movement.x);
            rayLength = Mathf.Abs(movement.x) + skinWidth;
            Vector2 rayOrigin = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + Vector2.up * movement.y;
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != collisions.slopeAngle)
                {
                    movement.x = (hit.distance - skinWidth) * directionX;
                    collisions.slopeAngle = slopeAngle;
                }
            }
        }
        */
    }

    /*
    void ClimbSlope(ref Vector3 movement, float slopeAngle)
    {
        float moveDistance = Mathf.Abs(movement.x);
        float climbMovementY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        if (movement.y <= climbMovementY)
        {
            movement.y = climbMovementY;
            movement.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(movement.x);
            collisions.below = true;
            collisions.climbingSlope = true;
            collisions.slopeAngle = slopeAngle;
        }
    }
    
    void DescendSlope(ref Vector3 movement)
    {
        float directionX = Mathf.Sign(movement.x);
        Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;
        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

        if (hit)
        {
            float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
            if (slopeAngle != 0 && slopeAngle <= maxDescendAngle)
            {
                if (Mathf.Sign(hit.normal.x) == directionX)
                {
                    if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(movement.x))
                    {
                        float moveDistance = Mathf.Abs(movement.x);
                        float descendMovementY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                        movement.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(movement.x);
                        movement.y -= descendMovementY;

                        collisions.slopeAngle = slopeAngle;
                        collisions.descendingSlope = true;
                        collisions.below = true;
                    }
                }
            }
        }
    }
    */

    void ResetFallingThroughPlatform()
    {
        Collisions.FallingThroughPlatform = false;
    }

    public struct CollisionInfo
    {
        public bool Above, Below;
        public bool Left, Right;

        /*
        public bool climbingSlope;
        public bool descendingSlope;
        public float slopeAngle, slopeAngleOld;
        */
        public Vector3 MovementOld;
        public int FaceDir;
        public bool FallingThroughPlatform;

        public void Reset()
        {
            Above = Below = false;
            Left = Right = false;
            /*
            climbingSlope = false;
            descendingSlope = false;

            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
            */
        }
    }

}