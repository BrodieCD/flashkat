﻿using UnityEngine;
using System.Collections;

public class SpriteController : MonoBehaviour {

    [Header("Variables")]
    public bool spriteIsNaturallyfacingLeft;

    [Header("References")]
    public SpriteRenderer spriteRenderer;

    private Controller2D _controller2D;

    void Start()
    {
        _controller2D = GetComponentInParent<Controller2D>();
    }

    void Update()
    {
        if (_controller2D.PlayerInput.x != 0)
        	FaceDir(_controller2D.Collisions.FaceDir);
    }

    public void FaceDir(int faceDir)
    {
        if (spriteIsNaturallyfacingLeft)
        {
            if (faceDir == -1)
                spriteRenderer.flipX = false;
            else if (faceDir == 1)
                spriteRenderer.flipX = true;
        }
        else
        {
            if (faceDir == 1)
                spriteRenderer.flipX = true;
            else if (faceDir == -1)
                spriteRenderer.flipX = false;
        }
    }
}
