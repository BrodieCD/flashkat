﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(InputUser))]
public class CombatController : MonoBehaviour 
{
	public delegate void AttackCallback(Attack.Names attackName);
	public delegate void DefenseCallback(bool isRoll);

	public List<ComboAttack> ComboAttacks;

//    public float ComboChainCoefficient = 0.25f;
//    public float FinisherCoefficient = 2;
//    public int MegaFinisherThreshold = 15;
//    public float MegaFinisherCoefficient = 4;
	
	public char PunchButtonCode = 'l';
	public char KickButtonCode = 'r';

	public int ComboCodeMaxChars = 3;

    private string _comboBuffer = "";

	private Dictionary<int, Dictionary<string, Attack.Names>> _comboCodesChecklists;
	private Attack.Names _bufferedAttackName;

	private Dictionary<Attack.Names, ComboAttack> _comboAttackByName;

    private float _lastAttackMadeTime;
	private float _lastAttackMadeComboTimeout = -1;
	private float _lastAttackMadeCooldownComboDelay = -1;

	private float _cooldownTimeCount;
	private AttackCallback _attackCallback;
	private DefenseCallback _defenseCallback;


//    [SerializeField] 
	private int _comboChainLength;

	private InputUser _inputUser;


	void Awake()
	{
		_inputUser = GetComponent<InputUser>();
	}

	public void Initialize(List<Attack> attacks, AttackCallback attackCallback, DefenseCallback defenseCallback) 
	{
		initializeComboCheckLists(attacks);	
		_lastAttackMadeTime = Time.time;

		_comboAttackByName = new Dictionary<Attack.Names, ComboAttack>();
		int dataCount = ComboAttacks.Count;
		for(int i = 0; i < dataCount; i++)
		{
			Attack.Names attackName = ComboAttacks[i].AttackName;
			if(attackName != Attack.Names.None)
				_comboAttackByName.Add(attackName, ComboAttacks[i]);
		}

		_attackCallback = attackCallback;
		_defenseCallback = defenseCallback;
	}
	
	public void ReadAndWriteOnAttackBuffer(bool isAttacking, int facing)
	{
		if(!readAttackBuffer())
			HandleAttackInput(isAttacking, facing);
	}

	public void HandleAttackInput(bool isAttacking, int facing)
	{
		if(_inputUser.GetPunchButtonDown())
			handleInputAttack(isAttacking, facing, PunchButtonCode, Attack.Names.Raiden, Attack.Names.DragonRaidenPunch1);
		else if(_inputUser.GetKickButtonDown())
			handleInputAttack(isAttacking, facing, KickButtonCode, Attack.Names.RageKick, Attack.Names.DragonKick);
		else if(!isAttacking)
		{
			if(_inputUser.GetSpecialButtonDown())
			{
				float moveH = _inputUser.GetMoveH();

				if((Mathf.Abs(moveH) > 0) && (Mathf.Sign(moveH) == facing))
					_attackCallback(Attack.Names.TornadoKick);
				else if(_inputUser.GetMoveV() > 0)
					_attackCallback(Attack.Names.FlamingFlash1);
				else 
					_attackCallback(Attack.Names.ElectricShoot);
			}
			else if(_inputUser.GetDefenseButtonDown())
				_defenseCallback(Mathf.Abs(_inputUser.GetMoveH()) > 0);	
		}
	}

	public void HandleAttackStartupInit()
	{ 		
		_bufferedAttackName = Attack.Names.None;
		_lastAttackMadeTime = Time.time;
	}

	public void HandleInitAttackHit(Attack.Names lastAttackName)
	{
		if(_comboAttackByName.ContainsKey(lastAttackName))
		{
			//We need to do this instead of using LastAttack, because LastAttack is used for received attacks too.
			ComboAttack lastComboAttackData = _comboAttackByName[lastAttackName];
			_lastAttackMadeComboTimeout = lastComboAttackData.ComboTimeout;  
			_lastAttackMadeCooldownComboDelay = lastComboAttackData.CooldownComboDelay;
		}
	}

	public void HandleAttackCooldownInit()
	{
		_cooldownTimeCount = 0;
	}

	public void HandleAttackCooldownUpdate(int facing)
	{
		if(_cooldownTimeCount < _lastAttackMadeCooldownComboDelay)
		{
			_cooldownTimeCount += Time.deltaTime;
			HandleAttackInput(true, facing);
		}
		else
			ReadAndWriteOnAttackBuffer(true, facing);
	}

	private void handleInputAttack(bool isAttacking, int facing, char buttonCode, Attack.Names forwardInputAttack, Attack.Names upwardInputAttack)
	{
		bool didTiltAttack = false;

		if(!isAttacking)
		{
			float moveH = _inputUser.GetMoveH();
		
			if((Mathf.Abs(moveH) > 0) && (Mathf.Sign(moveH) == facing))
			{
				_attackCallback(forwardInputAttack);
				didTiltAttack = true;
			}
			else if(_inputUser.GetMoveV() > 0)
			{
				_attackCallback(upwardInputAttack);
				didTiltAttack = true;
			}
		}

		if(!didTiltAttack)
			processAttackInput(buttonCode, isAttacking);
	}

	private void processAttackInput(char buttonCode, bool isAttacking)
	{
		bool comboCannotBeLonger = (_comboBuffer.Length == ComboCodeMaxChars);

        if(comboCannotBeLonger && isAttacking)
		{
			//print("Combo cannot continue. Ignoring attack input : " + buttonCode);
			return;
		}

		float currentTime = Time.time;
		float timeSinceLastAttack = currentTime - _lastAttackMadeTime;

		//print("timeSinceLastAttack: " + timeSinceLastAttack);

		if((comboCannotBeLonger && !isAttacking)
			|| ((_lastAttackMadeComboTimeout != -1) && (timeSinceLastAttack >= _lastAttackMadeComboTimeout)))
		{
			//print("ComboBuffer cleared.");
	        _comboBuffer = "";
		}

		bufferAttackInput(buttonCode, isAttacking);
	}

	private void initializeComboCheckLists(List<Attack> attacks)
	{
		_comboCodesChecklists = new Dictionary<int, Dictionary<string, Attack.Names>>();
		for(int i = 1; i <= ComboCodeMaxChars; i++)
			_comboCodesChecklists.Add(i, new Dictionary<string, Attack.Names>());

		int attacksCount = ComboAttacks.Count;
		for(int i = 0; i < attacksCount; i++)
		{
			if(attacks[i].Name != Attack.Names.None)
			{
				string playerAttackCode = ComboAttacks[i].Code;
				int charsAmount = playerAttackCode.Length;
				_comboCodesChecklists[charsAmount].Add(playerAttackCode, attacks[i].Name);
			}
		}
	}

	private void bufferAttackInput(char c, bool isAttacking)
	{
		if(_bufferedAttackName != Attack.Names.None)
		{
			//print("Attack input ignored as there is already an attack buffered");
			return;
		}

		string codeToCheck = _comboBuffer + c;
		int codeLenght = codeToCheck.Length;
		bool wasComboBufferUpdated = false;

		if(_comboCodesChecklists[codeLenght].ContainsKey(codeToCheck))
		{
			_comboBuffer += c;
			wasComboBufferUpdated = true;
			#if UNITY_EDITOR
			//print("Input char: " + c + " results in: " + codeToCheck + " that matches with attack: " + _comboCodesChecklists[codeLenght][ComboBuffer].Name);	
			#endif	
		}
		else
		{
			if(!isAttacking)
			{
				_comboBuffer = c.ToString();
				codeLenght = _comboBuffer.Length;
				wasComboBufferUpdated = true;
				//print("Input char: " + c + " results in: " + codeToCheck + " that does not match with any attack. As player is not attacking, ComboBuffer is reset.");	
			}
			#if UNITY_EDITOR
//			else
//				print("Input char: " + c + " results in: " + codeToCheck + " that does not match with any attack. Ignored.");	
			#endif		
		}

		if(wasComboBufferUpdated)
		{
			//print("codeLenght: " + codeLenght + " ComboBuffer: " + ComboBuffer);
			_bufferedAttackName = _comboCodesChecklists[codeLenght][_comboBuffer];
		}
	}

	private bool readAttackBuffer()
	{
		bool attackRead = false;

		if(_bufferedAttackName != Attack.Names.None)
		{
			_attackCallback(_bufferedAttackName);
			attackRead = true;
		}

		return attackRead;
	}

//   private float calculateDamageFromGivenCombo(string combo, int chainLenght, bool isFinisher)
//    {
//        float dmg = 0;
//
//        int comboLenght = combo.Length;
//        float chainDmg = (float)(chainLenght * ComboChainCoefficient);
//
//        if (isFinisher)
//        {
//            if (chainLenght >= MegaFinisherThreshold)
//            {
//                dmg = MegaFinisherCoefficient * comboLenght + chainDmg;
//                //_comboChainLength = 0;
//            }
//            else
//                dmg = FinisherCoefficient * comboLenght + chainDmg;
//            
//            _comboBuffer = "";
//        }
//		else 
//            dmg = 1 + chainDmg;
//
//        Debug.Log("Chain Length: " + chainLenght + ", Combo: " + combo + ", Damage: " + (int)dmg);
//        return (int)dmg;
//    }

	[System.Serializable]
	public class ComboAttack 
	{
		public Attack.Names AttackName;
		public string Code = "r"; 
		public float ComboTimeout = 0.3f;
		public float CooldownComboDelay = 0.2f;
	}
}
