﻿using UnityEngine;
using System.Collections;

public class SpawnablePrefabs : MonoBehaviour {

    public prefabs _prefabs;
    public static SpawnablePrefabs instance;

    void Awake()
    {
        instance = this;
    }

	public enum PrefabNames
    {
        Burger,
        Banana,
        Cake,
        Candy,
        Carrot,
        IceCream,
        Pizza,
        Strawberry,
        HealthCrate,
        SpeedPowerup,
        StrengthPowerup,
        JumpPowerup
    }

    [System.Serializable]
    public struct prefabs
    {
        public GameObject burger;
        public GameObject banana;
        public GameObject cake;
        public GameObject candy;
        public GameObject carrot;
        public GameObject iceCream;
        public GameObject pizza;
        public GameObject strawberry;
        public GameObject healthCreate;
        public GameObject speedPowerup;
        public GameObject strenghtPowerup;
        public GameObject JumpPowerup;
    }

    public GameObject FindPrefabUsingPrefabName(PrefabNames prefabName)
    {
        if (prefabName == PrefabNames.Burger)
            return _prefabs.burger;
        else
        if (prefabName == PrefabNames.Banana)
            return _prefabs.banana;
        else
        if (prefabName == PrefabNames.Cake)
            return _prefabs.cake;
        else
        if (prefabName == PrefabNames.Candy)
            return _prefabs.candy;
        else
        if (prefabName == PrefabNames.Carrot)
            return _prefabs.carrot;
        else
        if (prefabName == PrefabNames.IceCream)
            return _prefabs.iceCream;
        else
        if (prefabName == PrefabNames.Pizza)
            return _prefabs.pizza;
        else
        if (prefabName == PrefabNames.Strawberry)
            return _prefabs.strawberry;
        else
        if (prefabName == PrefabNames.HealthCrate)
            return _prefabs.healthCreate;
        else
        if (prefabName == PrefabNames.SpeedPowerup)
            return _prefabs.speedPowerup;
        else
        if (prefabName == PrefabNames.StrengthPowerup)
            return _prefabs.strenghtPowerup;
        else
        if (prefabName == PrefabNames.JumpPowerup)
            return _prefabs.JumpPowerup;
        else
            return null;
    }
}
