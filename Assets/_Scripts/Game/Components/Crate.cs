﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Crate : MonoBehaviour {

    [Header("Variables")]
    public float currentCondition;
    public float maxCondition;

    [Header("ItemDropVariables")]
    public bool canDropItem;
    [Range(0, 100)]
    public float percentageChanceToDropItem;

    public enum DropableItemTypes {Powerup, Consumeable, Random, Checkpoint}
    public DropableItemTypes dropableItemType; 
    public GameObject[] dropableConsumeables;
    public GameObject[] dropablePowerups;

    [Header("FxAndSound")]
    public List<FxAndSounds.FxsNames> hitFx;
    public List<FxAndSounds.FxsNames> destroyFx;

    public List<FxAndSounds.SoundsNames> hitSounds;
    public List<FxAndSounds.SoundsNames> destroySounds;

    void Start()
    {
        currentCondition = maxCondition;
    }


    public void GetHit(Attack attack)
    {
        //Debug.Log("Took hit");
        currentCondition -= attack.Damage;

        if (currentCondition <= 0)
        {
            DestroyCrate();
        }else
        {
            FxManager.EnableRandomFxFromList(this, hitFx, transform.position, Quaternion.identity);
            SoundManager.PlayRandomOneShotSoundFromList(this, hitSounds, GameConfig.SfxVolumeRatio);
        }
    }

    void DestroyCrate()
    {
        if (dropableItemType == DropableItemTypes.Checkpoint)
            CheckpointManager.instance.SpawnCheckpoint(transform.position -Vector3.up * GetComponent<BoxCollider2D>().size.y / 2);

        if (canDropItem)
        { 
            int a = Random.Range(0, 101);
            if (a <= percentageChanceToDropItem)
            {
                if (dropableItemType == DropableItemTypes.Random)
                {
                    //this picks either 0: powerup or 1: consumeable
                    int j = Random.Range(0, 2);

                    if (j == 0)
                    {
                        Instantiate(dropablePowerups[Random.Range(0, dropablePowerups.Length)], transform.position, Quaternion.identity);
                    }else if (j == 1)
                    {
                        Instantiate(dropableConsumeables[Random.Range(0, dropableConsumeables.Length)], transform.position, Quaternion.identity);
                    }
                } else if (dropableItemType == DropableItemTypes.Powerup)
                {
                    Instantiate(dropablePowerups[Random.Range(0, dropablePowerups.Length)], transform.position, Quaternion.identity);
                } else if (dropableItemType == DropableItemTypes.Consumeable)
                {
                    Instantiate(dropableConsumeables[Random.Range(0, dropableConsumeables.Length)], transform.position, Quaternion.identity);
                }
            }
        }

        FxManager.EnableRandomFxFromList(this, destroyFx, transform.position, Quaternion.identity);
        SoundManager.PlayRandomOneShotSoundFromList(this, destroySounds, GameConfig.SfxVolumeRatio);

        Destroy(gameObject);
    }

}
