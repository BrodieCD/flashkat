﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Actor : MonoBehaviour 
{
	#region Enums
	public enum AnimationNames
	{
		None,
		Idle,
		Walk,
		Trot,
		ExitLocomotion,
		PunchLstartup,
		PunchLhit,
		PunchLcooldown,
		PunchRstartup,
		PunchRhit,
		PunchRcooldown,
		KickRstartup,
		KickRhit,
		KickRcooldown,
		KickLstartup,
		KickLhit,
		KickLcooldown,
		SpecialStartup,
		SpecialHit,
		SpecialCooldown,
		SuperStartup,
		SuperHit,
		SuperCooldown,
		StunStart,
		StunStay,
		StartRise,
		Rise,
		StartFall,
		Fall,
		Land,
		Dead,
		WallSlide,
		SlidePunchStartup,
		SlidePunchHit,
		SlidePunchCooldown,
		RaidenStartup,
		RaidenHit,
		RaidenCooldown,
		ElectricShootStartup,
		ElectricShootHit,
		ElectricShootCooldown,
		DragonRaidenPunchStartup,
		DragonRaidenPunchHit1,
		DragonRaidenPunchHit2,
		DragonRaidenPunchHit3,
		DragonRaidenPunchCooldown,
		FlamingFlashStartup,
		FlamingFlashHit1,
		FlamingFlashHit2,
		FlamingFlashHit3,
		FlamingFlashCooldown,
		SpotDodgeStartup,
		SpotDodgeMiddle,
		SpotDodgeCooldown,
		RollDodgeStartup,
		RollDodgeMiddle,
		RollDodgeCooldown,
		DragonKickStartup,
		DragonKickHit,
		DragonKickCooldown,
		RageKickStartup,
		RageKickHit,
		RageKickCooldown,
		TornadoKickStartup,
		TornadoKickHit,
		TornadoKickCooldown
	}
	#endregion

	#region Constants
	const string SPEED_H_ANIM_PARAM = "SpeedH"; 
	#endregion

	#region Public fields
	#if UNITY_EDITOR
	public Attack TestAttack;
	public Vector2 TestAttackerPos = new Vector2(-Mathf.Infinity, 0);
	public KeyCode TestAttackKey = KeyCode.T;
	#endif

	public Text TempLife;	

//    public float StunMovementAmplitudeFactorY = 0.05f;
//	public float StunMovementFrequencyFactorY = 50;		

	public float MaxEnergy = 100;
	public float StartingEnergy = 100;

	public float MaxAnger = 100;
	public float StartingAnger = 50;

	public Vector2 RollDodgeVelocity = new Vector2(1, 0);
	public float RollDodgeFriction = 0.1f;

	public string SpeedXanimParam = "SpeedX";
	public string SpeedYanimParam = "SpeedY";
	public string VelocityYanimParam = "VelocityY";
	public string GroundedAnimParam = "Grounded";

	public int MaxAnimTriggerQueueCount = 5;

	public List<Attack> Attacks = new List<Attack>();

	public List<FxAndSounds.SoundsNames> HitSounds;
	public List<FxAndSounds.SoundsNames> SwingSounds;
	public List<FxAndSounds.SoundsNames> LandSounds;
	public List<FxAndSounds.SoundsNames> DodgeSounds;

	[HideInInspector]
	public int ActorId = -2;

	[HideInInspector]
	public Attack LastAttack = null;   //Used for both attack made and attack received

	[HideInInspector]
	public bool HitConfirmed = false;	

	[HideInInspector]
	public bool IsPlayer = false;

	public Animator AnimatorRef;	
	public SpriteRenderer SpriteRendererRef;
	public GameObject HitBox;
	public ActorVoiceController Voice;
	#endregion

	#region Protected fields
	protected bool _isActive = true;
	protected float _currentEnergy;

	protected bool _isInvulnerable = false;
	
	protected int _direction = GameConstants.DIR_RIGHT;	
	protected int _directionLastFrame = GameConstants.DIR_RIGHT;
	protected int _actionDirection = GameConstants.DIR_RIGHT;  //Used for both attack made or attack received

	protected float _actionTimeCount;
	protected float _actionCurrentDelay;	

	protected Dictionary<Attack.Names, Attack> _attacksByName;
	protected AnimationNames _currentAnimationName = AnimationNames.None;

	protected bool _isLeavingStateByStun = false;
	protected bool _isInLocomotion = false;

	protected Vector3 _velocity;
	protected float _gravity;

	protected Controller2D _controller2d;
	#endregion 

	#region Private fields	
	private float _currentActionMinDelay;
	private float _currentActionMaxDelay;

	private SimpleCallback _currentAnimStateCallback;
	private SimpleCallback _nextLeaveAnimStateCallback;	

	private Queue<AnimTriggers> _lastAnimTriggers = new Queue<AnimTriggers>();
	#endregion

	#region Properties
	public float CurrentEnergy
	{
		get { return _currentEnergy; }
		set 
		{
			_currentEnergy = value;

			if(CurrentEnergy < 0)
				_currentEnergy = 0;
			else if(CurrentEnergy > MaxEnergy)
				_currentEnergy = MaxEnergy;

			updateEnergyBar();
		}
	}
	
	public Vector3 Velocity { get { return _velocity; } }
	public Vector3 SignedVelocity { get	{ return new Vector3(_velocity.x * Facing, _velocity.y, _velocity.z); } }

	public int Direction 
	{
		get { return _direction; }
		set 
		{
			_direction = value;
			Facing = _direction;
		}
	}

	public int Facing
	{
		get { return (int)Mathf.Sign(transform.localScale.x); }
		set 
		{
			if(Facing != value)
			{
				Vector3 localScale = transform.localScale;
				transform.localScale = new Vector3(localScale.x * -1, localScale.y, localScale.z);
			}
		}
	}

	public Vector3 Position 
	{
		get { return  transform.position; }
		set { transform.position = value; }
	}
	
	public float PosX
	{
		get { return Position.x; }
		set { Position = new Vector3(value, Position.y, Position.z); }
	}
	
	public float PosY
	{
		get { return Position.y; }
		set { Position = new Vector3(Position.x, value, Position.z); }
	}	
	
	public float PosZ
	{
		get { return Position.z; }
		set { Position = new Vector3(Position.x, Position.y, value); }
	}	
	
	public bool IsAlive { get { return (CurrentEnergy > 0); } }
	public bool IsActiveInLocomotion{ get { return _isActive && _isInLocomotion; } }

	#endregion

	#region Monobehaviour methods
	public virtual void Awake () 
	{
		_controller2d = GetComponent<Controller2D>();
		IsPlayer = (name == "Player");
	}

	public virtual void Update () 
	{
		#if UNITY_EDITOR
		handleTestAttack();
		#endif		
	}
	#endregion

	#region Miscelaneous methods
	public virtual void Initialize () 
	{
		CurrentEnergy = StartingEnergy;

		_velocity.y = 0;	
		_gravity = GameConfig.Gravity;

		_attacksByName = new Dictionary<Attack.Names, Attack>();
		foreach(Attack attack in Attacks)
		{
			if(attack.Name != Attack.Names.None)
				_attacksByName.Add(attack.Name, attack);
		}

		if(HitBox != null)
			HitBox.SetActive(false);

		//Should work to set ActorRef but there is a bug and it gets nulled after being set. This did not happen before.
//		ActorStateMachineBehaviour[] actorBehaviours = AnimatorRef.GetBehaviours<ActorStateMachineBehaviour>();
//		foreach(ActorStateMachineBehaviour actorBehaviour in actorBehaviours)
//			actorBehaviour.SetActorRef(this);
	}

	public Attack GetAttackByName(Attack.Names name)
	{
		return _attacksByName[name];
	}

	#if UNITY_EDITOR
	private void handleTestAttack()
	{
		if(!IsPlayer)  //Change or remove to test Player or enemies.
			return;

		if(Input.GetKeyDown(TestAttackKey))
		{
			FxManager.EnableFx(this, TestAttack.HitFx, transform.position, Quaternion.identity);
			SoundManager.PlayRandomOneShotSoundFromList(this, HitSounds);
			GetHit(TestAttack, TestAttackerPos);
		}
	}
	#endif

	virtual public bool GetHit(Attack attack, Vector2 attackerPos)
	{
//		if(_isPlayer)  //TODO: Remove Hack
//			return;

		if(_isActive)
		{	
			if(IsAlive && !_isInvulnerable)
			{
//				print("Actor " + name + " got hit with damage: " + attack.Damage + ", stun: " + attack.StunTime + " , knockback: " 
//					+ attack.KnockBackVelocity.ToString() + ", position: " + attackerPos.ToString());	
	
//				if(name != "Player") //TODO: Remove Hack

				if(Voice != null)
					Voice.PlayRandomHurtSound();

                if (IsPlayer && GetComponent<PowerupController>().powerupData.UsingStrenghtPowerup)
                {
                    CurrentEnergy -= attack.Damage * GetComponent<PowerupController>().powerupData.DamageReductionMultiplyer;
                }
                else
                    CurrentEnergy -= attack.Damage;
				
				_actionDirection = (int)Mathf.Sign(PosX - attackerPos.x);
				LastAttack = attack;
		
				stun();
				return true;
			}
			#if UNITY_EDITOR
//			else
//				print("Actor " + name + " was hit while dead.");
			#endif
		}
		else
			print("Actor " + name + " was hit while inactive.");

		return false;
	}

    virtual public bool GetHit(Attack attack, Vector2 attackerPos, float dmgMultiplier)
    {
        //		if(_isPlayer)  //TODO: Remove Hack
        //			return;

        if (_isActive)
        {
            if (IsAlive && !_isInvulnerable)
            {
                //				print("Actor " + name + " got hit with damage: " + attack.Damage + ", stun: " + attack.StunTime + " , knockback: " 
                //					+ attack.KnockBackVelocity.ToString() + ", position: " + attackerPos.ToString());	

                //				if(name != "Player") //TODO: Remove Hack

                if (Voice != null)
                    Voice.PlayRandomHurtSound();

                if (IsPlayer && GetComponent<PowerupController>().powerupData.UsingStrenghtPowerup)
                {
                    CurrentEnergy -= attack.Damage * GetComponent<PowerupController>().powerupData.DamageReductionMultiplyer;
                }
                else
                    CurrentEnergy -= attack.Damage * dmgMultiplier;

                _actionDirection = (int)Mathf.Sign(PosX - attackerPos.x);
                LastAttack = attack;

                stun();
                return true;
            }
#if UNITY_EDITOR
            //			else
            //				print("Actor " + name + " was hit while dead.");
#endif
        }
        else
            print("Actor " + name + " was hit while inactive.");

        return false;
    }

    public void GetHealth(float healthToHeal, GameObject objectPickedUp)
    {
        if (CurrentEnergy < MaxEnergy)
        {
            CurrentEnergy += healthToHeal;
            Destroy(objectPickedUp);
        }

        if (CurrentEnergy >= MaxEnergy)
            CurrentEnergy = MaxEnergy;
    }

	public void DoAttack(Attack.Names attackName)
	{
		DoAttack(attackName, false);
	}

	virtual public void DoAttack(Attack.Names attackName, bool isCoolingDown)
	{
		if(isCoolingDown)
			exitCooldown();

		exitLocomotion();
		setAnimTrigger(_attacksByName[attackName].AnimTrigger);
	}

	virtual public void DoDefense(bool isRoll)
	{
		exitLocomotion();

		if(isRoll)
			setAnimTrigger(AnimTriggers.RollDodge);
		else 
			setAnimTrigger(AnimTriggers.SpotDodge);
	}

	virtual protected void updateEnergyBar(){}

	virtual protected void enableActivation(bool enabled)
	{
		AnimatorRef.enabled = enabled;
		SpriteRendererRef.enabled = enabled;
		_isActive = enabled;
	}

	protected void disable()
	{
		gameObject.SetActive(false);
	}

	protected void applyGravity()
	{
		applyGravity(true);
	}

	protected void applyGravity(bool handleMovement)
	{
		_velocity.y += _gravity * Time.deltaTime;

		if(handleMovement)
		{
			float movementY = _velocity.y * Time.deltaTime;		
			_controller2d.Move(new Vector3(0, movementY, 0), Facing);
		}
	}

	protected bool handleGrounding()
	{
		if(_controller2d.IsGrounded)
		{
			_velocity.y = 0;
			return true;
		}

		return false;
	}

	protected void updateAttackMovement(bool useGravity = true) //Used for both attack made and received
	{
		updateActionMovement(LastAttack.Friction, useGravity); 
	}

	protected void updateActionMovement(float friction, bool useGravity = true) 
	{
		float movementX = _velocity.x * _actionDirection * Time.deltaTime;
		float movementY = _velocity.y * Time.deltaTime; 

		float velocityXsign = Mathf.Sign(_velocity.x);
		
		_velocity.x -= _velocity.x * friction;

		if(velocityXsign > 0)
		{
			if(_velocity.x < 0)
				_velocity.x = 0;
		}
		else if(_velocity.x > 0)
				_velocity.x = 0;

		if(useGravity)
			applyGravity(false);

		_controller2d.Move(new Vector3(movementX, movementY, 0), Vector2.zero, Facing, false, false, useGravity);	
	}

	protected void updateAnimSpeedX()
	{
		AnimatorRef.SetFloat(SpeedXanimParam, Mathf.Abs(_velocity.x));
	}

	protected void updateAnimSpeedY()
	{
		AnimatorRef.SetFloat(SpeedYanimParam, Mathf.Abs(_velocity.y));
	}

	protected void updateAnimVelocityY()
	{
		AnimatorRef.SetFloat(VelocityYanimParam, _velocity.y);
	}

	protected void moveH(float movementX)
	{
		_controller2d.Move(new Vector3(movementX, 0, 0), Facing);	
	}
	#endregion

	#region Timing Methods
	protected void setActionTime(float delay)
	{
		_actionTimeCount = 0;
		_actionCurrentDelay = delay;	
	}
	
	protected void setRandomActionTime(float minActionTime, float maxActionTime)
	{
		_actionTimeCount = 0;
		_actionCurrentDelay = UnityEngine.Random.Range(minActionTime, maxActionTime);	
		_currentActionMinDelay = minActionTime;
		_currentActionMaxDelay = maxActionTime;		
	}

	protected void updateActionTime(SimpleCallback callback, bool isRandomReset = false)
	{
//		print("actionTimeCount: " + _actionTimeCount + " delay: " + _actionCurrentDelay 
//				+ " in animation: " + _currentAnimationName.ToString() + " state: " + State.ToString());

		_actionTimeCount += Time.deltaTime;
		
		if(_actionTimeCount >= _actionCurrentDelay)
		{
			_actionTimeCount = 0;

			if(isRandomReset)
				_actionCurrentDelay = UnityEngine.Random.Range(_currentActionMinDelay, _currentActionMaxDelay);	
						
			callback();
		}
	}
	#endregion

	#region Animation Methods
	protected void updateAnimState()
	{
		if(_currentAnimStateCallback != null)
			_currentAnimStateCallback();
	}

	protected void setAnimTrigger(AnimTriggers animationTrigger)
	{
		string animationTriggerString = animationTrigger.ToString();

//		if(_isPlayer)
//			print("setTrigger: " + animationTriggerString);
		
		if(!_lastAnimTriggers.Contains(animationTrigger))
		{
			_lastAnimTriggers.Enqueue(animationTrigger);
			if(_lastAnimTriggers.Count > MaxAnimTriggerQueueCount)
				_lastAnimTriggers.Dequeue();
		}

		AnimatorRef.SetTrigger(animationTriggerString);
	}

	protected void resetAnimTrigger(Enum animationTrigger)
	{
		string animationTriggerString = animationTrigger.ToString();
		AnimatorRef.ResetTrigger(animationTriggerString);
	}

	protected void genericAttackStartupAnimStateStart(AnimationNames animToStart)
	{
		handleAnimStateStart(animToStart, genericAttackStartupUpdate, null);
		genericAttackStartupInit();
	}

	protected void genericAttackHitAnimStateStart(AnimationNames animToStart, Attack.Names attackName)
	{
		handleAnimStateStart(animToStart, genericAttackHitUpdate, genericAttackHitLeave);
		genericAttackHitInit(attackName);
	}

	protected void genericAttackCooldownAnimStateStart(AnimationNames animToStart)
	{
		handleAnimStateStart(animToStart, genericAttackCooldownUpdate, genericAttackCooldownLeave);
		genericAttackCooldownInit();
	}

	protected void handleAnimStateStart(AnimationNames animToStart, SimpleCallback currentUpdateAnimStateCallback, SimpleCallback nextLeaveAnimStateCallback)
	{ 
		_currentAnimationName = animToStart;

		if(_nextLeaveAnimStateCallback != null)
			_nextLeaveAnimStateCallback();

		_currentAnimStateCallback = currentUpdateAnimStateCallback;
		_nextLeaveAnimStateCallback = nextLeaveAnimStateCallback;

//		if(_isPlayer)
//			print(name + " Anim started: " + animToStart.ToString());
	}
	
	protected void goLocomotion()
	{
		if((_currentAnimationName != AnimationNames.Idle) && (_currentAnimationName != AnimationNames.Walk) 
			&& (_currentAnimationName != AnimationNames.Trot)) 
			setAnimTrigger(AnimTriggers.GoLocomotion);
	}

	protected void exitCooldown()
	{
		setAnimTrigger(AnimTriggers.ExitCooldown);
	}

	protected void exitLocomotion()
	{
		if(_currentAnimationName != AnimationNames.ExitLocomotion)
			setAnimTrigger(AnimTriggers.ExitLocomotion);
	}
	
	virtual protected void genericAttackStartupInit(){} 	

	virtual protected void genericAttackStartupUpdate()
	{
		applyGravity();
	}

	virtual protected void genericAttackHitInit(Attack.Names attackName)
	{
		LastAttack = _attacksByName[attackName];
		_velocity = new Vector3(LastAttack.Velocity.x, LastAttack.Velocity.y, 0);
		_actionDirection = Facing;

		if(Voice != null)
			Voice.PlayRandomAttackSound();

		if(LastAttack.SwingManualSoundFx != FxAndSounds.SoundsNames.None)
			SoundManager.PlayManualSound(this, LastAttack.SwingManualSoundFx, ActorId, LastAttack.SwingManualSoundVol);

		if(LastAttack.SwingOneShotSoundFx != FxAndSounds.SoundsNames.None)
			SoundManager.PlayOneShotSound(this, LastAttack.SwingOneShotSoundFx, LastAttack.SwingOneShotSoundVol);
		else 
			SoundManager.PlayRandomOneShotSoundFromList(this, SwingSounds);

		if(HitBox != null)
			HitBox.SetActive(true);
	}

	virtual protected void genericAttackHitUpdate()
	{
		genericAttackHitUpdate(true);
	}

	virtual protected void genericAttackHitUpdate(bool useGravity)  
	{
		updateAttackMovement(useGravity);
	}

	virtual protected void genericAttackHitLeave()
	{
		_velocity.x = 0;
		if(HitBox != null)
			HitBox.SetActive(false);	

		if(LastAttack.SwingManualSoundFx != FxAndSounds.SoundsNames.None)
			SoundManager.StopManualSound(this, ActorId);	
	}

	virtual protected void genericAttackCooldownInit(){}

	virtual protected void genericAttackCooldownUpdate()
	{
		applyGravity();
	}

	virtual protected void genericAttackCooldownLeave(){}

	protected void punchL()
	{
		DoAttack(Attack.Names.PunchL);
	}

	protected void kickR()
	{
		DoAttack(Attack.Names.KickR);
	}

	protected void exitAttack()
	{
		setAnimTrigger(AnimTriggers.ExitCooldown);
	}

	protected void stun()
	{
		_isLeavingStateByStun = true;	

		foreach(AnimTriggers animTrigger in _lastAnimTriggers)
			resetAnimTrigger(animTrigger);

		_lastAnimTriggers.Clear();

		setAnimTrigger(AnimTriggers.Stun);
	}	
	
	protected void die()
	{
		setAnimTrigger(AnimTriggers.Die);
	}	
	#endregion

	#region Animation State Machine

	public void OnEnterLocomotion()
	{
		resetAnimTrigger(AnimTriggers.GoLocomotion);
		_isInLocomotion = true;
	}

	public void OnLeaveLocomotion()
	{
//		if(IsPlayer)
//			print("Leave locomotion");
		_isInLocomotion = false;
	}
	
	virtual public void IdleInit() 
	{
		handleAnimStateStart(AnimationNames.Idle, idleUpdate, null);	
	}

	virtual protected void idleUpdate(){} 

	virtual public void WalkingInit() 
	{
		handleAnimStateStart(AnimationNames.Walk, walkingUpdate, null);
	}

	virtual protected void walkingUpdate() {}

	virtual public void TrotingInit() 
	{
		handleAnimStateStart(AnimationNames.Walk, trotingUpdate, null);
	}

	virtual protected void trotingUpdate() {}

	public void ExitLocomotionInit()
	{
		handleAnimStateStart(AnimationNames.ExitLocomotion, exitLocomotionUpdate, null);

//		if(IsPlayer)
//			print("ExitLocomotionInit");
	}

	protected void exitLocomotionUpdate() 
	{
		applyGravity();
	}

	public void PunchLstartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.PunchLstartup);
	}

	virtual public void PunchLhitInit()
	{
		genericAttackHitAnimStateStart(AnimationNames.PunchLhit, Attack.Names.PunchL);
	}

	virtual public void PunchLCooldownInit()
	{
		genericAttackCooldownAnimStateStart(AnimationNames.PunchLcooldown);
	}

	public void KickRstartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.KickRstartup);
	}

	public void KickRhitInit()
	{
		genericAttackHitAnimStateStart(AnimationNames.KickRhit, Attack.Names.KickR);
	}

	virtual public void KickRcooldownInit()
	{
		genericAttackCooldownAnimStateStart(AnimationNames.KickRcooldown);
	}

	public void SuperStartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.SuperStartup);
	}

	virtual public void SuperHitInit()
	{
		handleAnimStateStart(AnimationNames.SuperHit, genericAttackHitUpdate, genericAttackHitLeave);
	}

	public void SuperCooldownInit()
	{
		handleAnimStateStart(AnimationNames.SuperCooldown, genericAttackCooldownUpdate, superCooldownLeave);
		genericAttackCooldownInit();
	}

	virtual protected void superCooldownLeave(){}

	virtual public void StunnedStartInit() 
	{
		handleAnimStateStart(AnimationNames.StunStart, null, stunnedStartLeave);
		_isLeavingStateByStun = false;
		_velocity = new Vector3(LastAttack.KnockBackVelocity.x, LastAttack.KnockBackVelocity.y, 0);
	}
	
	protected void stunnedStartLeave() 
	{
		if(!IsAlive)
			die();
	}	

	public void StunnedStayInit() 
	{
		handleAnimStateStart(AnimationNames.StunStay, stunnedStayUpdate, stunnedStayLeave);
		setActionTime(LastAttack.StunTime);
	}
	
	protected void stunnedStayUpdate() 
	{
		//print("actionTimeCount: " + _actionTimeCount);
		updateActionTime(goLocomotion, false);
		genericAttackHitUpdate();
		//PosY += StunMovementAmplitudeFactorY * Mathf.Sin(StunMovementFrequencyFactorY * Time.timeSinceLevelLoad);
	}
	
	virtual protected void stunnedStayLeave() { }	

	virtual public void DyingInit()
	{
		handleAnimStateStart(AnimationNames.Dead, dyingUpdate, dyingLeave);
		_velocity.x = 0;
	}
	
	protected void dyingUpdate() 
	{
		applyGravity();
		handleGrounding();
		//print("velocity: " + _velocity); 
	}
	
	protected void dyingLeave() 
	{
		disable();
	}

	public void SpotDodgeStartupInit() 
	{
		handleAnimStateStart(AnimationNames.SpotDodgeStartup, applyGravity, null);
	}

	public void SpotDodgeMiddleInit() 
	{
		handleAnimStateStart(AnimationNames.SpotDodgeMiddle, applyGravity, spotDodgeMiddleLeave);
		_isInvulnerable = true;

		SoundManager.PlayRandomOneShotSoundFromList(this, DodgeSounds);
	}

	private void spotDodgeMiddleLeave()
	{
		_isInvulnerable = false;
	}

	public void SpotDodgeCooldownInit() 
	{
		handleAnimStateStart(AnimationNames.SpotDodgeCooldown, applyGravity, spotDodgeCooldownLeave);
	}

	virtual protected void spotDodgeCooldownLeave(){}

	public void RollDodgeStartupInit() 
	{
		handleAnimStateStart(AnimationNames.RollDodgeStartup, applyGravity, null);
	}

	public void RollDodgeMiddleInit() 
	{
		handleAnimStateStart(AnimationNames.RollDodgeMiddle, rollDodgeMiddleUpdate, rollDodgeMiddleLeave);
		_isInvulnerable = true;
		_velocity = RollDodgeVelocity;
		_actionDirection = Facing;
		SoundManager.PlayRandomOneShotSoundFromList(this, DodgeSounds);
	}

	virtual protected void rollDodgeMiddleUpdate() 
	{
		updateActionMovement(RollDodgeFriction, false);
	}

	private void rollDodgeMiddleLeave()
	{
		_isInvulnerable = false;
		_velocity.x = 0;
	}

	public void RollDodgeCooldownInit() 
	{
		handleAnimStateStart(AnimationNames.RollDodgeCooldown, applyGravity, rollDodgeColldownLeave);
	}

	virtual protected void rollDodgeColldownLeave(){}
	#endregion
}
