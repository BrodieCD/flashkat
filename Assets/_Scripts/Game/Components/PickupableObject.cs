﻿using UnityEngine;
using System.Collections;

public class PickupableObject : MonoBehaviour {

    public enum ObjectTypes
    { Powerup, Consumeable}

    public enum PowerupTypes
    {Speed, Strenght, Jump}

    public ObjectTypes objectType;
    public PowerupTypes powerupType;

    public PickupableObjectData pickupableObjectData;
    public PowerupController.PowerupData myPowerupData;

    public void PickupObject(Player user)
    {
        /*Healing destroys the game object externally as if the player already has full health this method will do nothing and the object should not be destroyed (this gets checked with private variables unnaccesable by this script */
        if (objectType == ObjectTypes.Consumeable)
        {
            user.GetHealth(pickupableObjectData.healthToHeal, this.gameObject);
        } else if (objectType == ObjectTypes.Powerup)
        {
            if (powerupType == PowerupTypes.Strenght)
            {
                user.PowerupControllerRef.powerupData.StrengthPowerupDuration = myPowerupData.StrengthPowerupDuration;
                user.PowerupControllerRef.powerupData.DamageMultiplyer = myPowerupData.DamageMultiplyer;
                user.PowerupControllerRef.powerupData.DamageReductionMultiplyer = myPowerupData.DamageReductionMultiplyer;
                user.PowerupControllerRef.powerupData.UsingStrenghtPowerup = true;
                Destroy(gameObject);
            } else if (powerupType == PowerupTypes.Speed)
            {
                user.PowerupControllerRef.powerupData.SpeedPowerupDuration = myPowerupData.SpeedPowerupDuration;
                user.PowerupControllerRef.powerupData.SpeedMultiplyer = myPowerupData.SpeedMultiplyer;
                user.PowerupControllerRef.powerupData.UsingSpeedPowerup = true;
                Destroy(gameObject);
            }else if (powerupType == PowerupTypes.Jump)
            {
                user.PowerupControllerRef.powerupData.JumpPowerupDuration = myPowerupData.JumpPowerupDuration;
                user.PowerupControllerRef.powerupData.ExtraJumps = myPowerupData.ExtraJumps;
                user.PowerupControllerRef.powerupData.MaxJumpHeight = myPowerupData.MaxJumpHeight;
                user.PowerupControllerRef.powerupData.MinJumpHeight = myPowerupData.MinJumpHeight;
                user.PowerupControllerRef.powerupData.TimeToJumpApex = myPowerupData.TimeToJumpApex;
                user.PowerupControllerRef.powerupData.UsingJumpPowerup = true;
                Destroy(gameObject);
            }
        }
    }

    [System.Serializable]
    public struct PickupableObjectData
    {
        public float healthToHeal;
    }
}
