﻿using UnityEngine;
using System.Collections;

public class PowerupController : MonoBehaviour 
{
	[Header("Inspector reading only")]
	public PowerupData powerupData;



    void Awake()
    {
        powerupData.ResetAllPowerupDataVariables();
    }

#if UNITY_EDITOR
    public KeyCode testKey;
    public PowerupData testData;

    void Update()
    {
        if (Input.GetKeyDown(testKey))
        {
            powerupData.DamageMultiplyer = testData.DamageMultiplyer;
            powerupData.DamageReductionMultiplyer = testData.DamageReductionMultiplyer;
            powerupData.UsingStrenghtPowerup = true;

            powerupData.SpeedPowerupDuration = testData.SpeedPowerupDuration;
            powerupData.SpeedMultiplyer = testData.SpeedMultiplyer;
            powerupData.UsingSpeedPowerup = true;

            powerupData.JumpPowerupDuration = testData.JumpPowerupDuration;
            powerupData.MaxJumpHeight = testData.MaxJumpHeight;
            powerupData.MinJumpHeight = testData.MinJumpHeight;
            powerupData.TimeToJumpApex = testData.TimeToJumpApex;
            powerupData.ExtraJumps = testData.ExtraJumps;
            powerupData.UsingJumpPowerup = true;


        }
    }
#endif

    public void UpdatePower(SimpleCallback initializeMovementCallback, ref float gravity, ref float maxJumpVelocity, ref float minJumpVelocity)
	{
        //Resets powerups if their time has expired
        if (powerupData.UsingSpeedPowerup || powerupData.UsingStrenghtPowerup ||powerupData.UsingJumpPowerup)
        { 
            //Speed
            if (powerupData.UsingSpeedPowerup && Time.time > powerupData.TimeWhenPickedUpSpeedPowerup + powerupData.SpeedPowerupDuration)
            {
                powerupData.ResetSpeedPowerupVariables();
            }

            //Jump
            if (powerupData.UsingJumpPowerup && Time.time > powerupData.TimeWhenPickedUpJumpPowerup + powerupData.JumpPowerupDuration)
            {
                powerupData.ResetJumpPowerupVariables();
                initializeMovementCallback();
            }

            //Strength
            if (powerupData.UsingStrenghtPowerup && Time.time > powerupData.TimeWhenPickedUpStrenghtPowerup + powerupData.StrengthPowerupDuration)
            {
                powerupData.ResetStrengthPowerupVariables();
            }
        }

        if (powerupData.UsingJumpPowerup == true && !powerupData.HasCalculatedJumpPowerupVariables)
            ApplyJumpPowerupToPlayer(ref gravity, ref maxJumpVelocity, ref minJumpVelocity);		
	}

	public void ApplyJumpPowerupToPlayer(ref float gravity, ref float maxJumpVelocity, ref float minJumpVelocity)
    {
        powerupData.HasCalculatedJumpPowerupVariables = true;
        gravity = -(2 * powerupData.MaxJumpHeight) / Mathf.Pow(powerupData.TimeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * powerupData.TimeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * powerupData.MinJumpHeight);
    }

    [System.Serializable]
    public struct PowerupData
    {
        [Header("Speed Power up Variables")]
        //Speed Powerup data
        public float SpeedPowerupDuration;
        public float TimeWhenPickedUpSpeedPowerup;
        bool _usingSpeedPowerup;
        public float SpeedMultiplyer;

        [Header("Jump Power up Variables")]
        //JumpPowerup
        public float JumpPowerupDuration;
        public float TimeWhenPickedUpJumpPowerup;
        public bool HasCalculatedJumpPowerupVariables;
        bool _usingJumpPowerup;
        public float MaxJumpHeight;
		public float MinJumpHeight;
		public float TimeToJumpApex;
		public float ExtraJumps;

        [Header("Strenght Power up Variables")]
        //Strenght Powerup data
        public float StrengthPowerupDuration;
        public float TimeWhenPickedUpStrenghtPowerup;
        public float DamageMultiplyer;
		public float DamageReductionMultiplyer; //percentage represented as a decimal
        bool _usingSrengthPowerup;
	
	    public bool UsingStrenghtPowerup
	    {
	        get { return (_usingSrengthPowerup); }
	        set
            {
                if (value == true)
                {
                    TimeWhenPickedUpStrenghtPowerup = Time.time;
                }
                _usingSrengthPowerup = value;
            }
	    }
	
	    public bool UsingSpeedPowerup
	    {
	        get { return (_usingSpeedPowerup); }
	        set
	        {
	            _usingSpeedPowerup = value;
	            if (value == true)
	            {
	                TimeWhenPickedUpSpeedPowerup = Time.time;
	            }
	        }
	    }

        public bool UsingJumpPowerup
        {
            get { return (_usingJumpPowerup); }
            set
            {
                _usingJumpPowerup = value;
                if (value == true)
                {
                    TimeWhenPickedUpJumpPowerup = Time.time;
                }
            }
        }
			
		public void ResetAllPowerupDataVariables()
		{ 
		    TimeWhenPickedUpJumpPowerup = TimeWhenPickedUpSpeedPowerup = TimeWhenPickedUpStrenghtPowerup = 0;
            SpeedPowerupDuration = JumpPowerupDuration = StrengthPowerupDuration = 0;
		    HasCalculatedJumpPowerupVariables = false;
		    UsingSpeedPowerup = UsingStrenghtPowerup = UsingJumpPowerup = false;
		
		    SpeedMultiplyer = 0;
		    MaxJumpHeight = 0;
		    MinJumpHeight = 0;
		    TimeToJumpApex = 0;
		    ExtraJumps = 0;
		
		    DamageMultiplyer = 0;
		    DamageReductionMultiplyer = 0;
		}

        public void ResetJumpPowerupVariables()
        {
            UsingJumpPowerup = false;
            TimeWhenPickedUpJumpPowerup = 0;
            JumpPowerupDuration = 0;
            HasCalculatedJumpPowerupVariables = false;

            MaxJumpHeight = 0;
            MinJumpHeight = 0;
            TimeToJumpApex = 0;
            ExtraJumps = 0;
        }

        public void ResetStrengthPowerupVariables()
        {
            StrengthPowerupDuration = 0;
            TimeWhenPickedUpStrenghtPowerup = 0;
            DamageMultiplyer = 0;
            DamageReductionMultiplyer = 0;
            UsingStrenghtPowerup = false;
        }

        public void ResetSpeedPowerupVariables()
        {
            SpeedMultiplyer = 0;
            SpeedPowerupDuration = 0;
            TimeWhenPickedUpSpeedPowerup = 0;
            UsingSpeedPowerup = false;
        }
	}
}
