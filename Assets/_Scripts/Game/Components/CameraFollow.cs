﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour 
{	
	public Controller2D target;
	public float verticalOffset;
	public float lookAheadDstX;
	public float lookSmoothTimeX;
	public float verticalSmoothTime;
	public Vector2 focusAreaSize;

	public SpriteRenderer LevelBoundsSpriteRenderer;
	
	FocusArea focusArea;
	
	float currentLookAheadX;
	float targetLookAheadX;
	float lookAheadDirX;
	float smoothLookVelocityX;
	float smoothVelocityY;
	
	bool lookAheadStopped;

	private Vector2 _topLeftLimit;
	private Vector2 _bottomRightLimit;

	private Camera _cam;

	void Awake()
	{
		_cam = GetComponent<Camera>();
	}
	
	void Start() {
		focusArea = new FocusArea (target.CachedCollider.bounds, focusAreaSize);
		LevelBounds.GetCameraLimits(_cam, out _topLeftLimit, out _bottomRightLimit);
	}
	
	void LateUpdate() {
		focusArea.Update (target.CachedCollider.bounds);
		
		Vector2 focusPosition = focusArea.centre + Vector2.up * verticalOffset;
		
		if (focusArea.velocity.x != 0) {
			lookAheadDirX = Mathf.Sign (focusArea.velocity.x);
			if (Mathf.Sign(target.PlayerInput.x) == Mathf.Sign(focusArea.velocity.x) && target.PlayerInput.x != 0) {
				lookAheadStopped = false;
				targetLookAheadX = lookAheadDirX * lookAheadDstX;
			}
			else {
				if (!lookAheadStopped) {
					lookAheadStopped = true;
					targetLookAheadX = currentLookAheadX + (lookAheadDirX * lookAheadDstX - currentLookAheadX)/4f;
				}
			}
		}
		
		
		currentLookAheadX = Mathf.SmoothDamp (currentLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTimeX);
		
		focusPosition.y = Mathf.SmoothDamp (transform.position.y, focusPosition.y, ref smoothVelocityY, verticalSmoothTime);
		focusPosition += Vector2.right * currentLookAheadX;

		Vector3 rawPos = (Vector3)focusPosition + Vector3.forward * -10;
		Vector3 boundPos = new Vector3(Mathf.Clamp(rawPos.x, _topLeftLimit.x, _bottomRightLimit.x), Mathf.Clamp(rawPos.y, _bottomRightLimit.y, _topLeftLimit.y), rawPos.z);

		transform.position = boundPos;
	}
	
	void OnDrawGizmos() {
		Gizmos.color = new Color (1, 0, 0, .25f);
		Gizmos.DrawCube (focusArea.centre, focusAreaSize);
	}
	
	struct FocusArea {
		public Vector2 centre;
		public Vector2 velocity;
		float left,right;
		float top,bottom;
		
		
		public FocusArea(Bounds targetBounds, Vector2 size) {
			left = targetBounds.center.x - size.x/2;
			right = targetBounds.center.x + size.x/2;
			bottom = targetBounds.min.y;
			top = targetBounds.min.y + size.y;
			
			velocity = Vector2.zero;
			centre = new Vector2((left+right)/2,(top +bottom)/2);
		}
		
		public void Update(Bounds targetBounds) {
			float shiftX = 0;
			if (targetBounds.min.x < left) {
				shiftX = targetBounds.min.x - left;
			} else if (targetBounds.max.x > right) {
				shiftX = targetBounds.max.x - right;
			}
			left += shiftX;
			right += shiftX;
			
			float shiftY = 0;
			if (targetBounds.min.y < bottom) {
				shiftY = targetBounds.min.y - bottom;
			} else if (targetBounds.max.y > top) {
				shiftY = targetBounds.max.y - top;
			}
			top += shiftY;
			bottom += shiftY;
			centre = new Vector2((left+right)/2,(top +bottom)/2);
			velocity = new Vector2 (shiftX, shiftY);
		}
	}
	
}