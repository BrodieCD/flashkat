﻿using UnityEngine;

public class Checkpoint : MonoBehaviour 
{
	public Animator SpriteAnimator;
	private bool _closed = true;

    void OnTriggerEnter2D(Collider2D other)
	{
		if(_closed)
		{
			//print("checkpoint triggered");
			SpriteAnimator.SetTrigger("Open");
			CheckpointManager.instance.CheckPointActivated(transform.position);
			_closed = false;
		}
	}
}
