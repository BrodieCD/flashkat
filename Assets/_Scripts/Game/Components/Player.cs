﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(InputUser))]
[RequireComponent(typeof(CombatController))]
[RequireComponent(typeof(PlayerStatsBars))]
public class Player : Actor 
{
	const int PLAYER_ACTOR_ID = -1;

	public List<AttackData> AttackDatas;

	public float MinEnergyAfterSpecial = 1;
	private float _minEnergySpecialCost;

    #region Player movement fields
    public GameObject CloudPuff;

	public float MoveSpeed = 6;

    public float MaxJumpHeight = 4;
    public float MinJumpHeight = 1;
    public float TimeToJumpApex = .4f;

	public float MaxDelayForShortJump = 0.1f;

	public Vector2 WallJumpClimb = new Vector2(4, 16);
	public Vector2 WallJumpOff = new Vector2(8.5f, 7);
	public Vector2 WallLeap = new Vector2(18, 17);

	public string WallSlideAnimParam = "WallSliding";

    public float WallSlideSpeedMax = 3;
    public float WallStickTime = .5f;

	public float FallOffCheckVelocityY = -1;

	public GameObject Electricity;
	public GameObject Tornado;

	protected Dictionary<Attack.Names, AttackData> _attackDataByName;

    private float _accelerationTimeAirborne = .2f;
    private float _accelerationTimeGrounded = .1f;

    private int _numberOfJumps = 0;
    private float _wallUnstickTime;

    private float _maxJumpVelocity; 
    private float _minJumpVelocity;
	private float _nextJumpVelocity;
	private float _jumpButtonDownTime;

	private bool _isJumping = false;

    private float _velocityXSmoothing;
	private bool _isGoingOutFromWallSlide = false;

	private float _lastWallDirX = 0;
    #endregion

    public PowerupController PowerupControllerRef;

    private float _currentAnger;

	private PlayerStatsBars _playerStatsBars;
	private InputUser _inputUser;
	private CombatController _combatController;


	public float CurrentAnger
	{
		get { return _currentAnger; }
		set 
		{
			_currentAnger = value;

			if(_currentAnger < 0)
				_currentAnger = 0;
			else if(_currentAnger > MaxAnger)
				_currentAnger = MaxAnger;

			updateAngerBar();
		}
	}

	override public void Awake()
	{
		base.Awake();
		ActorId = PLAYER_ACTOR_ID;

		_inputUser = GetComponent<InputUser>();
		_playerStatsBars = GetComponent<PlayerStatsBars>();
		_combatController = GetComponent<CombatController>();
		PowerupControllerRef = GetComponent<PowerupController>();
	}

    void Start()
    {
		base.Initialize();
		CalculateJumpingVariables();
		_combatController.Initialize(Attacks, DoAttack, DoDefense);

		initializeAttackData();

		_currentAnger = StartingAnger;

		_playerStatsBars.ResetEnergyBar(_currentEnergy, MaxEnergy);
		_playerStatsBars.ResetAngerBar(_currentAnger, MaxAnger);

		Electricity.SetActive(false);
		Tornado.SetActive(false);
    }

    override public void Update()
    {
		base.Update();

        //		if(Input.GetMouseButtonDown((int)MouseButton.Right))
        //			print("Mouse R in animation: " + _currentAnimationName);

		//if(Input.GetKeyDown(KeyCode.I))
		
		alignFacingWithDirection();
		updateAnimState();

		PowerupControllerRef.UpdatePower(CalculateJumpingVariables, ref _gravity, ref _maxJumpVelocity, ref _minJumpVelocity);
        //Debug.Log(_gravity);
    }

	public void Respawn()
	{
		setAnimTrigger(AnimTriggers.Respawn);
	}

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<PickupableObject>() != null)
            col.GetComponent<PickupableObject>().PickupObject(this);

        if (col.GetComponent<TriggerZone>() != null)
        {
            col.GetComponent<TriggerZone>().TriggerEvent();
        }
    }

	override protected void updateEnergyBar()
	{
		_playerStatsBars.UpdateEnergyBar(_currentEnergy);	
		_inputUser.UpdateSpecialButtonVisibility(_currentEnergy > _minEnergySpecialCost);
	}

	private void initializeAttackData()
	{
		_minEnergySpecialCost = MaxEnergy - MinEnergyAfterSpecial;
		_attackDataByName = new Dictionary<Attack.Names, AttackData>();

		foreach(AttackData attackData in AttackDatas)
		{
			_attackDataByName.Add(attackData.AttackName, attackData);
			float energy = attackData.EnergyConsumption;
			if((energy != 0) && (energy < _minEnergySpecialCost))
				_minEnergySpecialCost = energy;
		}	
	}

	private void updateAngerBar()
	{
		_playerStatsBars.UpdateAngerBar(_currentAnger);	
	}

	private void CalculateJumpingVariables()
	{
        _gravity = -(2 * MaxJumpHeight) / Mathf.Pow(TimeToJumpApex, 2);
        _maxJumpVelocity = Mathf.Abs(_gravity) * TimeToJumpApex;
        _minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(_gravity) * MinJumpHeight);
	}

	private void alignFacingWithDirection()
	{
		if(_currentAnimationName == AnimationNames.WallSlide)
			return;

        if (_controller2d.PlayerInput.x != 0)
			Facing = _direction;
	}

	private void handlePlayerMovement()
	{
		Vector2 input = new Vector2(_inputUser.GetMoveH(), _inputUser.GetMoveV());
		bool isGrounded = _controller2d.IsGrounded;

        float targetVelocityX = input.x * MoveSpeed;

        if (PowerupControllerRef.powerupData.UsingSpeedPowerup)
            targetVelocityX *= PowerupControllerRef.powerupData.SpeedMultiplyer;

         _velocity.x = Mathf.SmoothDamp(_velocity.x, targetVelocityX, ref _velocityXSmoothing, isGrounded? _accelerationTimeGrounded : _accelerationTimeAirborne);

		applyGravity(false);

		//print("Player moveH:  " + input.x + " velocity.x: " + _velocity.x);

		_controller2d.Move(_velocity * Time.deltaTime, input, Facing, _inputUser.GetJumpButtonDown());

		_direction = (int)Mathf.Sign(_velocity.x);
	}

	private void handleGroundJump()
	{
		if(_inputUser.GetJumpButtonDown())
		{
			_jumpButtonDownTime = Time.time;
			exitLocomotion();
			jump();
		}
	}

	private void checkPlatformFall()
	{
		if(!_controller2d.IsGrounded && (_velocity.y <= FallOffCheckVelocityY))
		{
			exitLocomotion();
			fall();
		}
	}

	private void checkLanding()
	{
		if(_controller2d.IsGrounded)
			land();
	}

	private void handleAirJump()
	{
		if(_inputUser.GetJumpButtonDown())
		{
			if ((_inputUser.GetMoveV() < 0) && (_velocity.y > -_maxJumpVelocity))
	            _velocity.y = -_maxJumpVelocity;
			else if (_numberOfJumps < (2 + PowerupControllerRef.powerupData.ExtraJumps))
	        {
	            Instantiate(CloudPuff, transform.position + Vector3.down * transform.localScale.y / 2f, Quaternion.identity);
	            _velocity.y = _maxJumpVelocity;
	            _numberOfJumps++;
				SoundManager.PlayOneShotSound(this, FxAndSounds.SoundsNames.Jump);
	        }
		}
    }

	private bool handleWallSliding()
	{
		Controller2D.CollisionInfo collisions = _controller2d.Collisions;
		int facing = Facing;
		bool wallSliding = ((collisions.Left && (facing == GameConstants.DIR_LEFT)) || (collisions.Right && (facing == GameConstants.DIR_RIGHT)));
		AnimatorRef.SetBool(WallSlideAnimParam, wallSliding);	

		return wallSliding;
	}

	private void handleLocomotionMethods()
	{
		handlePlayerMovement();
		handleGrounding();
		handleGroundJump();
		updateAnimSpeedX();
		checkPlatformFall();
		_combatController.ReadAndWriteOnAttackBuffer(false, Facing);
	}

	#region Combat Controller methods
	override public void DoAttack(Attack.Names attackName, bool isCoolingDown)
	{
		bool isThereAttackData = _attackDataByName.ContainsKey(attackName);
		float energy = 0;
		float anger = 0;

		if(isThereAttackData)
		{
			AttackData attackData = _attackDataByName[attackName];
			energy = attackData.EnergyConsumption;
			anger = attackData.AngerIncrease;
		}

		if(CurrentEnergy > (energy + MinEnergyAfterSpecial)) // So you are left with at least, 1 point of health.
		{
			CurrentEnergy -= energy;
			CurrentAnger += anger;

			if(_isJumping)
				exitJump();
		
			base.DoAttack(attackName, isCoolingDown);
		}
		else
		{
			//print("You need more energy.");
			SoundManager.PlayOneShotSound(this, FxAndSounds.SoundsNames.NotEnoughEnergy);
			_playerStatsBars.HandleNotEnoughEnergy();
		}
	}

	override public void DoDefense(bool isRoll)
	{
		if(_isJumping)
			exitJump();

		base.DoDefense(isRoll);
	}

	override public bool GetHit(Attack attack, Vector2 attackerPos)
	{
		if(base.GetHit(attack, attackerPos))
		{
            if (PowerupControllerRef.powerupData.UsingStrenghtPowerup)
                CurrentAnger += attack.Damage * PowerupControllerRef.powerupData.DamageReductionMultiplyer;
            else
			    CurrentAnger += attack.Damage;

			return true;
		}

		return false;
	}

	override protected void genericAttackStartupInit()
	{ 		
		_combatController.HandleAttackStartupInit();
		GameEvents.Instance.OnPlayerAttackStartupInit.Emit(this);
	}

	override protected void genericAttackStartupUpdate()
	{
		base.genericAttackStartupUpdate();
		_combatController.HandleAttackInput(true, Facing);
	}

	override protected void genericAttackHitInit(Attack.Names attackName)
	{
		base.genericAttackHitInit(attackName);
		_combatController.HandleInitAttackHit(LastAttack.Name);
		GameEvents.Instance.OnPlayerAttackHitInit.Emit(this);
	}

	override protected void genericAttackHitUpdate(bool useGravity = true)
	{
		base.genericAttackHitUpdate(useGravity);
		_combatController.HandleAttackInput(true, Facing);
	}

	override protected void genericAttackCooldownInit()
	{
		_combatController.HandleAttackCooldownInit();
	}

	override protected void genericAttackCooldownUpdate()
	{
		base.genericAttackCooldownUpdate();
		_combatController.HandleAttackCooldownUpdate(Facing);
	}
	#endregion

	#region Animation State Machine
	public void OnEnterJump()
	{
		resetAnimTrigger(AnimTriggers.Jump);
		_isJumping = true;
	}

	public void OnLeaveJump()
	{
//		if(IsPlayer)
//			print("Leave locomotion");
		_isJumping = false;
	}

	private void exitJump()
	{
		setAnimTrigger(AnimTriggers.ExitJump);
	}

	private void jump()
	{
		setAnimTrigger(AnimTriggers.Jump);
	}

	private void fall()
	{
		//print("fall");
		setAnimTrigger(AnimTriggers.Fall);
	}

	private void land()
	{
		setAnimTrigger(AnimTriggers.Land);
	}

	override protected void idleUpdate()
	{
		base.Update();
		handleLocomotionMethods();
	} 
	
	override protected void walkingUpdate() 
	{
		handleLocomotionMethods();
	}
	
	override protected void trotingUpdate() 
	{
		handleLocomotionMethods();
	}

	override public void PunchLhitInit()
	{
		base.PunchLhitInit();
		genericAttackHitInit(Attack.Names.PunchL);
	}

	public void PunchRstartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.PunchRstartup);
	}

	public void PunchRhitInit()
	{
		genericAttackHitAnimStateStart(AnimationNames.PunchLhit, Attack.Names.PunchR);
	}

	public void PunchRCooldownInit()
	{
		genericAttackCooldownAnimStateStart(AnimationNames.PunchRcooldown);
	}

	public void KickLstartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.KickLstartup);
	}

	public void KickLhitInit()
	{
		genericAttackHitAnimStateStart(AnimationNames.KickLhit, Attack.Names.KickL);
	}

	public void KickLcooldownInit()
	{
		genericAttackCooldownAnimStateStart(AnimationNames.KickLcooldown);
	}

	public void SlidePunchStartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.SlidePunchStartup);
	}

	public void SlidePunchHitInit()
	{
		genericAttackHitAnimStateStart(AnimationNames.SlidePunchHit, Attack.Names.SlidePunch);
	}

	public void SlidePunchCooldownInit()
	{
		genericAttackCooldownAnimStateStart(AnimationNames.SlidePunchCooldown);
	}

	public void DragonKickStartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.DragonKickStartup);
	}

	public void DragonKickHitInit()
	{
		handleAnimStateStart(AnimationNames.DragonKickHit, raidenHitUpdate, genericAttackHitLeave);
		genericAttackHitInit(Attack.Names.DragonKick);
	}

	private void dragonKickHitUpdate()
	{
		genericAttackHitUpdate(false);
	}

	public void DragonKickCooldownInit()
	{
		genericAttackCooldownAnimStateStart(AnimationNames.DragonKickCooldown);
	}

	public void RageKickStartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.RageKickStartup);
	}

	public void RageKickHitInit()
	{
		genericAttackHitAnimStateStart(AnimationNames.RageKickHit, Attack.Names.RageKick);
	}

	public void RageKickCooldownInit()
	{
		genericAttackCooldownAnimStateStart(AnimationNames.RageKickCooldown);
	}

	public void RaidenStartupInit()
	{
		genericAttackStartupAnimStateStart(AnimationNames.RaidenStartup);
	}

	public void RaidenHitInit()
	{
		handleAnimStateStart(AnimationNames.RaidenHit, raidenHitUpdate, genericAttackHitLeave);
		genericAttackHitInit(Attack.Names.Raiden);
	}

	private void raidenHitUpdate()
	{
		genericAttackHitUpdate(false);
	}

	public void RaidenCooldownInit()
	{
		genericAttackCooldownAnimStateStart(AnimationNames.RaidenCooldown);
	}

	public void ElectricShootStartupInit() 
	{
		handleAnimStateStart(AnimationNames.ElectricShootStartup, genericAttackStartupUpdate, null);
		genericAttackStartupInit();
	}

	public void ElectricShootHitInit() 
	{
		handleAnimStateStart(AnimationNames.ElectricShootHit, genericAttackHitUpdate, electricShootHitLeave);
		genericAttackHitInit(Attack.Names.ElectricShoot);
		Electricity.SetActive(true);
	}

	private void electricShootHitLeave()
	{
		Electricity.SetActive(false);
		genericAttackHitLeave();
	}	

	public void ElectricShootCooldownInit() 
	{
		genericAttackCooldownAnimStateStart(AnimationNames.ElectricShootCooldown);
	}

	public void TornadoKickStartupInit() 
	{
		handleAnimStateStart(AnimationNames.TornadoKickStartup, genericAttackStartupUpdate, null);
		genericAttackStartupInit();
	}

	public void TornadoKickHitInit() 
	{
		handleAnimStateStart(AnimationNames.TornadoKickHit, genericAttackHitUpdate, tornadoKickHitLeave);
		genericAttackHitInit(Attack.Names.TornadoKick);
		Tornado.SetActive(true);
	}

	private void tornadoKickHitLeave()
	{
		Tornado.SetActive(false);
		genericAttackHitLeave();
	}	

	public void tornadoKickCooldownInit() 
	{
		genericAttackCooldownAnimStateStart(AnimationNames.TornadoKickCooldown);
	}

	public void FlamingFlashStartupInit() 
	{
		handleAnimStateStart(AnimationNames.FlamingFlashStartup, null, null);
		genericAttackStartupAnimStateStart(AnimationNames.FlamingFlashStartup);
	}

	public void FlamingFlashHit1Init() 
	{
		handleAnimStateStart(AnimationNames.FlamingFlashHit1, flamingFlashHitupdate, genericAttackHitLeave);
		genericAttackHitInit(Attack.Names.FlamingFlash1);
		_isInvulnerable = true;
	}

	private void flamingFlashHitupdate()
	{
		genericAttackHitUpdate(false);
	}

	public void FlamingFlashHit2Init() 
	{
		handleAnimStateStart(AnimationNames.FlamingFlashHit2, flamingFlashHitupdate, genericAttackHitLeave);
		genericAttackHitInit(Attack.Names.FlamingFlash2);
	}

	public void FlamingFlashHit3Init() 
	{
		handleAnimStateStart(AnimationNames.FlamingFlashHit3, flamingFlashHitupdate, genericAttackHitLeave);
		genericAttackHitInit(Attack.Names.FlamingFlash3);
	}

	public void FlamingFlashCooldownInit() 
	{
		handleAnimStateStart(AnimationNames.FlamingFlashCooldown, null, null);
		_isInvulnerable = false;
	}

	public void DragonRaidenPunchStartupInit() 
	{
		handleAnimStateStart(AnimationNames.DragonRaidenPunchStartup, null, null);
		genericAttackStartupInit();
	}

	public void DragonRaidenPunchHit1Init() 
	{
		handleAnimStateStart(AnimationNames.DragonRaidenPunchHit1, dragonRaidenPunchHit1update, genericAttackHitLeave);
		genericAttackHitInit(Attack.Names.DragonRaidenPunch1);
	}

	private void dragonRaidenPunchHit1update()
	{
		genericAttackHitUpdate(false);
	}

	public void DragonRaidenPunchHit2Init() 
	{
		handleAnimStateStart(AnimationNames.DragonRaidenPunchHit2, dragonRaidenPunchHit2update, genericAttackHitLeave);
		genericAttackHitInit(Attack.Names.DragonRaidenPunch2);
	}

	private void dragonRaidenPunchHit2update()
	{
		genericAttackHitUpdate(false);
	}

	public void DragonRaidenPunchHit3Init() 
	{
		handleAnimStateStart(AnimationNames.DragonRaidenPunchHit3, dragonRaidenPunchHit3update, genericAttackHitLeave);
		genericAttackHitInit(Attack.Names.DragonRaidenPunch3);
	}

	private void dragonRaidenPunchHit3update()
	{
		genericAttackHitUpdate(false);
	}

	public void DragonRaidenPunchCooldownInit() 
	{
		handleAnimStateStart(AnimationNames.DragonRaidenPunchCooldown, null, null);
	}

	override public void SuperHitInit()
	{
		base.SuperHitInit();
		genericAttackHitInit(Attack.Names.FlipKick);
	}

	public void StartRiseInit() 
	{
		handleAnimStateStart(AnimationNames.StartRise, StartRiseUpdate, StartRiseLeave);
		resetAnimTrigger(AnimTriggers.ExitLocomotion);
		_nextJumpVelocity = 0;
	}

	private void StartRiseUpdate() 
	{
		if(_nextJumpVelocity == 0)
		{
			if(_inputUser.GetJumpButton())
			{
				if ((Time.time - _jumpButtonDownTime) >= MaxDelayForShortJump)
					_nextJumpVelocity = _maxJumpVelocity;
			}
			else if(_inputUser.GetJumpButtonUp())
				_nextJumpVelocity = _minJumpVelocity;
		}
	}

	private void StartRiseLeave()
	{
		if(_nextJumpVelocity == -1)
			_nextJumpVelocity = _maxJumpVelocity;
	}

	public void RiseInit() 
	{
		handleAnimStateStart(AnimationNames.Rise, riseUpdate, null);

		if(_controller2d.IsGrounded)
		{
			SoundManager.PlayOneShotSound(this, FxAndSounds.SoundsNames.Jump);
			_velocity.y = _nextJumpVelocity;
		}
	}

	private void riseUpdate() 
	{
		handlePlayerMovement();
		handleAirJump(); 

        if (_controller2d.Collisions.Above)
            _velocity.y = 0;

		updateAnimVelocityY();
		_combatController.ReadAndWriteOnAttackBuffer(false, Facing);
	}

	public void StartFallInit() 
	{
		handleAnimStateStart(AnimationNames.StartFall, StartFallUpdate, null);
	}

	private void StartFallUpdate() 
	{
		handlePlayerMovement();
		handleAirJump();
		updateAnimVelocityY();
		_combatController.ReadAndWriteOnAttackBuffer(false, Facing);
	}

	public void FallInit() 
	{
		handleAnimStateStart(AnimationNames.Fall, FallUpdate, null);
	}

	private void FallUpdate() 
	{
		handlePlayerMovement();
		handleAirJump();
		
		if(handleGrounding())
			setAnimTrigger(AnimTriggers.Land);

		updateAnimVelocityY();
		if(!handleWallSliding())
			_combatController.ReadAndWriteOnAttackBuffer(false, Facing);
	}

	public void LandInit() 
	{
		handleAnimStateStart(AnimationNames.Land, null, null);
		_numberOfJumps = 0;
		SoundManager.PlayRandomOneShotSoundFromList(this, LandSounds);
	}

	public void WallSlidingInit() 
	{
		handleAnimStateStart(AnimationNames.WallSlide, WallSlidingUpdate, WallSlidingLeave);
		_numberOfJumps = 0;
		_wallUnstickTime = WallStickTime;
		_lastWallDirX = _controller2d.Collisions.Left? -1 : 1;
	}

	private void WallSlidingUpdate() 
	{
		if(_isGoingOutFromWallSlide)
			return;

		handlePlayerMovement();
		
		float axisH = _inputUser.GetMoveH(); 
		float inputX = (axisH == 0)? 0 : Mathf.Sign(axisH);
		
		float velocityLimitY = -WallSlideSpeedMax;

		if(_inputUser.GetMoveDownHold())
			velocityLimitY = -_maxJumpVelocity;

		if (_velocity.y < velocityLimitY)
		    _velocity.y = velocityLimitY;

//		print("inputX: " + inputX);
//		print("_lastWallDirX: " + _lastWallDirX);
		
		if ((inputX != _lastWallDirX) && (inputX != 0))
		{
		    _wallUnstickTime -= Time.deltaTime;
			_velocity.x = 0;
			_velocityXSmoothing = 0;
		
			if(_wallUnstickTime <= 0)
			{
				_isGoingOutFromWallSlide = true;
				AnimatorRef.SetBool(WallSlideAnimParam, false);	
				_isGoingOutFromWallSlide = true;
			}
		}
		else
		    _wallUnstickTime = WallStickTime;

        if (_inputUser.GetJumpButtonDown())
        {
			_isGoingOutFromWallSlide = true;

            if (_lastWallDirX == inputX)
            {
				//print("wall climb");
                _velocity.x = -_lastWallDirX * WallJumpClimb.x;
                _velocity.y = WallJumpClimb.y;
            } 
            else if (inputX == 0)
            {
				//print("jump off");
                _velocity.x = -_lastWallDirX * WallJumpOff.x;
                _velocity.y = WallJumpOff.y;
            }
            else 
            {
				//print("wall leap");
                _velocity.x = -_lastWallDirX * WallLeap.x;
                _velocity.y = WallLeap.y;
            }

			updateAnimVelocityY();
		}

		handleGrounding();
	    updateAnimSpeedY();
	}

	private void WallSlidingLeave()
	{
		AnimatorRef.SetBool(WallSlideAnimParam, false);
		_isGoingOutFromWallSlide = false;
	}

	override public void DyingInit()
	{
		base.DyingInit();
		GameEvents.Instance.OnPlayerDeath.Emit(this);
	}

//  Anim State Methods Template
//	virtual public void Init() 
//	{
//		handleAnimStateStart(AnimationNames, Update, Leave);
//	}
//
//	virtual private void Update() {}
//	virtual private void Leave(){}
	#endregion

	[System.Serializable]
	public class AttackData 
	{
		public Attack.Names AttackName;
		public float EnergyConsumption = 0;
		public float AngerIncrease = 0;
	}
}