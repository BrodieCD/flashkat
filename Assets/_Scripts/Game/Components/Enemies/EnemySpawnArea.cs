using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawnArea : MonoBehaviour 
{
    [Header("Variables")]
	public int MaxAmountToSpawn;
	
	public float MinDistanceBetweenEnemies;
	public float SeparationVelocity;	
	
	public EnemySpawnTriggerOptions EnemySpawnTrigger = EnemySpawnTriggerOptions.Leave;
	
    [Header("References")]
	public GameObject PatrolLimitPair;
	public Transform SpawnPositionTransform;
	public Enemy EnemyToSpawnPrefab;

	public EnemyCallBack RegisterEnemyCallback;
	public CheckFlagCallback CheckEnemyLimitCallback;

	[HideInInspector]
	public List<Enemy> Enemies = new List<Enemy>();

	private GameObject _areaEnemiesContainer;
	
    private int _index;
	
	public int Index { set { _index = value; } }
	public int EnemiesAmount { get { return Enemies.Count; } }
		
	void Awake()
	{

	}
	
	void Start()
	{
		if(PatrolLimitPair != null)
			PatrolLimitPair.SetActive(false);
	}

	void Update () 
	{
		if(MaxAmountToSpawn > 1)
			keepHorizontalDistanceBetweenEnemies();
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(EnemySpawnTrigger == EnemySpawnTriggerOptions.Enter)
			spawnEnemies();	
	}
	
	void OnTriggerExit(Collider other)
	{
		if(EnemySpawnTrigger == EnemySpawnTriggerOptions.Leave)	
			spawnEnemies();	
	}
	
	public void Initialize() 
	{
		GetComponent<Renderer>().enabled = false;
		SpawnPositionTransform.GetComponent<Renderer>().enabled = false;	
		
		_areaEnemiesContainer = new GameObject("AreaEnemiesContainer" + _index);		
		
		createEnemies();
		
		if(EnemySpawnTrigger == EnemySpawnTriggerOptions.Leave)
			spawnEnemies();
	}	
	
	private void createEnemies()
	{
		for(int i = 0; i < MaxAmountToSpawn; i++)
		{
			GameObject newEnemyGameObject = (GameObject)GameObject.Instantiate(EnemyToSpawnPrefab.gameObject, SpawnPositionTransform.position, Quaternion.identity);
			newEnemyGameObject.transform.parent = _areaEnemiesContainer.transform;
			
			Enemy enemy = newEnemyGameObject.GetComponent<Enemy>();
			enemy.Initialize();
			Enemies.Add(enemy);

			enemy.ExtractLimitsFromPair(PatrolLimitPair);
			newEnemyGameObject.SetActive(false);
		}		
	}

    private void createEnemies(int numberToSpawn)
    {
        for (int i = 0; i < numberToSpawn; i++)
        {
            GameObject newEnemyGameObject = (GameObject)GameObject.Instantiate(EnemyToSpawnPrefab.gameObject, SpawnPositionTransform.position, Quaternion.identity);
            newEnemyGameObject.transform.parent = _areaEnemiesContainer.transform;

            Enemy enemy = newEnemyGameObject.GetComponent<Enemy>();
            enemy.Initialize();
            Enemies.Add(enemy);

            enemy.ExtractLimitsFromPair(PatrolLimitPair);
            newEnemyGameObject.SetActive(false);
        }
    }

    public void ResetNumberOfMinionsToCheckpointValue(int numberOfEnemiesToSpawn)
    {
        foreach (Enemy enemy in Enemies)
        {
            if (enemy != null)
            Destroy(enemy.gameObject);
        }
        Enemies.Clear();

        createEnemies(numberOfEnemiesToSpawn);
        spawnEnemies();
    }
	
	private void spawnEnemies()
	{
		bool canSpawnEnemies = true;
		
		if(CheckEnemyLimitCallback != null)
		{
			if(CheckEnemyLimitCallback())
				canSpawnEnemies = false;
		}
		
		if(canSpawnEnemies)
		{
			foreach(Enemy enemy in Enemies)
			{
				if(!enemy.gameObject.activeSelf)
				{
					enemy.gameObject.SetActive(true);
					enemy.ReactivationReset();
				}
			}
		}
	}
	
	private void keepHorizontalDistanceBetweenEnemies()
	{	
		foreach(Enemy enemy in Enemies)
		{
            if (enemy != null)
            { 
			if(!enemy.gameObject.activeSelf)
				continue;

			bool startChecking = false;

			foreach(Enemy other in Enemies)
			{
				if(!startChecking)
				{
					if(enemy == other)
						startChecking = true;

					continue;
				}
	
				if(!other.gameObject.activeSelf)
					continue;

				if(enemy.Direction != other.Direction)
					continue;

				float enemyVelocityX = enemy.Velocity.x;
				float otherVelocityX = other.Velocity.x;

				if((enemyVelocityX == 0) && (otherVelocityX != 0))
					continue;

				if((enemyVelocityX != 0) && (otherVelocityX == 0))
					continue;				 

				float posDiffBetweenEnemies = enemy.PosX - other.PosX;
				bool otherIsLeft = (Mathf.Sign(posDiffBetweenEnemies) == GameConstants.DIR_RIGHT);

                    if (Mathf.Abs(posDiffBetweenEnemies) < MinDistanceBetweenEnemies)
                    {
                        float separationMovement = SeparationVelocity * Time.deltaTime;

                        //print ("separation movement: " + separationMovement);
                        if (otherIsLeft)
                        {
                            enemy.PosX += separationMovement;
                            enemy.limitRighMovement();

                            other.PosX -= separationMovement;
                            other.limitLeftMovement();
                        }
                        else // other is right
                        {
                            enemy.PosX -= separationMovement;
                            enemy.limitLeftMovement();

                            other.PosX += separationMovement;
                            other.limitRighMovement();
                        }
                    }
				}					
			}
		}
	}	
}
