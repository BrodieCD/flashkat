using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

//[RequireComponent(typeof(EnemyDodgeAI))]
 public abstract class Enemy : Actor 
{
	#region Enums
	public enum EnemyStates
	{
		Off,
		Chasing,
		Patrolling,
		Brawling,
		Firing,
		Menacing,	
		Waiting
	}
	
	protected enum _enemyEvents
	{
		TurnOff,
		Patrol,	
		Chase,
		Brawl,
		Fire,
		Menace,
		Wait
	}

	protected enum _decisions
	{
		None,
		ChangeDir,
		Rest,
		Walk,
		Fire,
		Special,
		Super,
		Punch,
		Kick,
		SpotDodge,
		RollDodge
	}
	#endregion

	#region Public fields
	static public Player PlayerRef;
	static public Transform PlayerTransform;
	static public Vector2 LastEnemyKilledPos;

    [Header("Variables")]
	public int NormalKillScore = 30;
	public int PerfectKillScore = 50;

	public float ChasePosLimitOffset = 1;
	public float ChasePosAdjustLerpTime = 0.1f;

	public float PatrolSpeed = 2.5f;
	public float MenaceSpeed = 5;
	public float ChaseSpeed = 10;
	
	public float OffActivationCheckDelay = 3;
	public float PatrolActivationCheckDelay = 1.5f;
	public float PatrolAndMenacePerceptionDelay = 1;
	public float ChasePerceptionDelay = 0.5f;

	public float DeactivationSquareRange = 400;

	public float PerceptionFarFrontRange = 10;
	public float PerceptionFarBackRange = 5;
	public float PerceptionNearRange = 2;
	public float PerceptionVerticalRange = 5;

	public float PatrolMinDecisionDelay = 1;
	public float PatrolMaxDecisionDelay = 4;

	public float MenaceMinDecisionDelay = 0.75f;
	public float MenaceMaxDecisionDelay = 2;

	public float ChaseMinDecisionDelay = 0.5f;
	public float ChaseMaxDecisionDelay = 2;

	public int CounterAttackPercentageEasy = 25;
	public int CounterAttackPercentageMedium = 50;
	public int CounterAttackPercentageHard = 75;

	public int ComboContinuePercentageEasy = 25;
	public int ComboContinuePercentageMedium = 50;
	public int ComboContinuePercentageHard = 75;

	public int ComboLimitEasy = 2;
	public int ComboLimitMedium = 3;
	public int ComboLimitHard = 4;

	public bool HasHitPlayer = false;

	public EnemyTypeOptions EnemyType;

	public SimpleCallback OnStunCallback;
	#endregion	

	#region Constants	
	const float RIGHT_DIR_PROBABILITY =	0.5f;
	#endregion 

	#region Protected fields
	protected Fsm _fsm;
	
	protected float _patrolLimitLeft;
	protected float _patrolLimitRight; 	
	
	protected float _activationTimeCount;
	protected float _perceptionTimeCount;
	
	protected int _currentCounterAttackPercentage = 50;
	protected int _currentComboContinuePercentage = 50;
	protected int _currentComboLimit = 3;
	protected int _comboCount = 0;

    protected SimpleCallback _currentMovementHandler;
	#endregion

	#region Private fields
	private float _meleeAttackDistanceTimeCount;

	private List<_decisions> _patrolDecisions;
	private List<_decisions> _menaceDecisions;
	private List<_decisions> _chaseDecisions;
	private List<_decisions> _brawlDecisions;

	private _decisions _currentDecisionException = _decisions.None;
	
	private float _perceptionCurrentDelay;	
	private float _activationCurrentDelay;
	#endregion
	
	#region Properties
	public Vector3 PlayerPosition { get { return PlayerTransform.position; } }
	public Vector3 VectorToPlayer { get { return (PlayerPosition - Position); } }
	public Vector3 VectorFromPlayer { get { return (Position - PlayerPosition); } }
	public Vector3 DirectionToPlayer { get { return VectorToPlayer.normalized; } }	
	public float SquareDistanceToPlayer { get { return VectorToPlayer.sqrMagnitude; } }
	public float DistanceToLeftLimit { get { return Mathf.Abs(PosX - _patrolLimitLeft); } }	
	public float DistanceToRightLimit { get { return Mathf.Abs(PosX - _patrolLimitRight); } }	
	public bool IsPlayerAtLeft { get { return(VectorToPlayer.x < 0); } }
	public EnemyStates State { get { return (EnemyStates)_fsm.CurrentStateKey; }  } 
	public bool IsActive { get { return _isActive; } }
	#endregion

	#region Monobehaviour methods
	override public void Update() 
	{	
		base.Update();

		if(_isActive)
		{
			updateAnimState();
			applyGravity();
			handleGrounding();

			if(Input.GetKeyDown(KeyCode.I))
				SpotDodge();
			else if(Input.GetKeyDown(KeyCode.O))
				RollDodge();
		}

		_fsm.Update();

		#if UNITY_EDITOR
		//drawPerceptionLines();
		#endif	
	}
	#endregion

	#region Test Methods
	#if UNITY_EDITOR
	private void drawPerceptionLines()
	{	
		if(_isActive)
		{
			float posX = PosX;
			float posY = PosY;

			bool isFarPerception = ((State == EnemyStates.Patrolling) || (State == EnemyStates.Menacing));
			int facing = Facing;
	
			float frontRange = isFarPerception? PerceptionFarFrontRange : PerceptionNearRange;
			float backRange = isFarPerception? PerceptionFarBackRange : PerceptionNearRange;
	
			float up = posY + PerceptionVerticalRange;
			float down = posY - PerceptionVerticalRange;
			float left = posX + facing * frontRange;
			float right = posX - facing * backRange;
			
			Debug.DrawLine(new Vector3(left, up, 0), new Vector3(right, up, 0), Color.cyan);
			Debug.DrawLine(new Vector3(right, up, 0), new Vector3(right, down, 0), Color.cyan);
			Debug.DrawLine(new Vector3(right, down, 0), new Vector3(left, down, 0), Color.cyan);
			Debug.DrawLine(new Vector3(left, down, 0), new Vector3(left, up, 0), Color.cyan);
		}
		else
		{
			Vector3 vectorToTurnOnLimit = Mathf.Sqrt(DeactivationSquareRange) * DirectionToPlayer;
			Debug.DrawLine(Position, Position + vectorToTurnOnLimit, Color.cyan);		
		}
	}
	#endif	
	#endregion

	#region Miscelaneous Methods

	override public void Initialize() 
	{
		base.Initialize();

		_patrolDecisions = new List<_decisions>() {_decisions.Walk, _decisions.ChangeDir, _decisions.Rest};
		_menaceDecisions = new List<_decisions>() {_decisions.Walk, _decisions.ChangeDir/*, _decisions.Fire*/};
		_chaseDecisions = new List<_decisions>() {_decisions.Walk, _decisions.Special, _decisions.Super/*, _decisions.RollDodge*/};
		_brawlDecisions = new List<_decisions>() {_decisions.Punch, _decisions.Kick, _decisions.Special, _decisions.Super/*, _decisions.SpotDodge, _decisions.RollDodge*/};

		//TODO: Adjust according to difficulty
		_currentCounterAttackPercentage = CounterAttackPercentageMedium;
		_currentComboContinuePercentage = ComboContinuePercentageMedium;

		initializeFsm();
	}

//	override protected void enableActivation(bool enabled)
//	{
////		goLocomotion();
////		_velocity.x = enabled? PatrolSpeed : 0;
////		updateAnimSpeedX();
//		base.enableActivation(enabled);
//	}

	#if UNITY_EDITOR
	protected void drawLineToPlayer(Color color)
	{
		//Debug.DrawLine(PlayerPosition, Position, color); 	
	}
	#endif

	public void ExtractLimitsFromPair(GameObject patrolLimitPair)
	{
		_patrolLimitLeft = patrolLimitPair.transform.FindChild("PatrolLimitLeft").position.x;
		_patrolLimitRight = patrolLimitPair.transform.FindChild("PatrolLimitRight").position.x;
	}	

	private void limitByDirection()
	{
		if(_actionDirection == GameConstants.DIR_LEFT)
			limitLeftMovement();
		else // right
			limitRighMovement();
	}

	public void limitLeftMovement()
	{
		if(PosX < _patrolLimitLeft)
			PosX = _patrolLimitLeft;
	}

	public void limitRighMovement()
	{
		if(PosX > _patrolLimitRight)
			PosX = _patrolLimitRight;
	}

	protected void adjustPosToChaseLimit(bool isPlayerAtLeft)
	{
		float posLimit = PlayerPosition.x; 
		bool shouldAdjust = false;

		if(isPlayerAtLeft)
		{
			posLimit += ChasePosLimitOffset;
			shouldAdjust = (PosX < posLimit);
		}
		else
		{
			posLimit -= ChasePosLimitOffset;
			shouldAdjust = (PosX > posLimit);		
		}	

		if(shouldAdjust)
			PosX = Mathf.Lerp(PosX, posLimit, ChasePosAdjustLerpTime);
	}
		
	protected void setRandomDirection()
	{
		if(GeneralUtils.GetActionProbability(RIGHT_DIR_PROBABILITY))
			Direction = GameConstants.DIR_LEFT;
		else
			Direction = GameConstants.DIR_RIGHT;
	}	
	
	protected void facePlayer()
	{
		Direction = IsPlayerAtLeft? GameConstants.DIR_LEFT: GameConstants.DIR_RIGHT;
	}	

	protected void invertDirection(bool flipFacing = true)
	{
		_direction *= -1;

		if(flipFacing)
			Facing = _direction;
	}

	public void ReactivationReset() 
	{
		CurrentEnergy = StartingEnergy;
		patrol();
	}



	override protected void genericAttackHitInit(Attack.Names attackName)
	{
		base.genericAttackHitInit(attackName);
		
	}

	override protected void genericAttackHitUpdate(bool useGravity = true)  //Used for both attack made and received
	{
		base.genericAttackHitUpdate(useGravity);
		limitByDirection();
	}

	override protected void genericAttackCooldownLeave()
	{
		if(!_isLeavingStateByStun)
			checkAttackContinues();
	}
	#endregion

	#region Timing Methods
	protected void setActivationTime(float delay)
	{
		_activationTimeCount = 0;
	}

	protected void updateActivationTime()
	{
		_perceptionTimeCount += Time.deltaTime;
		
		if(_perceptionTimeCount >= _perceptionCurrentDelay)
		{
			_perceptionTimeCount = 0;		
			if(SquareDistanceToPlayer > DeactivationSquareRange)
				turnOff();
			else
				patrol();
		}
	}

	protected void setPerceptionTime(float delay)
	{
		_perceptionTimeCount = 0;
		_perceptionCurrentDelay = delay;	
	}
	
	protected void updatePerceptionTime(SimpleCallback callback)
	{
		_perceptionTimeCount += Time.deltaTime;
		
		if(_perceptionTimeCount >= _perceptionCurrentDelay)
		{
			_perceptionTimeCount = 0;		
			callback();
		}
	}
	#endregion	

	#region Animation State Machine
	override protected void walkingUpdate()
	{
		_currentMovementHandler();
	}

	override public void PunchLCooldownInit()
	{
		base.PunchLCooldownInit();
		checkAttackContinues(true);
	}

	override public void KickRcooldownInit()
	{
		base.KickRcooldownInit();
		checkAttackContinues(true);
	}

	public void SpotDodge()
	{
		exitLocomotion();
		setAnimTrigger(AnimTriggers.SpotDodge);
	}

	public void RollDodge()
	{
		exitLocomotion();
		setAnimTrigger(AnimTriggers.RollDodge);
	}

	private void doSuper()
	{
		DoAttack(Attack.Names.FlyingKick);
	}

	private void doSpecial()
	{
		DoAttack(Attack.Names.Dash);
	}

	public void SpecialStartupInit()
	{
		handleAnimStateStart(AnimationNames.SpecialStartup, null, null);
	}

	public void SpecialHitInit()
	{
		genericAttackHitAnimStateStart(AnimationNames.SpecialHit, Attack.Names.Dash);
	}

	public void SpecialCooldownInit()
	{
		handleAnimStateStart(AnimationNames.SpecialCooldown, null, specialCooldownLeave);
		genericAttackCooldownInit();
	}

	protected void specialCooldownLeave()
	{
		if(_isLeavingStateByStun)
			return;

		if((State == EnemyStates.Brawling) || (State == EnemyStates.Waiting))
			checkAttackContinues();
		else // Chasing
		{
			//Same as taking the chase decision
			_velocity.x = ChaseSpeed;
			updateAnimSpeedX();
			_currentDecisionException = _decisions.Walk;  
		}
	}

	override public void SuperHitInit()
	{
		base.SuperHitInit();
		genericAttackHitInit(Attack.Names.FlyingKick);
	}

	override protected void superCooldownLeave()
	{
		specialCooldownLeave();
	}

	override public void StunnedStartInit() 
	{
		base.StunnedStartInit();
		Wait();

		if(OnStunCallback != null)
			OnStunCallback();
	}
	
	override protected void stunnedStayLeave() 
	{
		if(_isLeavingStateByStun)
			return;

		if(CurrentEnergy == 0)
		{
			exitLocomotion();
			die();
		}
		else if(GeneralUtils.GetActionProbability(_currentCounterAttackPercentage))
			checkAttackContinues(false);
		else 
			chase();
	}	

	override public void DyingInit() 
	{
		base.DyingInit();

		int scoreEarned = HasHitPlayer? NormalKillScore : PerfectKillScore;

		GameObject damageMessageGO = MessageManager.FireMessage(MessageData.Names.ScoreUpFade, Position);
		Text damageText = damageMessageGO.GetComponentInChildren<Text>();
		damageText.text = HasHitPlayer? scoreEarned.ToString() : "Perfect Kill!" + Environment.NewLine + scoreEarned.ToString();
		damageText.text += " pts";

		GameManager.Score += scoreEarned;
		
		GameEvents.Instance.OnScoreUpdate.Emit(this);

		LastEnemyKilledPos = Position;
	}

	override protected void rollDodgeMiddleUpdate()
	{
		base.rollDodgeMiddleUpdate();
		limitByDirection();
	}

	override protected void spotDodgeCooldownLeave()
	{
		specialCooldownLeave();
	}

	override protected void rollDodgeColldownLeave()
	{
		specialCooldownLeave();
	}
	#endregion

	#region A.I. Methods.
	protected void updatePatrolAndMenaceMovement()
	{
		float movementX = 0;
		bool isPatrol = (State == EnemyStates.Patrolling);
		
		if(_direction == GameConstants.DIR_LEFT)
		{
			movementX = -_velocity.x * Time.deltaTime;
			moveH(movementX);
			
			if(PosX < _patrolLimitLeft)
			{
				PosX = _patrolLimitLeft;
				invertDirection(isPatrol);
			}
		}
		else if(_direction == GameConstants.DIR_RIGHT)
		{
			movementX = _velocity.x * Time.deltaTime;
			moveH(movementX);
			
			if(PosX >= _patrolLimitRight)
			{
				PosX = _patrolLimitRight;
				invertDirection(isPatrol);
			}
		}			
	}

	protected void updateChaseMovement()
	{
		bool isPlayerAtLeft = IsPlayerAtLeft;

		float movementX = 0;

		//print("PosX: " + PosX + " limit L: " + _patrolLimitLeft + " limit R: " + _patrolLimitRight);

		if(isPlayerAtLeft)
		{
			movementX = -_velocity.x * Time.deltaTime;
			moveH(movementX);

			adjustPosToChaseLimit(true);

			if(PosX < _patrolLimitLeft)
			{
				PosX = _patrolLimitLeft;
				faceAndMenace();
			}
		}
		else
		{
			movementX = _velocity.x * Time.deltaTime;
			moveH(movementX);

			adjustPosToChaseLimit(false);
			
			if(PosX > _patrolLimitRight)
			{
				PosX = _patrolLimitRight;
				faceAndMenace();
			}
		}

		if((_directionLastFrame == GameConstants.DIR_LEFT) != isPlayerAtLeft)
		{
			Direction = isPlayerAtLeft? GameConstants.DIR_LEFT : GameConstants.DIR_RIGHT;
			_directionLastFrame = _direction;
		}
	}

	protected void makePatrolDecision()
	{
		_decisions decision = getDecision(_patrolDecisions);

		switch(decision)
		{
			case _decisions.ChangeDir:
				invertDirection(true);
				_currentDecisionException = _decisions.None;
				break;
			case _decisions.Rest:
				_velocity.x = 0;
				updateAnimSpeedX();
				_currentDecisionException = _decisions.Rest;
				break;
			case _decisions.Walk:
				_velocity.x = PatrolSpeed;
				updateAnimSpeedX();
				_currentDecisionException = _decisions.Walk;
				break;
		}

		//print("Made patrol decision: " + decision.ToString());
	}

	protected void makeMenaceDecision()
	{
		_decisions decision = getDecision(_menaceDecisions);

		switch(decision)
		{
			case _decisions.ChangeDir:
				invertDirection(false);
				_currentDecisionException = _decisions.None;
				break;
			case _decisions.Walk:
				_velocity.x = MenaceSpeed;
				updateAnimSpeedX();
				_currentDecisionException = _decisions.Walk;
				break;
			case _decisions.Fire:
				fire();
				break;
		}

		//print("Made menace decision: " + decision.ToString());
	}

	protected void makeChaseDecision()
	{
		_decisions decision = getDecision(_chaseDecisions);

		switch(decision)
		{
			case _decisions.Walk:
				_velocity.x = ChaseSpeed;
				updateAnimSpeedX();
				_currentDecisionException = _decisions.Walk;  
				break;
			case _decisions.Special:
				doSpecial();
				_currentDecisionException = _decisions.Special;
				break;
			case _decisions.Super:
				doSuper();
				_currentDecisionException = _decisions.Super;
				break;
			case _decisions.RollDodge:
				RollDodge();
				_currentDecisionException = _decisions.None;
				break;
			case _decisions.Fire:
				fire();
				_currentDecisionException = _decisions.Fire;
				break;
		}

		//print("Made chase decision: " + decision.ToString());		
	}

	protected void makeBrawlDecision()
	{
		facePlayer();

		_decisions decision = getDecision(_brawlDecisions);

		switch(decision)
		{
			case _decisions.Punch:
				punchL();
				_currentDecisionException = _decisions.Punch;
				break;
			case _decisions.Kick:
				kickR();	
				_currentDecisionException = _decisions.Kick;
				break;
			case _decisions.Special:
				doSpecial();	
				_currentDecisionException = _decisions.Special;
				break;
			case _decisions.Super:
				doSuper();
				_currentDecisionException = _decisions.Super;
				break;
			case _decisions.RollDodge:
				RollDodge();
				_currentDecisionException = _decisions.None;
				break;
			case _decisions.SpotDodge:
				SpotDodge();
				_currentDecisionException = _decisions.None;
				break;
		}

		//print("Made brawl decision: " + decision.ToString());				
	}

	protected _decisions getDecision(List<_decisions> stateDecisions)
	{
		List<_decisions> decisionsCopy = new List<_decisions>(stateDecisions);
		if(_currentDecisionException != _decisions.None)
				decisionsCopy.Remove(_currentDecisionException);

		_decisions decision = GeneralUtils.GetRandomItemFromList<_decisions>(decisionsCopy);

		//print("Decision made: " + decision.ToString() + " in state: " + State);

		return decision;
	}

	protected void doFarPerceptionCheck()
	{
		Vector3 vectorToPlayer = VectorToPlayer;
		if(Mathf.Abs(vectorToPlayer.y) <= PerceptionVerticalRange)
		{
			bool isPlayerAtLeft = IsPlayerAtLeft;
			float perceptionRange = (isPlayerAtLeft == (Facing == GameConstants.DIR_LEFT))? PerceptionFarFrontRange : PerceptionFarBackRange;
			float distanceToPlayerX = Mathf.Abs(vectorToPlayer.x);					

			if(distanceToPlayerX <= perceptionRange)
			{
				float distanceToLimit = isPlayerAtLeft? DistanceToLeftLimit : DistanceToRightLimit;
	
				if(distanceToPlayerX > distanceToLimit)
					faceAndMenace();
				else
					chase();
			} 
			else 
				patrol();
		}
		else
			patrol();
	}

	protected void doNearPerceptionCheck()
	{
		doBrawlPerceptionCheck();
	}

	protected bool doBrawlPerceptionCheck()
	{
		bool isBrawl = false;

		if(PlayerRef.IsAlive)
		{	
			Vector3 vectorToPlayer = VectorToPlayer;
			if(Mathf.Abs(vectorToPlayer.y) <= PerceptionVerticalRange)
			{	
				float distanceToPlayerX = Mathf.Abs(vectorToPlayer.x);		
				if(distanceToPlayerX <= PerceptionNearRange)
				{
					bool isPlayerAtLeft = IsPlayerAtLeft;
					float distanceToLimit = isPlayerAtLeft? DistanceToLeftLimit : DistanceToRightLimit;
		
					if(distanceToPlayerX > distanceToLimit)
						faceAndMenace();
					else
					{
						brawl();
						isBrawl = true;
					}
				} 
				else 
					chase();
			}
			else
				faceAndMenace();
		}
		else
			patrol();

		//isBrawl = true; //TODO: Remove hack

		return isBrawl;
	}

	protected void faceAndMenace()
	{
		facePlayer();
		menace();
	}

	protected void checkAttackContinues(bool isInitiatingCooldown = false)
	{
		if(doBrawlPerceptionCheck())
		{
			if(isInitiatingCooldown)
			{
				if(HitConfirmed)
				{
					_comboCount++;

					if((_comboCount < _currentComboLimit) && GeneralUtils.GetActionProbability(_currentComboContinuePercentage))
					{
						//print("Combo attempt! Count: " + _comboCount);
						exitAttack();
					}
					else
						_comboCount = 0;

					HitConfirmed = false;
				}
			}
			else
				makeBrawlDecision();
		}
	}
		
	#endregion

	#region A.I. State Machine
	public void Wait()
	{
		_fsm.HandleEvent(_enemyEvents.Wait);
	}

	protected void turnOff()
	{
		_fsm.HandleEvent(_enemyEvents.TurnOff);
	}
	
	protected void patrol()
	{
		_fsm.HandleEvent(_enemyEvents.Patrol);
	}
	
	protected void chase()
	{
		_fsm.HandleEvent(_enemyEvents.Chase);
	}	
	
	protected void brawl()
	{
		_fsm.HandleEvent(_enemyEvents.Brawl);
	}
	
	protected void fire()
	{
		_fsm.HandleEvent(_enemyEvents.Fire);
	}

	protected void menace()
	{
		_fsm.HandleEvent(_enemyEvents.Menace);
	}

	protected void initializeFsm()
	{
		_fsm = new Fsm();

		#if UNITY_EDITOR
//		_fsm.SetDebugLabel(name);
//		_fsm.EnableStateChangesPrint(true);
		#endif
		
		_fsm.AddState(EnemyStates.Off, offInit, offUpdate, offLeave);		
		_fsm.AddState(EnemyStates.Patrolling, patrollingInit, patrollingUpdate, null);
		_fsm.AddState(EnemyStates.Chasing, chasingInit, chasingUpdate, null);
		_fsm.AddState(EnemyStates.Brawling, brawlingInit, brawlingUpdate, null);
		_fsm.AddState(EnemyStates.Firing, firingInit, firingUpdate, null);
		_fsm.AddState(EnemyStates.Menacing, menacingInit, menacingUpdate, null);
		_fsm.AddState(EnemyStates.Waiting, null, waitingUpdate, waitingLeave);
		
		_fsm.AddEvent(_enemyEvents.TurnOff);		
		_fsm.AddEvent(_enemyEvents.Patrol);
		_fsm.AddEvent(_enemyEvents.Chase);	
		_fsm.AddEvent(_enemyEvents.Brawl);
		_fsm.AddEvent(_enemyEvents.Fire);
		_fsm.AddEvent(_enemyEvents.Menace);	
		_fsm.AddEvent(_enemyEvents.Wait);

		_fsm.SetRelation(EnemyStates.Off, _enemyEvents.Patrol, EnemyStates.Patrolling);

		_fsm.SetRelation(EnemyStates.Patrolling, _enemyEvents.TurnOff, EnemyStates.Off);
		_fsm.SetRelation(EnemyStates.Patrolling, _enemyEvents.Chase, EnemyStates.Chasing);
		_fsm.SetRelation(EnemyStates.Patrolling, _enemyEvents.Menace, EnemyStates.Menacing);
		_fsm.SetRelation(EnemyStates.Patrolling, _enemyEvents.Wait, EnemyStates.Waiting);
		
		_fsm.SetRelation(EnemyStates.Chasing, _enemyEvents.Menace, EnemyStates.Menacing);
		_fsm.SetRelation(EnemyStates.Chasing, _enemyEvents.Patrol, EnemyStates.Patrolling);
		_fsm.SetRelation(EnemyStates.Chasing, _enemyEvents.Fire, EnemyStates.Firing);
		_fsm.SetRelation(EnemyStates.Chasing, _enemyEvents.Brawl, EnemyStates.Brawling);
		_fsm.SetRelation(EnemyStates.Chasing, _enemyEvents.Wait, EnemyStates.Waiting);

		_fsm.SetRelation(EnemyStates.Menacing, _enemyEvents.Patrol, EnemyStates.Patrolling);
		_fsm.SetRelation(EnemyStates.Menacing, _enemyEvents.Chase, EnemyStates.Chasing);
		_fsm.SetRelation(EnemyStates.Menacing, _enemyEvents.Fire, EnemyStates.Firing);
		_fsm.SetRelation(EnemyStates.Menacing, _enemyEvents.Wait, EnemyStates.Waiting);

		_fsm.SetRelation(EnemyStates.Brawling, _enemyEvents.Patrol, EnemyStates.Patrolling);
		_fsm.SetRelation(EnemyStates.Brawling, _enemyEvents.Menace, EnemyStates.Menacing);
		_fsm.SetRelation(EnemyStates.Brawling, _enemyEvents.Chase, EnemyStates.Chasing);
		_fsm.SetRelation(EnemyStates.Brawling, _enemyEvents.Wait, EnemyStates.Waiting);

		_fsm.SetRelation(EnemyStates.Firing, _enemyEvents.Menace, EnemyStates.Menacing);
		_fsm.SetRelation(EnemyStates.Firing, _enemyEvents.Chase, EnemyStates.Chasing);
		_fsm.SetRelation(EnemyStates.Firing, _enemyEvents.Wait, EnemyStates.Waiting);

		_fsm.SetRelation(EnemyStates.Waiting, _enemyEvents.Patrol, EnemyStates.Patrolling);
		_fsm.SetRelation(EnemyStates.Waiting, _enemyEvents.Menace, EnemyStates.Menacing);
		_fsm.SetRelation(EnemyStates.Waiting, _enemyEvents.Chase, EnemyStates.Chasing);
		_fsm.SetRelation(EnemyStates.Waiting, _enemyEvents.Brawl, EnemyStates.Brawling);

		_fsm.Start(EnemyStates.Patrolling);
	}
	
	protected void offInit() 
	{
		enableActivation(false);
		setActivationTime(OffActivationCheckDelay);
	}

	protected void offUpdate() 
	{
		updateActivationTime();

		#if UNITY_EDITOR
		drawLineToPlayer(Color.black);
		#endif
	}

	protected void offLeave() 
	{
		enableActivation(true);
	}

	protected void patrollingInit() 
	{
		setRandomDirection();
		goLocomotion();
		_currentDecisionException = _decisions.None;
		makePatrolDecision();
		setRandomActionTime(PatrolMinDecisionDelay, PatrolMaxDecisionDelay);
		setPerceptionTime(PatrolAndMenacePerceptionDelay);
		setActivationTime(PatrolActivationCheckDelay);
		_currentMovementHandler = updatePatrolAndMenaceMovement;
	}

	protected void patrollingUpdate()
	{
		if(PlayerRef.IsAlive)
		{
			updatePerceptionTime(doFarPerceptionCheck);
			updateActivationTime();
		}

		updateActionTime(makePatrolDecision, true);


		#if UNITY_EDITOR
		drawLineToPlayer(Color.blue);
		#endif
	}

	protected void chasingInit() 
	{
		facePlayer();
		_directionLastFrame = Direction;
		goLocomotion();
		_currentDecisionException = _decisions.None;
		makeChaseDecision();
		setRandomActionTime(ChaseMinDecisionDelay, ChaseMaxDecisionDelay);
		setPerceptionTime(ChasePerceptionDelay);
		_currentMovementHandler = updateChaseMovement;
	}

	protected void chasingUpdate() 
	{
		if(_currentAnimationName == AnimationNames.Walk)
		{
			updatePerceptionTime(doNearPerceptionCheck);
			updateActionTime(makeChaseDecision, true);
		}

		#if UNITY_EDITOR
		drawLineToPlayer(Color.magenta);
		#endif
	}

	protected void firingInit() 
	{
		facePlayer();
		_velocity.x = 0;
	}

	protected void firingUpdate() 
	{
		#if UNITY_EDITOR
		drawLineToPlayer(Color.white);
		#endif
	}

	protected void menacingInit() 
	{
		facePlayer();
		goLocomotion();
		_currentDecisionException = _decisions.ChangeDir;
		makeMenaceDecision();
		setPerceptionTime(PatrolAndMenacePerceptionDelay);
		setRandomActionTime(MenaceMinDecisionDelay, MenaceMaxDecisionDelay);
		_currentMovementHandler = updatePatrolAndMenaceMovement;
	}

	protected void menacingUpdate()
	{
		updatePerceptionTime(doFarPerceptionCheck);
		updateActionTime(makeMenaceDecision);

		#if UNITY_EDITOR
		drawLineToPlayer(Color.yellow);		
		#endif
	}
	
	protected void brawlingInit() 
	{
		_comboCount = 0;
		makeBrawlDecision();
	}

	protected void brawlingUpdate() 
	{
		#if UNITY_EDITOR
		drawLineToPlayer(Color.red);
		#endif
	}

	protected void waitingUpdate() 
	{
		#if UNITY_EDITOR
		drawLineToPlayer(Color.grey);
		#endif
	}
	protected void waitingLeave() {}
	#endregion
	
}
