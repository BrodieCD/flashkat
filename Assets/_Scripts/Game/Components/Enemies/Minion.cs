﻿using UnityEngine;
using System.Collections;

public class Minion : MonoBehaviour {

    /*
    contains OnDrawGizmos and Awake.
    calculates and creates bounds.
    */
    #region Bounds
    [Header("Walkable Bounds")]
    GameObject bounds;

    string uniqueName;

    public float boundLeft = 5f;
    public float boundRight = 5f;
    [Range(2.5f, 10)]
    public float boundHeight = 5;

    public enum BoundDrawType {Sides, JoinedSides}
    public BoundDrawType drawType;

    Vector2 center;
    Vector2 boundRightPos;
    Vector2 boundLeftPos;

    void Awake()
    {
        CalculateBounds();
        CreateBounds();
    }

    void OnDrawGizmos()
    {
        if (!Application.isPlaying)
            CalculateBounds();

        Gizmos.color = Color.green;
        if (drawType == BoundDrawType.Sides)
        {
            Gizmos.DrawLine(new Vector3(boundRightPos.x, boundRightPos.y - boundHeight / 2f, 0), new Vector3(boundRightPos.x, boundRightPos.y + boundHeight / 2f, 0));
            Gizmos.DrawLine(new Vector3(boundLeftPos.x, boundLeftPos.y - boundHeight / 2f, 0), new Vector3(boundLeftPos.x, boundLeftPos.y + boundHeight / 2f, 0));
        }else if (drawType == BoundDrawType.JoinedSides)
        {
            Gizmos.DrawLine(new Vector3(boundRightPos.x, boundRightPos.y - boundHeight / 2f, 0), new Vector3(boundRightPos.x, boundRightPos.y + boundHeight / 2f, 0));
            Gizmos.DrawLine(new Vector3(boundLeftPos.x, boundLeftPos.y - boundHeight / 2f, 0), new Vector3(boundLeftPos.x, boundLeftPos.y + boundHeight / 2f, 0));
            Gizmos.DrawLine(new Vector3(boundRightPos.x, boundRightPos.y - boundHeight /2f, 0), new Vector3(boundLeftPos.x, boundLeftPos.y - boundHeight / 2f, 0));
            Gizmos.DrawLine(new Vector3(boundRightPos.x, boundRightPos.y + boundHeight / 2f, 0), new Vector3(boundLeftPos.x, boundLeftPos.y + boundHeight / 2f, 0));
        }
    }

    void CalculateBounds()
    {
        center = new Vector2(transform.position.x, transform.position.y);

        boundRightPos = center - (Vector2.up * transform.localScale.y / 2) + Vector2.up * ((float)boundHeight / 2) + Vector2.right * boundRight;
        boundLeftPos = center - (Vector2.up * transform.localScale.y / 2) + Vector2.up * ((float)boundHeight / 2) + Vector2.left * boundLeft;
    }

    void CreateBounds()
    {
        uniqueName = gameObject.name + " WalkableBounds";

        GameObject parent = new GameObject(uniqueName);
        bounds = parent;

        //CHANGE THIS INT TO UPDATE THE BOUND LAYER//
        int layerIndex = 10;

        Vector3 boundShape = new Vector3(0.1f, boundHeight, 1);

        GameObject boundLeft = new GameObject("Bound Left");
        boundLeft.transform.position = new Vector3(boundLeftPos.x, boundLeftPos.y, 0);
        boundLeft.transform.parent = parent.transform;
        boundLeft.AddComponent<BoxCollider2D>();
        boundLeft.GetComponent<BoxCollider2D>().isTrigger = true;
        boundLeft.layer = layerIndex;
        boundLeft.transform.localScale = boundShape;

        GameObject boundRight = new GameObject("Bound Right");
        boundRight.transform.position = new Vector3(boundRightPos.x, boundRightPos.y, 0);
        boundRight.transform.parent = parent.transform;
        boundRight.AddComponent<BoxCollider2D>();
        boundRight.GetComponent<BoxCollider2D>().isTrigger = true;
        boundRight.layer = layerIndex;
        boundRight.transform.localScale = boundShape;
    }

    #endregion


    [Header("Health & Damage")]
    public float maxHealth = 10;
    public float currentHealth = 0;

    public float baseDamage = 1;
    public float attackRange = 1;

    [Header("AI")]
    public Player target;
    public LayerMask boundsMask;
    public LayerMask groundedMask;
    public enum States {Idle, Walk, Fight}
    public States state;

    [Header("Movement")]
    public float moveSpeed = 2;

    public float agroRange = 10;
    public float stoppingDistance = 1.5f;

    float skinWidth = 0.05f;

    void Start()
    {
        currentHealth = maxHealth;
        target = FindObjectOfType<Player>();

        int walkDir = Random.Range((int)-1, (int)2);
        while (walkDir == 0)
        {
            walkDir = Random.Range((int)-1, (int)2);
        }
        StartCoroutine(Movement(walkDir));
    }


    //Movement//

    private bool Grounded()
    {
        if (Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.down, transform.localScale.y / 2f + skinWidth, groundedMask))
            return true;
         else
            return false;
    }

    IEnumerator Movement(int dir)
    {
        while (true)
        {
            while (state == States.Walk)
            {
                Vector2 playerPos2D = new Vector2(target.transform.position.x, target.transform.position.y);
                float distToPlayer = (new Vector2(transform.position.x - transform.localScale.x / 2f, 0) - new Vector2(playerPos2D.x - target.transform.localScale.x / 2f, 0)).magnitude;

                RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.right * dir, transform.localScale.x / 2 + 0.1f, boundsMask);
                if (hit)
                {
                    if (hit.transform.parent.name.ToString() == uniqueName)
                    dir *= -1;
                }

                if (Grounded())
                    transform.position += transform.right * moveSpeed * Time.deltaTime * dir;
                else
                    transform.position += (transform.right * moveSpeed * Time.deltaTime * dir) / 2;

                if (distToPlayer < agroRange)
                {
                    state = States.Fight;
                }
                yield return new WaitForEndOfFrame();
            }

            while (state == States.Fight)
            {
                Vector2 playerPos2D = new Vector2(target.transform.position.x, target.transform.position.y);
                Vector2 dirToPlayer = (new Vector2(transform.position.x, transform.position.y + (playerPos2D.y - transform.position.y)) - playerPos2D).normalized;
                float distToPlayer = (new Vector2(transform.position.x - transform.localScale.x / 2f, 0) - new Vector2(playerPos2D.x + target.transform.localScale.x / 2f, 0)).magnitude;

                if (distToPlayer > stoppingDistance && Grounded())
                    transform.position += transform.right * -dirToPlayer.x * moveSpeed * Time.deltaTime;
                else if (distToPlayer > stoppingDistance && !Grounded())
                    transform.position += (transform.right * -dirToPlayer.x * moveSpeed * Time.deltaTime) / 2;

                if (distToPlayer > agroRange)
                    state = States.Walk;

                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForEndOfFrame();
        }
    }

    //Health and Damage//
    public void TakeDamage(float dmg, float force, Vector2 pos)
    {
        currentHealth -= dmg;
        if (currentHealth <= 0)
            Die();

        Rigidbody2D rb = GetComponent <Rigidbody2D>();
        Vector2 dirToPos = (new Vector2(transform.position.x, transform.position.y) - pos).normalized;
        rb.AddForce(dirToPos * 15 + Vector2.up * 0.9f * force);
    }

    void Die()
    {
        Destroy(gameObject);
        Destroy(bounds);
    }
}