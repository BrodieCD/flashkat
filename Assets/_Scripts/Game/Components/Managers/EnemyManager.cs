using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour 
{	
	public int MaxEnemyAmount;	  	
	public Transform EnemySpawnAreasContainer;

	public List<EnemySpawnArea> _spawnAreas;



	void Awake()
	{		
		_spawnAreas = new List<EnemySpawnArea>();
		int index = 0;
		
		if(EnemySpawnAreasContainer != null)
		{
			foreach(EnemySpawnArea spawnArea in EnemySpawnAreasContainer.GetComponentsInChildren<EnemySpawnArea>())
			{
				spawnArea.CheckEnemyLimitCallback = CheckEnemiesAmountBeyondLimit;
				spawnArea.Index = index++;
				_spawnAreas.Add(spawnArea);
			}
		}	
	}
	
	void Start () 
	{	
		Enemy.PlayerRef = GameObject.Find("Player").GetComponent<Player>();
		Enemy.PlayerTransform = Enemy.PlayerRef.transform;
		
		foreach(EnemySpawnArea spawnArea in _spawnAreas)
			spawnArea.Initialize();
	}
	
	void Update () 
	{
				
	}
	
	void OnDestroy()
	{

	}
	
	public bool CheckEnemiesAmountBeyondLimit()
	{
		//Debug.Log ("_enemiesCount " + _enemiesCount);
		int enemiesCount = 0;
		
		foreach(EnemySpawnArea spawnArea in _spawnAreas)
			enemiesCount += spawnArea.EnemiesAmount;
			
		return(enemiesCount >= MaxEnemyAmount);
	}
	
	public Enemy GetClosestEnemy(Vector3 targetPosition)
	{
		Enemy closestEnemy = null;
		float closestSquareDistance = Mathf.Infinity;
		
		foreach(EnemySpawnArea spawnArea in _spawnAreas)
		{
			foreach(Enemy enemy in spawnArea.Enemies)
			{
				float squareDistanceToTarget = Vector3.SqrMagnitude(targetPosition - enemy.Position);
				if(squareDistanceToTarget < closestSquareDistance)
				{
					closestSquareDistance = squareDistanceToTarget;
					closestEnemy = enemy;
				}
			}
		}	
		
		return closestEnemy;
	}
	
	public Enemy GetFarthestEnemyInRange(Vector3 targetPosition, float maxRange)
	{
		Enemy farthestEnemy = null;
		float farthestSquareDistance = 0;
		
		foreach(EnemySpawnArea spawnArea in _spawnAreas)
		{
			foreach(Enemy enemy in spawnArea.Enemies)
			{
				float squareDistanceToTarget = Vector3.SqrMagnitude(targetPosition - enemy.Position);
				if((squareDistanceToTarget > farthestSquareDistance) && (squareDistanceToTarget <= maxRange))
				{
					farthestSquareDistance = squareDistanceToTarget;
					farthestEnemy = enemy;
				}
			}
		}	
		
		return farthestEnemy;
	}	
}
