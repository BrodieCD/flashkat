﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EntityManager : MonoBehaviour {

    static public EntityManager instance;
    public int index = 0;

    //Entity data and string (name)
    public Dictionary<string, EntityData> EntityDictionary = new Dictionary<string, EntityData>();

    void Awake()
    {
        instance = this;
    }

    public string GenerateUniqueKey(string objName)
    {
        string key = objName + index.ToString();

        while (EntityDictionary.ContainsKey(key))
        {
            index++;
        }

        index++;
        return key;
    }

    public void AddEntityDataToDictionary(string uniqueKey, EntityData data)
    {
        EntityDictionary.Add(uniqueKey, data);
    }

    public void RemoveEntityFromDictionary(string uniqueKey)
    {
        if (EntityDictionary.ContainsKey(uniqueKey))
        {
            EntityDictionary.Remove(uniqueKey);
        }
    }

    public void RemoveEntities()
    {
        List<string> _keys = new List<string>(EntityDictionary.Keys);
        foreach (string key in _keys)
        {
            GameObject obj = GameObject.Find(key);
            if (obj != null)
                Destroy(obj);

            EntityDictionary.Remove(key);
        }

        EntityDictionary.Clear();
    }

    public void SpawnEntity(EntityData data)
    {
        Instantiate(data.prefab, data.position, Quaternion.Euler(data.rotation));
    }

	public EntityData FindEntityDataUsingObjectName(string name)
    {
        if (EntityDictionary.ContainsKey(name))
        {
            return (EntityDictionary[name]);
        }
        else
        {
            Debug.Log("This entity is not present in the entity dictionary");
            return null;
        }
    }
}
