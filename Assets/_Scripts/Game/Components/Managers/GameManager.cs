﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{
	static public int StartingLives = 4;
	static public int StartingScore = 0;

	static public int ScoreToCompleteLevel = 170;

	public bool MusicEnabled = true;
	static public float RespawnDelay = 3;

	public GameObject LevelCompletionJacket;
	public InputUser InputUserRef;

	public Text LevelText;
	public Text ScoreText;
	public Text LivesAmount;

	static public int Lives = 4;
	static public int Level = 1;
	static public int Score = 0;

	void Awake()
	{
		GameEvents.Instance.OnPlayerDeath.Connect(this, OnPlayerDeath);
		GameEvents.Instance.OnScoreUpdate.Connect(this, OnScoreUpdate);
	}

	void Start()
	{
		if(Application.platform != RuntimePlatform.WindowsEditor)  //TODO: Remove
			MusicEnabled = true;

		if(MusicEnabled)
			SoundManager.PlayMusic(this, FxAndSounds.SongsNames.TestSong);

		if(Application.platform == RuntimePlatform.Android)
			Instantiate(Resources.Load("Prefabs/GUI/MobileControlsCanvas"));

		LevelText.text = "LVL. " + Level.ToString();
		LivesAmount.text = Lives.ToString();	
		updateScoreDisplay();
	}

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Return) || InputUserRef.GetResetButtonDown())
		{
			resetStats();
			reloadLastScene();
		}

		if(Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}

	void OnDestroy()
	{
		GameEvents.Instance.OnPlayerDeath.Disconnect(this);
		GameEvents.Instance.OnScoreUpdate.Disconnect(this);
	}

    public void OnPlayerDeath(object dispatcher)
    {
        Lives--;
        if (Lives < 0)
            Lives = 0;

        if (CheckpointManager.instance.checkpoints[CheckpointManager.instance.checkpointIndex].active)
        {
            CheckpointManager.instance.Invoke("Respawn", RespawnDelay);
        } else {
            Invoke("reloadLastScene", RespawnDelay);
        }
    }

	public void OnScoreUpdate(object dispatcher)
	{
		updateScoreDisplay();
	}

	private static void resetStats()
	{
		Lives = StartingLives;
		Score = StartingScore;
	}

	private void updateScoreDisplay()
	{
		if(Score >= ScoreToCompleteLevel)  
		{
			if(!LevelCompletionJacket.activeSelf)
				LevelCompletionJacket.SetActive(true);
		}

        LivesAmount.text = Lives.ToString();
		ScoreText.text = "Score: " + Score.ToString() + " / " + ScoreToCompleteLevel.ToString() + " pts";
	}

	private void reloadLastScene()
	{
		Score = 0;
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
