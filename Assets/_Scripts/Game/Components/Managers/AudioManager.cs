﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public static AudioManager instance;

    public AudioSource musicSource;
    public AudioSource sfxSource;

    void Awake()
    {
        DontDestroyOnLoad(this);
        instance = this;
    }

    void Start()
    {
        UpdateVolume();
    }

    public void UpdateVolume()
    {
        musicSource.volume = PlayerPrefs.GetFloat("MusicVol", 1);
        sfxSource.volume = PlayerPrefs.GetFloat("SFXVol", 1);
    }
}
