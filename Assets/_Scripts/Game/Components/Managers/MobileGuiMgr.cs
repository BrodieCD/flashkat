﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MobileGuiMgr : MonoBehaviour 
{
	public List<MobileButton> MobileButtons;

	public MobileArea StickBG;
	public MobileArea StickPivot;
	public MobileArea BasicButtonsBG;

	private MobileButton _buttonStick;

	private Dictionary<MobileButton.Names, MobileButton> _mobileButtonsByName = new Dictionary<MobileButton.Names, MobileButton>();
	private Dictionary<int, MobileButton.Names> _fingerIdsAtButtonNames = new Dictionary<int, MobileButton.Names>();
	
//	private int _camFirstFingerId = -1;
//	private int _camSecondFingerId = -1;
	
	private Vector2 _camMoveDelta = Vector2.zero;
	
//	private float _pinchDelta = 0;
	
//	private Vector2 _leftStickDragOffset = Vector2.zero;

	private float _stickMaxDistance = -1;
	private Array _buttonNames;

	void Start () 
	{
		//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "MobileGuiMgrStart", false);
		
		_buttonNames = Enum.GetValues(typeof(MobileButton.Names));
	
		initializeAllButtons();
		//updateStickMaxDistance();

		GameEvents.Instance.OnMobileGuiMgrSent.Emit(this, this);
	}
	
	void Update() 
	{
		//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "MobileGuiMgrUpdate", true);
		foreach(MobileButton.Names buttonName in _buttonNames)
			_mobileButtonsByName[buttonName].HandleTransitoryStates();

		if((_stickMaxDistance == -1) && (StickBG.ScreenRect.width != 0))
			updateStickMaxDistance();
	
		handleTouchInput();
	}

	public void SpecialButtonEnable(bool enabled)
	{
		_mobileButtonsByName[MobileButton.Names.Special].IsActive = enabled;
	}

	public float GetStickAxisH()
	{
		float stickShiftX = _buttonStick.ScreenRect.x - StickPivot.ScreenRect.x;
		return stickShiftX/_stickMaxDistance;		
	}
	
	public float GetStickAxisV()
	{
		float stickShiftY = _buttonStick.ScreenRect.y  - StickPivot.ScreenRect.y;
		return stickShiftY/_stickMaxDistance;		
	}
	
	public float GetCamAxisH()
	{
		return _camMoveDelta.x;
	}
	
	public float GetCamAxisV()
	{
		return _camMoveDelta.y;
	}
	
//	public float GetPinchDelta()
//	{
//		return _pinchDelta;
//	}
	
	public bool GetButtonDown(MobileButton.Names buttonName)
	{
		return (_mobileButtonsByName[buttonName].State == ButtonStates.Down);
	}
	
	public bool GetButton(MobileButton.Names buttonName)
	{
		return (_mobileButtonsByName[buttonName].State == ButtonStates.Pressed);
	}
	
	public bool GetButtonUp(MobileButton.Names buttonName)
	{
		return (_mobileButtonsByName[buttonName].State == ButtonStates.Up);
	}

	private void initializeAllButtons()
	{
		foreach(MobileButton button in MobileButtons)
			_mobileButtonsByName.Add(button.ButtonName, button);

		_buttonStick = _mobileButtonsByName[MobileButton.Names.Stick];
	}

	private void updateStickMaxDistance()
	{
		_stickMaxDistance = (StickBG.ScreenRect.width - _buttonStick.ScreenRect.width) * 0.5f;
	}

	private bool checkButtonRect(MobileButton.Names buttonName, int fingerId, Vector2 touchGuiPos)
	{
		bool result = false;
		MobileButton button = _mobileButtonsByName[buttonName];

		//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "touchGuiPos: " + touchGuiPos.ToString() + " button.ScreenRect: " + button.ScreenRect.ToString(), true);

		if(button.ScreenRect.Contains(touchGuiPos))
		{
			if(button.IsActive && button.IsIdle)
			{
				button.SetDown(); 
				_fingerIdsAtButtonNames.Add(fingerId, buttonName);	
			}

			result = true;
		}
		
		return result;
	}

//	private void handlePinchZoom()
//	{
//		Vector2 touchOnePos = Vector2.zero;
//		Vector2 touchTwoPos = Vector2.zero;
//		
//		Vector2 prevTouchOnePos = Vector2.zero;
//		Vector2 prevTouchTwoPos = Vector2.zero;
//		
//		int touchCount = Input.touchCount;
//		for(int i = 0; i < touchCount; i++)
//		{
//			Touch touch = Input.GetTouch(i);
//			if(touch.fingerId == _camFirstFingerId)
//			{
//				touchOnePos = touch.position;
//				prevTouchOnePos = touchOnePos - touch.deltaPosition;
//			}
//			else if(touch.fingerId == _camSecondFingerId)
//			{
//				touchTwoPos = touch.position;
//				prevTouchTwoPos = touchTwoPos - touch.deltaPosition;
//			}
//		}
//		
//		float prevPinchDistance = (prevTouchOnePos - prevTouchTwoPos).magnitude;
//		float pinchDistance = (touchOnePos - touchTwoPos).magnitude;
//		
//		_pinchDelta = pinchDistance - prevPinchDistance;
//	}

	private void handleTouchInput()
	{
		//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "handleTouchInput", true);
		int touchCount = Input.touchCount;
		for(int i = 0; i < touchCount; i++)
		{
			Touch touch = Input.GetTouch(i);
			int fingerId = touch.fingerId;
			TouchPhase phase = touch.phase;
			Vector2 touchPos = touch.position;
			
			if(phase == TouchPhase.Began)
			{
				//bool isCamArea = true;
				bool isBasicButtonArea = false; 
				bool isStickArea = false;
				
				if(BasicButtonsBG.Contains(touchPos))
				{
					isBasicButtonArea = true;	
					//isCamArea = false;
				}	
				else if(StickBG.Contains(touchPos))
				{
					isStickArea = true;		
					//isCamArea = false;
				}
				
				if(isStickArea)
				{
					//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "isStickArea true", false);
					//Rect stickRect = ButtonStick.ScreenRect;
					
//					if(checkButtonRect(ButtonNames.Stick, fingerId, touchPos))
//						_leftStickDragOffset = stickRect.position - touchPos;
//					else
					if(!checkButtonRect(MobileButton.Names.Stick, fingerId, touchPos))
					{
						_buttonStick.SetPosition(touchPos);
						_buttonStick.SetDown(); 
						_fingerIdsAtButtonNames.Add(fingerId, MobileButton.Names.Stick);
					}
				}
				else 
				{
					foreach(MobileButton.Names buttonName in Enum.GetValues(typeof(MobileButton.Names)))
					{
						if(isStickArea)
							break;
						
						if(isBasicButtonArea && (buttonName > MobileButton.Names.Jump))
							break; 		
						
						if(checkButtonRect(buttonName, fingerId, touchPos))
						{					
							//isCamArea = false;
							break;
						}
					}
				}
				
//				if(isCamArea)
//				{
//					if(_camFirstFingerId == -1)
//						_camFirstFingerId = fingerId;	
//					else
//						_camSecondFingerId = fingerId;
//				}
			}
			else if(phase == TouchPhase.Moved)
			{		
//				if(fingerId == _camFirstFingerId)
//				{
//					if(_camSecondFingerId == -1)
//						_camMoveDelta = touch.deltaPosition;
//					else
//						handlePinchZoom();
//				}
//				else if(fingerId == _camSecondFingerId)
//					handlePinchZoom();
				/*else*/ if(_fingerIdsAtButtonNames.ContainsKey(fingerId))
				{
					if(_fingerIdsAtButtonNames[fingerId] == MobileButton.Names.Stick)
					{
						_buttonStick.SetPosition(new Vector2(touchPos.x/* + _leftStickDragOffset.x*/, touchPos.y/* + _leftStickDragOffset.y*/));
						
						Vector2 adjustedToShifted = _buttonStick.ScreenRect.center - StickPivot.ScreenRect.center;
						if(adjustedToShifted.magnitude > _stickMaxDistance)
						{
							Vector2 adjustedToShiftedLimited = adjustedToShifted.normalized * _stickMaxDistance;
							_buttonStick.SetPosition(StickPivot.ScreenRect.center + adjustedToShiftedLimited);
						}
					}
				}
			}	
			else if(phase == TouchPhase.Ended)
			{
//				if(fingerId == _camFirstFingerId)
//				{
//					if(_camSecondFingerId != -1)
//					{
//						_camFirstFingerId = _camSecondFingerId;
//						_camSecondFingerId = -1;
//						_pinchDelta = 0;
//					}
//					else
//						_camFirstFingerId = -1;
//					
//					_camMoveDelta = Vector2.zero;
//				}
//				else if(fingerId == _camSecondFingerId)
//				{
//					_camSecondFingerId = -1;
//					_pinchDelta = 0;
//				}
				/*else*/ if(_fingerIdsAtButtonNames.ContainsKey(fingerId))
				{
					MobileButton.Names buttonUpName = _fingerIdsAtButtonNames[fingerId];
					_mobileButtonsByName[buttonUpName].SetUp();
					
					if(buttonUpName == MobileButton.Names.Stick)
						_buttonStick.SetPosition(StickPivot.ScreenRect.center);
					
					_fingerIdsAtButtonNames.Remove(fingerId);
				}
			}
		}
	}
}
