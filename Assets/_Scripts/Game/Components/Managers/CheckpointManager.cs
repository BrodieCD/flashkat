﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CheckpointManager : MonoBehaviour {

    static public CheckpointManager instance;

    public int checkpointIndex = 0;
    public CheckpointData[] checkpoints;

    public GameObject checkpointPrefab;

    Player playerRef;

    Dictionary<EnemySpawnArea, int> SpawnAreasDictionary = new Dictionary<EnemySpawnArea, int>();
    
    bool firstCheckpointActivated = false;

    void Awake()
    {
        instance = this;
        checkpoints = new CheckpointData[Silverside.instance.NumberOfSwoopsWithCheckpoints];
        playerRef = FindObjectOfType<Player>();
    }

    public void Respawn()
    {
        if (GameManager.Lives > 0)
        {
            //Forces the game manager to update the score txts and lives txts so they are reset.
            GameManager.Score = checkpoints[checkpointIndex].score;
            GameEvents.Instance.OnScoreUpdate.Emit(this);
 
            if (playerRef != null)
            {
                playerRef.Position = checkpoints[checkpointIndex].checkpointPos;
                playerRef.CurrentEnergy = playerRef.StartingEnergy;
                playerRef.Respawn();
                Invoke("EnablePlayerGameObject", .1f);
            }

            foreach (EnemySpawnArea spawnArea in FindObjectOfType<EnemyManager>()._spawnAreas)
            {
                if (SpawnAreasDictionary.ContainsKey(spawnArea))
                {
                    spawnArea.ResetNumberOfMinionsToCheckpointValue(SpawnAreasDictionary[spawnArea]);
                }
            }

            // +1 because timesSwooped gets updated after the swoop not as the swoop happens (it causes silver to swoop on respawn if you dont add 1)
            Silverside.instance.timesSwooped = checkpoints[checkpointIndex].numberOfTimesSwooped + 1;

            //Entity management
            EntityManager.instance.RemoveEntities();
            List<EntityData> _data = new List<EntityData>(checkpoints[checkpointIndex].entityDictionary.Values);
            foreach (EntityData data in _data)
            {
                EntityManager.instance.SpawnEntity(data);
            }
        }
    }

    void EnablePlayerGameObject()
    {
        if (playerRef != null)
        {
            playerRef.gameObject.SetActive(true);
        }
    }

    public void SpawnCheckpoint(Vector3 pos)
    {
        Instantiate(checkpointPrefab, pos, Quaternion.identity);
    }

	public void CheckPointActivated(Vector3 pos)
	{
        UpdateSpawnAreasDictionary();

        if (firstCheckpointActivated)
        	checkpointIndex++;

        firstCheckpointActivated = true;

        checkpoints[checkpointIndex].active = true;
        checkpoints[checkpointIndex].checkpointPos = pos + Vector3.up * 2;
        checkpoints[checkpointIndex].score = GameManager.Score;
        checkpoints[checkpointIndex].numberOfTimesSwooped = Silverside.instance.timesSwooped;

        Invoke("UpdateEntityDictionary", 1.25f);
	}

    void UpdateEntityDictionary()
    {
        checkpoints[checkpointIndex].entityDictionary = new Dictionary<string, EntityData>();
        List<string> keys = new List<string>(EntityManager.instance.EntityDictionary.Keys);
        foreach (string key in keys)
        {
            checkpoints[checkpointIndex].entityDictionary.Add(key, EntityManager.instance.FindEntityDataUsingObjectName(key));
        }
        //Debug.Log(checkpoints[checkpointIndex].entityDictionary.Keys.Count);
    }
 
    void UpdateSpawnAreasDictionary()
    {
        SpawnAreasDictionary.Clear();
        List<EnemySpawnArea> enemySpawnAreas = FindObjectOfType<EnemyManager>()._spawnAreas;

        for (int i = 0; i < enemySpawnAreas.Count; i ++)
        {
            int num = 0;
            foreach (Enemy enemy in enemySpawnAreas[i].Enemies)
            {
                if (enemy.IsAlive)
                    num++;
            }

            SpawnAreasDictionary.Add(enemySpawnAreas[i], num);
        }
    }

    [System.Serializable]
    public struct CheckpointData
    {
        public bool active;

        //spawn data
        public Vector3 checkpointPos;

        public int score;

        //silver data
        public int numberOfTimesSwooped;

        public Dictionary<string, EntityData> entityDictionary;
    }
}
