﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MessageManager : MonoBehaviour 
{
	public List<MessageData> MessagesData = new List<MessageData>();
	
	public Camera GameCamera;
	public RectTransform CanvasRectTransform;

	private Dictionary<MessageData.Names, MessageData> _messageDataByname = new Dictionary<MessageData.Names, MessageData>();
	private Dictionary<MessageData.Names, Transform> _containerByName = new Dictionary<MessageData.Names, Transform>();

	static private MessageManager _instance;

	void Awake()
	{
		_instance = this;
	}

	void Start () 	
	{
		foreach(MessageData messageData in MessagesData)
		{
			_messageDataByname.Add(messageData.Name, messageData);
			GameObject messageContainer = new GameObject(messageData.Name.ToString() + "Container");
			messageContainer.transform.parent = this.transform;
			messageContainer.transform.localPosition = new Vector3(0, 0, 0);
			messageContainer.transform.localScale = new Vector3(1, 1, 1);
			
			_containerByName.Add(messageData.Name, messageContainer.transform);

			for(int i = 0; i < messageData.InstancesAmount; i++)
				createInstance(messageData.Name);
		}
	}

	static public GameObject FireMessage(MessageData.Names name, Vector3 pos)
	{
		return _instance.fireMessage(name, pos);
	}
	
	private GameObject fireMessage(MessageData.Names name, Vector3 pos)
	{
		#if UNITY_EDITOR
		if(!_containerByName.ContainsKey(name))
			Debug.LogError("MessageData with name : " + name + " has not been added.");
		#endif
	
		Transform container = _containerByName[name];
		GameObject instanceFired = null;

		foreach(Transform messageTransform in container)
		{
			GameObject messageGO = messageTransform.gameObject;

			if(!messageGO.activeSelf)
			{
				messageGO.SetActive(true);
				instanceFired = messageGO;
				break;
			}
		}	

		if(instanceFired == null)
		{
			Debug.LogWarning("Instance for: " + name.ToString() + " not found. Creating extra one for the pool.");
			instanceFired = createInstance(name);
		}

		RectTransform rectTransform = instanceFired.GetComponent<RectTransform>();
		rectTransform.position = pos;

		return instanceFired;
	}

	private GameObject createInstance(MessageData.Names name)
	{
		GameObject messageGO = (GameObject)Instantiate(_messageDataByname[name].TextPrefab);
		messageGO.transform.SetParent(_containerByName[name]);
		messageGO.transform.localScale = new Vector3(1, 1, 1);

		return messageGO;
	}
}
