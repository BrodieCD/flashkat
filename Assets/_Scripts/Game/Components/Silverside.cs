﻿using UnityEngine;
using System.Collections;

public class Silverside : MonoBehaviour {

    public SpriteRenderer spriteRenderer;
    public static Silverside instance;
    public Vector3[] nodes;
    public GameObject checkpointCrate;

    [Header("Auto-Swoop Variables")]
    public int timesSwooped;
    public SwoopData[] swoopData;

    [Header("Movement Variables")]
    public bool swooping;
    public float moveSpeed;

    public bool randomSwoopDir;
    public enum SwoopDirections {Left, Right}
    public SwoopDirections swoopDirection;
    float timeBetweenNodes;
    float timeWhenPastLastNode;
    int currentNodeIndex;

    [Header("Swoop path Variables")]
    public int numberOfNodes;
    public float horizontalDistance;
    public float gradientCoeficient;
    public float verticalOffset;
    bool hasCalculatedNodes;

    bool hasBeenGivenSwoopData;
    SwoopData givenSwoopData;

    public int NumberOfSwoopsWithCheckpoints
    {
        get
        {
            int num = 0;
            for (int i = 0; i < swoopData.Length; i ++)
            {
                if (swoopData[i].dropCheckpoint)
                    num++;
            }
            TriggerZone[] triggers = FindObjectsOfType<TriggerZone>();
            for (int j = 0; j < triggers.Length; j ++)
            {
                if (triggers[j].triggerZoneType == TriggerZone.TriggerZoneTypes.SilversideSwoop)
                {
                    if (triggers[j].swoopData.dropCheckpoint)
                        num++;
                }
            }

            return num;
        }
        private set { }
    }

    bool hasMadeDrop;

    void Awake()
    {
        instance = this;
        nodes = new Vector3[2 * numberOfNodes];
        spriteRenderer.enabled = false;
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        /*
        Gizmos.color = Color.green;
        float sphereRadius = .35f;

        if (Application.isPlaying)
        {
            for (int i = -numberOfNodes; i < numberOfNodes; i++)
            {
                Gizmos.DrawSphere(nodes[numberOfNodes + i], sphereRadius);
                Gizmos.DrawLine(nodes[numberOfNodes + i], nodes[numberOfNodes + i + 1]);
            }
        }
        */
    }
#endif


    void Update()
    {
        if (timesSwooped < swoopData.Length)
        {
            if (GameManager.Score >= (swoopData[timesSwooped].percentageScoreCompletion / 100) * GameManager.ScoreToCompleteLevel)
            {
				StartSwoop(Enemy.LastEnemyKilledPos);
            }
        }

        if (swooping)
        {
            if (Time.time > timeWhenPastLastNode + timeBetweenNodes)
            {
                if (swoopDirection == SwoopDirections.Left)
                {
                    currentNodeIndex--;

                    if (currentNodeIndex == numberOfNodes -1)
                    {
                        if (!hasMadeDrop)
                        {
                            if (!hasBeenGivenSwoopData)
                                swoopData[timesSwooped].MakeDrop();
                            else
                                swoopData[0].MakeDrop(givenSwoopData);

                            hasMadeDrop = true;
                        }
                    }
                }

                if (swoopDirection == SwoopDirections.Right)
                {
                    currentNodeIndex++;

                    if (currentNodeIndex == numberOfNodes +1)
                    {
                        if (!hasMadeDrop)
                        {
                            if (!hasBeenGivenSwoopData)
                                swoopData[timesSwooped].MakeDrop();
                            else
                                swoopData[0].MakeDrop(givenSwoopData);

                            hasMadeDrop = true;
                        }
                    }
                }
           

                if (currentNodeIndex > 2 * numberOfNodes - 1 || currentNodeIndex < 0)
                {
                    if (!hasBeenGivenSwoopData)
                    timesSwooped++;

                    hasBeenGivenSwoopData = false;
                    currentNodeIndex = 0;
                    hasMadeDrop = false;
                    hasCalculatedNodes = false;
                    swooping = false;
                    spriteRenderer.enabled = false;
                }

                //Debug.Log(currentNodeIndex);
                timeWhenPastLastNode = Time.time;
            }

            transform.LookAt(nodes[currentNodeIndex]);
            transform.position += transform.forward * moveSpeed * Time.deltaTime;
        }
    }

    public void StartSwoop(Vector3 rootPosition)
    {
        if (randomSwoopDir)
        {
            int a = Random.Range(0, 2);
            Debug.Log(a.ToString());
            if (a == 0)
                swoopDirection = SwoopDirections.Left;
            else
                swoopDirection = SwoopDirections.Right;
        }

        if (!hasCalculatedNodes)
        {
            CalculateNodes(rootPosition);
            hasCalculatedNodes = true;
        }

        swooping = true;
        spriteRenderer.enabled = true;
    }

    public void StartSwoop(Vector3 rootPosition, SwoopData SwoopData)
    {
        givenSwoopData = SwoopData;
        hasBeenGivenSwoopData = true;

        if (randomSwoopDir)
        {
            int a = Random.Range(0, 2);
            if (a == 0)
                swoopDirection = SwoopDirections.Left;
            else
                swoopDirection = SwoopDirections.Right;
        }

        if (!hasCalculatedNodes)
        {
            CalculateNodes(rootPosition);
            hasCalculatedNodes = true;
        }

        swooping = true;
        spriteRenderer.enabled = true;
    }

    void CalculateNodes(Vector3 rootPos)
    {
        nodes = new Vector3[2 * numberOfNodes];
        float nodeDistance = (2 * horizontalDistance) / numberOfNodes;

        for (int i = -numberOfNodes; i < numberOfNodes; i++)
        {
            float x = (float)(i * nodeDistance);
            float y = rootPos.y + verticalOffset + gradientCoeficient * Mathf.Pow(x, 2);

            Vector3 nodePos = new Vector3(x + rootPos.x, y, 0);
            nodes[numberOfNodes + i] = nodePos;
        }

        timeBetweenNodes = (float)((nodes[0] - nodes[1]).magnitude) / moveSpeed - Time.deltaTime;
        //Debug.Log(timeBetweenNodes);

        if (swoopDirection == SwoopDirections.Left)
        {
            currentNodeIndex = 2 * numberOfNodes - 1;
            transform.position = nodes[currentNodeIndex];
        }
        else
        {
            transform.position = nodes[0];
        }
    }

    [System.Serializable]
    public struct SwoopData
    { 
        [Range(0,100)]
        public float percentageScoreCompletion;
        public bool dropCheckpoint;

        public GameObject[] itemsToDrop;

        public void MakeDrop()
        {
            Vector3 spawnPos = instance.transform.position - Vector3.up * 2;

            for (int i = 0; i < itemsToDrop.Length; i++)
            {
                Instantiate(itemsToDrop[i], spawnPos + Vector3.right * i, Quaternion.identity);
            }
            if (dropCheckpoint)
            {
                Instantiate(instance.checkpointCrate, spawnPos, Quaternion.identity);
            }
        }

        public void MakeDrop(SwoopData data)
        {
            Vector3 spawnPos = instance.transform.position - Vector3.up * 2;

            for (int i = 0; i < data.itemsToDrop.Length; i++)
            {
                Instantiate(data.itemsToDrop[i], spawnPos + Vector3.right * i, Quaternion.identity);
            }
            if (data.dropCheckpoint)
            {
                Instantiate(instance.checkpointCrate, spawnPos, Quaternion.identity);
            }
        }
    }
}
