﻿using UnityEngine;
using System.Collections;

public class FadeOverTime : MonoBehaviour {

    public Renderer myRenderer;

    [Header("Variables")]
    public bool fadeUpponInstantiation;
    public bool fadeIn;
    public bool destroyAfterFade;
    public float fadeTime;

    public AnimationCurve fadeCurve;

    float fadeAmmountCoefficient;
    float fadeCurveRelativeTime = 0;

    void Start()
    {
        if (fadeUpponInstantiation)
            StartFade();

        if (destroyAfterFade)
        Destroy(gameObject, fadeTime + 0.5f);
    }

    public void StartFade(bool _fadeIn, bool _destroyAfterFade, float _fadeTime)
    {
        fadeIn = _fadeIn;
        fadeTime = _fadeTime;
        if (_destroyAfterFade)
            Destroy(gameObject, fadeTime + 0.5f);

        StartCoroutine(Fade());
    }

    public void StartFade(bool _fadeIn, bool _destroyAfterFade)
    {
        fadeIn = _fadeIn;
        if (destroyAfterFade)
            Destroy(gameObject, fadeTime + 0.5f);

        StartCoroutine(Fade());
    }

    public void StartFade()
    {
        StartCoroutine(Fade());
    }

    IEnumerator Fade()
    {
        Material mat = myRenderer.material;
        Color colour = mat.color;
        float fadeAmmountPerFrame = (1f / fadeTime) * Time.deltaTime;

        if (fadeIn)
        {
            colour.a = 0;
            mat.color = colour;

            while (colour.a < 1)
            {
                fadeCurveRelativeTime += Time.deltaTime / fadeTime;
                fadeAmmountCoefficient = fadeCurve.Evaluate(fadeCurveRelativeTime);

                colour.a += (fadeAmmountCoefficient < 0) ? fadeAmmountCoefficient * fadeAmmountPerFrame : ((1 - fadeAmmountPerFrame) + fadeAmmountCoefficient) * fadeAmmountPerFrame;
                mat.color = colour;

                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            while (colour.a > 0)
            {
                fadeCurveRelativeTime += Time.deltaTime / fadeTime;
                fadeAmmountCoefficient = fadeCurve.Evaluate(fadeCurveRelativeTime);

                colour.a -= (fadeAmmountCoefficient < 0)? fadeAmmountCoefficient * fadeAmmountPerFrame : ((1 - fadeAmmountPerFrame) + fadeAmmountCoefficient) * fadeAmmountPerFrame;
                mat.color = colour;

                yield return new WaitForEndOfFrame();
            }
        }
    }
}
