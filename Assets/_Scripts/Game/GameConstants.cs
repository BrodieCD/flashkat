﻿using UnityEngine;
using System.Collections;

public class GameConstants 
{
	public const int DIR_LEFT = -1;
	public const int DIR_RIGHT = 1;

	public const int SQUARE_POW = 2;
}
