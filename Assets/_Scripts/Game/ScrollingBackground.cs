﻿using UnityEngine;
using System.Collections;

public class ScrollingBackground : MonoBehaviour {

    public enum ScrollDirections {Left, Right}
    [Header("Variables")]
    public ScrollDirections scrollDirection;
    public float scrollSpeed;
    public float scrollResetDist;

    Vector3 originalPos;

    void Start()
    {
        originalPos = transform.position;
    }

    void Update()
    {
        if (originalPos.x - transform.position.x >= scrollResetDist || originalPos.x - transform.position.x <= -scrollResetDist)
            transform.position = originalPos;

        if (scrollDirection == ScrollDirections.Left)
        {
            transform.position += -Vector3.right * scrollSpeed * Time.deltaTime;
        }else
        {
            transform.position += Vector3.right * scrollSpeed * Time.deltaTime;
        }
    }

}
