﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MessageData  
{
	public enum Names
	{
		None,
		DamageUpFade,
		ScoreUpFade
	}

	public Names Name;
	public GameObject TextPrefab;
	public int InstancesAmount; 
}
