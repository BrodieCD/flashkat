﻿using UnityEngine;
using System.Collections;

public class TriggerZone : MonoBehaviour {

	public enum TriggerZoneTypes
    {
        Achievement,
        SilversideSwoop
    }

    public bool hasBeenTriggered;

    [Header("Visualization")]
    public bool drawTriggerZone;
    public Color colour;

    [Header("Traits")]
    public TriggerZoneSize triggerZoneSize;
    public TriggerZoneTypes triggerZoneType;

    [Header("Swoop Data")]
    public Silverside.SwoopData swoopData;


    Player playerRef;

    void Start()
    {
        playerRef = FindObjectOfType<Player>();
        myCol = GetComponent<BoxCollider2D>();
        myCol.size = new Vector2(triggerZoneSize.scaleHorizontal, triggerZoneSize.scaleVertical);
    }

    BoxCollider2D myCol;

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = colour;

        if (drawTriggerZone)
        {
            Gizmos.DrawCube(transform.position, new Vector3(triggerZoneSize.scaleHorizontal, triggerZoneSize.scaleVertical, 0));
        }
    }
#endif 

    public void TriggerEvent()
    {
        if (!hasBeenTriggered)
        {
            //Silver swoop
            if (triggerZoneType == TriggerZoneTypes.SilversideSwoop)
            {
                Silverside.instance.StartSwoop(playerRef.Position, swoopData);
            }
        }
        hasBeenTriggered = true;
    }


    [System.Serializable]
    public struct TriggerZoneSize
    {
        [Range(1, 100)]
        public float scaleVertical;
        [Range(1,100)]
        public float scaleHorizontal; 
    }
}
