using UnityEngine;
using System.Collections;

public class GameUtils
{
	static public Vector3 GetAimToWorldPoint(Vector3 planeNormal, Vector3 planePoint, Vector3 screenPoint)
	{
		Vector3 targetPoint = new Vector3();
		
	    // Generate a plane that intersects the release spot transform's position with a forward normal.
	    Plane bowPlane = new Plane(planeNormal, planePoint);
	    
	    // Generate a ray from the cursor position
	    Ray ray = Camera.main.ScreenPointToRay(screenPoint);
	    
	    // Determine the point where the cursor ray intersects the plane.
	    // This will be the point that the object must look towards to be looking at the mouse.
	    // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
	    //   then find the point along that ray that meets that distance.  This will be the point
	    //   to look at.
	    float hitdistance = 0.0f;
	    // If the ray is parallel to the plane, Raycast will return false.
	    if(bowPlane.Raycast(ray, out hitdistance)) 
		{
	        // Get the point along the ray that hits the calculated distance.
	        targetPoint = ray.GetPoint(hitdistance);
		}	
		
		return targetPoint;
	}	
}
