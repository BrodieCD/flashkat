﻿using UnityEngine;
using System.Collections;

public class ParallaxBackground : MonoBehaviour {

    public bool _enabled;

    [Header("References")]
    public Transform[] backgrounds;

    [Header("Variables")]
    public bool inverseMoveDirection;
    public float[] parallaxScales;
    public float smoothing;
    Vector3 camPosOld;

    void Start()
    {
        transform.position = transform.position;
        camPosOld = transform.position;
    }

    void LateUpdate()
    {
        if (_enabled)
        {
            float moveValue = (camPosOld.x - transform.position.x);
            for (int i = 0; i < backgrounds.Length; i++)
            {
                float _moveValue = moveValue * parallaxScales[i];
                if (inverseMoveDirection)
                    _moveValue *= -1;

                Vector3 targetPos = backgrounds[i].position + Vector3.right * _moveValue;
                backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, targetPos, smoothing * Time.deltaTime);
            }
            camPosOld = transform.position;
        }
    }
}
