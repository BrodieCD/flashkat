using UnityEngine;


public enum InGameStates
{
	Restarting,	
	Playing,
	GameOver,
}

public enum InGameEvents
{
	Restart,	
	Play,
	Lose,
	Pause
}

public enum MouseButtonOptions
{
	Left = 0,
	Right,
	Middle
}

public enum EnemyTypeOptions
{
	None,
	Speed
}

public enum EnemySpawnTriggerOptions
{
	Enter,
	Leave
}

public enum RespawnType
{
	None,
	DeathRespawn,
	CliffRespawn
}

public enum InstantSfxNames
{
	None,
	DustParticles,
	FireballImpact,
}

public enum BulletNames
{
	Fireball,
}

public enum AnimTriggers
{
	None,
	GoLocomotion,
	ExitLocomotion,
	PunchL,
	PunchR,
	KickL,
	KickR,
	Special,
	Super,
	ExitCooldown,
	Stun,
	Die,
	Jump,
	Fall,
	Land,
	Raiden,
	SlidePunch,
	ExitJump,
	ElectricShoot,
	DragonRaidenPunch,
	FlamingFlash,
	FlipKick,
	Respawn,
	SpotDodge,
	RollDodge,
	DragonKick,
	RageKick,
	TornadoKick
}