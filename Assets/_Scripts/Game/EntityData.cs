﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EntityData
{
    public string dictionaryKey;

    public GameObject prefab;
    public Vector3 rotation;
    public Vector3 position;

    public EntityData(string key, GameObject _prefab, Vector3 pos, Vector3 rot)
    {
        dictionaryKey = key;
        prefab = _prefab;
        position = pos;
        rotation = rot;
    }
}
