﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Enemy))]
public class EnemyDodgeAI : MonoBehaviour 
{
	[Range(0, 1)]
	public float NearDodgeProbability = 0.4f;

	[Range(0, 1)]
	public float FarDodgeProbability = 1;

	[Range(0, 1)]
	public float RollDodgeProbability = 0.5f;

	public float FarDodgeMaxAngle = 30;
	public float NearDodgeMaxSquareDistance = 9;

	public float NearDodgeMaxDelay = 0.3f;
	public float FarDodgeMaxDelay = 0.3f;
	public float FarDodgeMinDelay = -0.2f;

	private const float _NEAR_DODGE_MIN_DELAY = 0;	

	private bool _nearDodgePending = false;
	private bool _farDodgePending = false;

	private float _nextDodgeDelay;

	private float _dodgeTimeCount;

	private Enemy _enemyUser;

	void Awake()
	{
		_enemyUser = GetComponent<Enemy>();
		_enemyUser.OnStunCallback = CancelDodge;

		GameEvents.Instance.OnPlayerAttackStartupInit.Connect(this, OnPlayerAttackStartupInit);
		GameEvents.Instance.OnPlayerAttackHitInit.Connect(this, OnPlayerAttackHitInit); 
	}
	
	void Update () 
	{
		if(_nearDodgePending)
			updateDodge(true);
		else if(_farDodgePending)
			updateDodge(false);
	}

	void OnDestroy()
	{
		GameEvents.Instance.OnPlayerAttackStartupInit.Disconnect(this);	
		GameEvents.Instance.OnPlayerAttackHitInit.Disconnect(this); 	
	}

	#region Signal Callbacks
	public void OnPlayerAttackStartupInit(object dispatcher)
	{
		handleDodge(true);
	}

	public void OnPlayerAttackHitInit(object dispatcher)
	{
		handleDodge(false);		
	}
	#endregion

	public void CancelDodge()
	{
		_nearDodgePending = false;
		_farDodgePending = false;
	}

	private void handleDodge(bool isNear)
	{
		if(_enemyUser.IsActiveInLocomotion)
		{
			//print("square distance to player: " + _enemyUser.SquareDistanceToPlayer);
			if(isNear == (_enemyUser.SquareDistanceToPlayer <= NearDodgeMaxSquareDistance))
			{
				if(GeneralUtils.GetActionProbability(isNear? NearDodgeProbability : FarDodgeProbability))
				{
					//print("angle: " + Vector3.Angle(_enemyUser.VectorFromPlayer, Enemy.PlayerRef.SignedVelocity));
					if(isNear || (Vector3.Angle(_enemyUser.VectorFromPlayer, Enemy.PlayerRef.SignedVelocity) <= FarDodgeMaxAngle))
					{
						_enemyUser.Wait();  //We pause AI so the enemy won't do another action via AI while the dodge is pending

						_dodgeTimeCount = 0;
						_nearDodgePending = isNear;
						_farDodgePending = !isNear;
			
//						if(_nearDodgePending)
//							print("near dodge!");
//						else
//							print("far dodge!");

						_nextDodgeDelay = Random.Range(isNear? _NEAR_DODGE_MIN_DELAY : FarDodgeMinDelay, isNear? NearDodgeMaxDelay : FarDodgeMaxDelay);

						if(!isNear)
						{
							//We add approximate impact time t = d / v ;
							_nextDodgeDelay += _enemyUser.VectorFromPlayer.magnitude / Enemy.PlayerRef.Velocity.magnitude;

							if(_nextDodgeDelay < 0)
								_nextDodgeDelay = 0;
						}
					}
				}
			}
		}
	}

	private void updateDodge(bool isNear)
	{
		_dodgeTimeCount += Time.deltaTime;

		if(_dodgeTimeCount >= _nextDodgeDelay)
		{
			dodge();

			if(isNear)
				_nearDodgePending = false;
			else
				_farDodgePending = false;
		}
	}

	private void dodge()
	{
		//print("dodge!");
		if(Random.Range(0, 1) > RollDodgeProbability)
			_enemyUser.RollDodge();
		else
			_enemyUser.SpotDodge();
	}
}
