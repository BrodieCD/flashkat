using UnityEngine;

public delegate void GameObjectCallback(GameObject target);
public delegate void SimpleCallback();
public delegate void PositionCallback(Vector3 position);
public delegate void EnemyCallBack(Enemy enemy);
public delegate bool CheckFlagCallback();
public delegate void GetReusableBulletCallback(ReusableBullet bullet);
