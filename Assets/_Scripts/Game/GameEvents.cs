using UnityEngine;
using System.Collections;

public class GameEvents : Singleton<GameEvents>
{
	public SignalSimple OnPlayerDeath = new SignalSimple("PlayerDeath");
	public SignalSimple OnScoreUpdate = new SignalSimple("ScoreUpdate");

	public SignalSimple OnPlayerAttackStartupInit = new SignalSimple("PlayerAttackStartupInit");
	public SignalSimple OnPlayerAttackHitInit = new SignalSimple("PlayerAttackHitInit");
	
	public SignalBulletShot OnBulletShot = new SignalBulletShot("BulletShot");

    public Signal<ReusableBulletMgr> OnReusableBulletMgrRefSent = new Signal<ReusableBulletMgr>("ReusableBulletMgrRefSent");
	public SignalReusableBulletShot OnReusableBulletShot = new SignalReusableBulletShot("ReusableBulletShot");

	public Signal<MobileGuiMgr> OnMobileGuiMgrSent = new Signal<MobileGuiMgr>("MobileGuiMgrSent");
}
