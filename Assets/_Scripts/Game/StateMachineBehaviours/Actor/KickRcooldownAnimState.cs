﻿using UnityEngine;

public class KickRcooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).KickRcooldownInit();
	}
}