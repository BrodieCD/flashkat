﻿using UnityEngine;

public class SuperStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).SuperStartupInit();
	}		
}
