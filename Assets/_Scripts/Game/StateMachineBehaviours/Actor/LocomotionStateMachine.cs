﻿using UnityEngine;

public class LocomotionStateMachine : ActorStateMachineBehaviour 
{
	override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)	
	{
		getActor<Actor>(animator).OnEnterLocomotion();
	} 

	override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
	{
		getActor<Actor>(animator).OnLeaveLocomotion();
	}
}
