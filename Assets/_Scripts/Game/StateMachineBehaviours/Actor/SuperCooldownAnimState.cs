﻿using UnityEngine;

public class SuperCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).SuperCooldownInit();
	}		
}