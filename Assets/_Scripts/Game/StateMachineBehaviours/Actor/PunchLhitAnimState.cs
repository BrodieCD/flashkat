﻿using UnityEngine;

public class PunchLhitAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).PunchLhitInit();
	}	
}
