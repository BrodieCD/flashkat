﻿using UnityEngine;

public class JumpStateMachine : ActorStateMachineBehaviour 
{
	override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)	
	{
		getActor<Player>(animator).OnEnterJump();
	} 

	override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
	{
		getActor<Player>(animator).OnLeaveJump();
	}
}
