﻿using UnityEngine;

public class KickRstartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).KickRstartupInit();
	}
}
