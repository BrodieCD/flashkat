﻿using UnityEngine;

public class PunchLstartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).PunchLstartupInit();
	}		
}
