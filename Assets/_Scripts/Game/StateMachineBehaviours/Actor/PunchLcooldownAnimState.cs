﻿using UnityEngine;

public class PunchLcooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).PunchLCooldownInit();
	}	
}
