﻿using UnityEngine;

public class WalkAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).WalkingInit();
	}	
}
