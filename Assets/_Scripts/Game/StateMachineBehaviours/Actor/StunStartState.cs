﻿using UnityEngine;

public class StunStartState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).StunnedStartInit();
	}	
}
