﻿using UnityEngine;

public class StunStayState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).StunnedStayInit();
	}	
}

