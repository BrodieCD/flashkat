﻿using UnityEngine;

public class ExitLocomotionAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).ExitLocomotionInit();
	}	
}
