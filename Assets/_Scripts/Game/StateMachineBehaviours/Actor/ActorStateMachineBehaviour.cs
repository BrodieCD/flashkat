﻿using UnityEngine;

public class ActorStateMachineBehaviour : StateMachineBehaviour 
{
	protected Actor _actorRef;

//	public void SetActorRef(Actor actorRef)
//	{
//		_actorRef = actorRef;
//	}

	public T getActor<T>(Animator animator) where T : Actor
	{
		if(_actorRef == null)
			_actorRef = animator.GetComponent<T>();	

		return (T)_actorRef;
	}
}
