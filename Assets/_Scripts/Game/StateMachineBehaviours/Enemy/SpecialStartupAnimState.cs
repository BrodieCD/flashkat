﻿using UnityEngine;

public class SpecialStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Enemy>(animator).SpecialStartupInit();
	}		
}