﻿using UnityEngine;

public class SpecialCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Enemy>(animator).SpecialCooldownInit();
	}		
}