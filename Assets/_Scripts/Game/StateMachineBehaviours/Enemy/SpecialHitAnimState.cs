﻿using UnityEngine;

public class SpecialHitAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Enemy>(animator).SpecialHitInit();
	}		
}