﻿using UnityEngine;

public class DragonKickStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).DragonKickStartupInit();
	}	
}