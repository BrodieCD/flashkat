﻿using UnityEngine;

public class FlamingFlashCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).FlamingFlashCooldownInit();
	}
}
