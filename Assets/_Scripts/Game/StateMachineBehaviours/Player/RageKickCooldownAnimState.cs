﻿using UnityEngine;

public class RageKickCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).RageKickCooldownInit();
	}	
}