﻿using UnityEngine;

public class SpotDodgeMiddleAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).SpotDodgeMiddleInit();
	}	
}