﻿using UnityEngine;

public class SlidePunchStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).SlidePunchStartupInit();
	}	
}