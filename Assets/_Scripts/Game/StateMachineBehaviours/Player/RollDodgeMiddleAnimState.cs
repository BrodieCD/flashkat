﻿using UnityEngine;

public class RollDodgeMiddleAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).RollDodgeMiddleInit();
	}	
}