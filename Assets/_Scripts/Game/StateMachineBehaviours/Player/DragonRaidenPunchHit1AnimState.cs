﻿using UnityEngine;

public class DragonRaidenPunchHit1AnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).DragonRaidenPunchHit1Init();
	}
}
