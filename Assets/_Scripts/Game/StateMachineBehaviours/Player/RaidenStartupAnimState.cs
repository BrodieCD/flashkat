﻿using UnityEngine;

public class RaidenStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).RaidenStartupInit();
	}	
}