﻿using UnityEngine;

public class RollDodgeCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).RollDodgeCooldownInit();
	}	
}