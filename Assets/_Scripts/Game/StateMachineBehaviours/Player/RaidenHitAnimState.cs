﻿using UnityEngine;

public class RaidenHitAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).RaidenHitInit();
	}	
}