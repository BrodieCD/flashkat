﻿using UnityEngine;

public class TornadoKickHitAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).TornadoKickHitInit();
	}	
}