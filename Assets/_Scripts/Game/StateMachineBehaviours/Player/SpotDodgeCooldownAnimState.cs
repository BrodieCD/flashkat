﻿using UnityEngine;

public class SpotDodgeCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).SpotDodgeCooldownInit();
	}	
}
