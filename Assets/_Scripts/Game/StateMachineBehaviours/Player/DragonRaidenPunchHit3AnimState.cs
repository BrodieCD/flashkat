﻿using UnityEngine;

public class DragonRaidenPunchHit3AnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).DragonRaidenPunchHit3Init();
	}
}