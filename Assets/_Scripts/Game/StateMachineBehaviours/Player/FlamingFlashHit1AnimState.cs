﻿using UnityEngine;

public class FlamingFlashHit1AnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).FlamingFlashHit1Init();
	}
}
