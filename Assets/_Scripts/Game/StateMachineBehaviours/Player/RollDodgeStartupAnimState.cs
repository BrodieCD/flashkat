﻿using UnityEngine;

public class RollDodgeStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).RollDodgeStartupInit();
	}	
}
