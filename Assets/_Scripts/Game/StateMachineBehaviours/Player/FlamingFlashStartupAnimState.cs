﻿using UnityEngine;

public class FlamingFlashStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).FlamingFlashStartupInit();
	}
}
