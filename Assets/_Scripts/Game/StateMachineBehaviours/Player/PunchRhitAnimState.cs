﻿using UnityEngine;

public class PunchRhitAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).PunchRhitInit();
	}	
}
