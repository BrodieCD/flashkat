﻿using UnityEngine;

public class ElectricShootCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).ElectricShootCooldownInit();
	}
}
