﻿using UnityEngine;

public class SlidePunchCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).SlidePunchCooldownInit();
	}	
}
