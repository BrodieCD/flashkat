﻿using UnityEngine;

public class RageKickStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).RageKickStartupInit();
	}	
}