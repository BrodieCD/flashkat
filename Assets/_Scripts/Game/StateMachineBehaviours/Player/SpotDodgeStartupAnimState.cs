﻿using UnityEngine;

public class SpotDodgeStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Actor>(animator).SpotDodgeStartupInit();
	}	
}