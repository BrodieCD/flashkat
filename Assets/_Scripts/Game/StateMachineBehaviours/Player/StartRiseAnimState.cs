﻿using UnityEngine;

public class StartRiseAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).StartRiseInit();
	}	
}