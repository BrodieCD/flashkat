﻿using UnityEngine;

public class DragonRaidenPunchHit2AnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).DragonRaidenPunchHit2Init();
	}
}