﻿using UnityEngine;

public class TornadoKickStartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).TornadoKickStartupInit();
	}	
}