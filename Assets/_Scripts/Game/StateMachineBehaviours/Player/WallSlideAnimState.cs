﻿using UnityEngine;

public class WallSlideAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).WallSlidingInit();
	}	
}