﻿using UnityEngine;

public class FlamingFlashHit3AnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).FlamingFlashHit3Init();
	}
}
