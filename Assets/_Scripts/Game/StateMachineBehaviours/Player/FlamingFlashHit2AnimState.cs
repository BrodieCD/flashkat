﻿using UnityEngine;

public class FlamingFlashHit2AnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).FlamingFlashHit2Init();
	}
}

