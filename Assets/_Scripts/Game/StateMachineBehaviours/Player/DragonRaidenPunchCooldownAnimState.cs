﻿using UnityEngine;

public class DragonRaidenPunchCooldownAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).DragonRaidenPunchCooldownInit();
	}
}
