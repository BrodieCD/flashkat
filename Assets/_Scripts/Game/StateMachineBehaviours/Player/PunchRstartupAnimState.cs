﻿using UnityEngine;

public class PunchRstartupAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).PunchRstartupInit();
	}		
}
