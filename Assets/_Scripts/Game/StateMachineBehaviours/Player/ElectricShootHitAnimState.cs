﻿using UnityEngine;

public class ElectricShootHitAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).ElectricShootHitInit();
	}
}