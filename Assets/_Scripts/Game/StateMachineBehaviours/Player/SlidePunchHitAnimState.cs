﻿using UnityEngine;

public class SlidePunchHitAnimState : ActorStateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		getActor<Player>(animator).SlidePunchHitInit();
	}	
}