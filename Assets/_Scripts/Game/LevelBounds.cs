﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class LevelBounds : MonoBehaviour 
{
	static private LevelBounds _instance;
	private SpriteRenderer _spriteRenderer;

	void Awake()
	{
		_instance = this;
		_spriteRenderer = GetComponent<SpriteRenderer>();
	}

	static public void GetCameraLimits(Camera cam, out Vector2 topLeftLimit, out Vector2 topRightLimit)
	{
		float camExtentV = cam.orthographicSize;  
     	float camExtentH = (camExtentV * Screen.width) / Screen.height;

		Bounds levelBounds = _instance._spriteRenderer.bounds;

		float leftBound = levelBounds.min.x + camExtentH;
	    float rightBound = levelBounds.max.x - camExtentH;
		float bottomBound = levelBounds.min.y + camExtentV;
	    float topBound = levelBounds.max.y - camExtentV;	

		topLeftLimit = new Vector2(leftBound, topBound);
		topRightLimit = new Vector2(rightBound, bottomBound);
	}
	
	static public void GetSpriteLimits(SpriteRenderer targetSpriteRenderer, float limitExtentFactor, out float _leftLimit, out float _rightLimit)
	{
		Bounds levelBounds = _instance._spriteRenderer.bounds;
		float userExtentsX = targetSpriteRenderer.bounds.extents.x * limitExtentFactor;

		_leftLimit = levelBounds.min.x + userExtentsX;
	    _rightLimit = levelBounds.max.x - userExtentsX;		
	}
}
