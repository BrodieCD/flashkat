﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnityUiUtils
{
	static public void SetRectTransformPosFromScreenPos(RectTransform rectTransform, Vector2 screenPos)
	{
		Vector3 worldPos;
		RectTransformUtility.ScreenPointToWorldPointInRectangle(rectTransform, screenPos, null, out worldPos);
		rectTransform.position = worldPos;	
	}
	
	static public Rect GetScreenRectFromRectTransform(RectTransform rectTransform)
	{
		Vector3[] corners = new Vector3[4];
		rectTransform.GetWorldCorners(corners);
		
		Vector2 bottomLeft = RectTransformUtility.WorldToScreenPoint(null, corners[0]);
		Vector2 topRight = RectTransformUtility.WorldToScreenPoint(null, corners[2]);
		
		return new Rect(bottomLeft.x, bottomLeft.y, topRight.x - bottomLeft.x, topRight.y - bottomLeft.y);
	}
}
