using System;
using System.Collections.Generic;
using UnityEngine;

public class GraphicsUtils
{
	static public Texture2D ExtractSubTexture(Texture2D source, int x, int y, int width, int height)
	{
		Color[] colorPixels = source.GetPixels(x, y, width, height);
		Texture2D subTexture = new Texture2D(width, height);
		subTexture.SetPixels(colorPixels);
		subTexture.Apply();	
		
		return subTexture;
	}
	
	static public List<Texture2D> ExtractTiledSubTextures(Texture2D source, int basePositionX, int basePositionY, int tileWidth, int tileHeight,
	                                                      int tileSeparationWidth, int tileSeparationHeight, int columnsAmount, int rowsAmount)
	{
		List<Texture2D > tiledSubTextures = new List<Texture2D>();
		
		for(int i = 0; i < rowsAmount; i ++)
		{
			int rowPositionY = basePositionY + i * (tileSeparationHeight + tileHeight);
			
			for(int j = 0; j < columnsAmount; j++)
			{
				int columnPositionX = basePositionX + j * (tileSeparationWidth + tileWidth);
				
				tiledSubTextures.Add(ExtractSubTexture(source, columnPositionX, rowPositionY, tileWidth, tileHeight)); 
			}
		}
		
		return tiledSubTextures;
	}
}


