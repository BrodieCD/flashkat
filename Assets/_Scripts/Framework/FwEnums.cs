
public enum GuiAlignmentOption
{
	TopLeft,
	TopRight,
	BottomLeft,
	BottomRight,
	CenterHorizontalTop,
	CenterHorizontalBottom,
	CenterVerticalLeft,
	CenterVerticalRight,
}

public enum MouseButton
{
	Left = 0,
	Right,
	Middle
}

public enum ButtonStates
{
	Idle,
	Down,
	Pressed,
	Up
}

public enum BulletBehaviorOption
{
	Straight,
	StraightRotateTowards,
	StraightRotatingZ,
	Sin
}	

public enum LoadingFadeScreenState
{
	In,
	Out,
	FadingOut,
	FadingIn
}
