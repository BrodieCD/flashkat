using System;
using UnityEngine;
using System.Collections.Generic;

public class GeneralUtils
{
	static public void DestroyGameObjectChildren(GameObject parent)
	{		
		foreach(Transform childrenTransform in parent.transform)
		{
			GameObject.Destroy(childrenTransform.gameObject);
		}
	}
	
	static public void SafeDestroy(GameObject gameObject)
	{
		if(gameObject != null)
		{
			GameObject.Destroy(gameObject);
		}
	}
	
    static public void GetLargestListDictionary<KeyType, ValueType>(Dictionary<KeyType, List<ValueType>> listDictionary, ref KeyType largestKey, out int largestCount)
	{
		largestCount = 0;
		
		foreach(KeyValuePair<KeyType, List<ValueType>> pair in listDictionary)
		{
			if(pair.Value.Count > largestCount)
			{
				largestCount = pair.Value.Count;
				largestKey = pair.Key;
			}
		}		
	}	
	
	static public List<T> SubstractList<T>(List<T> sourceList, List<T> substractionList)
	{
		List<T> resultList = new List<T>();
		
		foreach(T element in sourceList)
		{
			if(!substractionList.Contains(element))
			{
				resultList.Add(element);
			}
		}
		
		return resultList;
	}
	
	static public void AbideLastListItemsOnly<T>(List<T> list, int range)
	{
        if(list.Count > range)
		{
			int excess = list.Count - range;	
			list.RemoveRange(0, excess);        
		}	
	}

	static public void FillListWithEnumValues<T>(ref List<T> list) 
	{
		#if UNITY_EDITOR
	    if (!typeof(T).IsEnum) 
	    	throw new ArgumentException("T must be an enumerated type");
		#endif

		list = new List<T>();
		foreach(T value in Enum.GetValues(typeof(T)))
			list.Add(value);
	}  

	static public T GetRandomItemFromList<T>(List<T> list)
    {
		int listCount = list.Count;
		
		#if UNITY_EDITOR
		if(listCount == 0)
		{
			Debug.LogError(list.ToString() + " is empty. It is not possible to get a random item from it.");
			return default(T);
		}
		else
		#endif
		{
			if(listCount == 1)
				return list[0];
			else
				return list[UnityEngine.Random.Range(0, listCount)];
		}	
    }	

	static public T GetRandomItemFromListByProbability<T>(List<T> itemList, List<int> percentagesList)
    {
		int itemListCount = itemList.Count;
		
		#if UNITY_EDITOR
		if(itemListCount == 0)
		{
			Debug.LogError(itemList.ToString() + " is empty. It is not possible to get a random item from it.");
			return default(T);
		}
		else if(itemListCount != percentagesList.Count)
		{
			Debug.LogError(itemList.ToString() + " and " + percentagesList.ToString() + " need to be of the same size.");
			return default(T);
		}
		else if(itemListCount == 1)
			return itemList[0];
		else
		#endif
		{	
			int randomNumber = UnityEngine.Random.Range(1, 101); //Get a random int from 1 to 100
			int percentagesSum = 0;
			int index = -1; 
			for(int i = 0; i < itemListCount; i++)
			{
				percentagesSum = percentagesList[i];
				if(randomNumber <= percentagesSum)
				{
					index = i;
					break;
				}
			}

			#region UNITY_EDITOR
			if(percentagesSum != 100)
				Debug.LogError("Percentages should total 100. Method won't work as expected.");
			#endregion

			return itemList[index];
		}
    }	
	
	static public List<T> GetRandomItemsFromList<T>(List<T> list, int elementsAmount)
	{
		List<T> tempList = new List<T>(list);
		List<T> randomList = new List<T>();
		
		for(int i = 0; i < elementsAmount; i++)
		{
			int randomIndex = UnityEngine.Random.Range(0, tempList.Count);
			randomList.Add(tempList[randomIndex]);
			tempList.RemoveAt(randomIndex);
		}
		
		return randomList;
	}
	
	static public List<int> GetRandomInts(int minValue, int maxValue, int intsAmount)
	{
		List<int> randomInts = new List<int>();
		int randomInt = 0;
		
		for(int i = 0; i < intsAmount; i++)
		{
			do
			{
				randomInt = UnityEngine.Random.Range(minValue,maxValue);				
			}
			while(randomInts.Contains(randomInt));
				
			randomInts.Add(randomInt);
		}
		
		return randomInts;
	}
	
	static public T ExtractRandomItemFromList<T>(List<T> list)
	{
		T itemExtracted = GetRandomItemFromList<T>(list);
		
		list.Remove(itemExtracted);
		
		return itemExtracted;
	}
	
	static public bool Check2dCollision(float lx1, float rx1, float dy1, float uy1, float lx2, float rx2, float dy2, float uy2)
	{
		bool isColliding = false;
		
    	if ((lx1 < rx2) && (lx2 < rx1) && (dy1 > uy2) && (dy2 > uy1))
	    {	
			isColliding = true;
		}
		
		return isColliding;
	}	
	
	static public bool Check1dCollision(float a1, float b1, float a2, float b2)
	{
		return((a1 < b2) && (a2 < b1));
	}
	
	static public bool GetActionProbability(float probability)
	{
		return (UnityEngine.Random.Range(0.0f, 1.0f) <= probability);
	}

	static public bool GetActionProbability(int percentage)
	{
		return (UnityEngine.Random.Range(1, 101) <= percentage);
	}
	
//	public static void DeductTimeCount(ref float timeCount, SimpleCallback onCompleted)
//	{
//		if(timeCount > 0)
//		{
//			timeCount -= Time.deltaTime;
//			
//			if(timeCount <= 0)
//			{
//				onCompleted();
//			}
//		}		
//	}
	
	static public string TranslateTimeToString(float time)
	{
		int min = Mathf.FloorToInt(time/60f);
		int sec = Mathf.FloorToInt(time % 60f);
		int fraction = Mathf.FloorToInt((time * 100) %100);
		
		return  String.Format("{0}:{1}:{2}", min, sec, fraction);		
	}
	
	static public Vector3 GetAimToWorldPoint(Vector3 planeNormal, Vector3 planePoint, Vector3 screenPoint)
	{
		Vector3 targetPoint = new Vector3();
		
	    // Generate a plane that intersects the release spot transform's position with a forward normal.
	    Plane bowPlane = new Plane(planeNormal, planePoint);
	    
	    // Generate a ray from the cursor position
	    Ray ray = Camera.main.ScreenPointToRay(screenPoint);
	    
	    // Determine the point where the cursor ray intersects the plane.
	    // This will be the point that the object must look towards to be looking at the mouse.
	    // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
	    //   then find the point along that ray that meets that distance.  This will be the point
	    //   to look at.
	    float hitdistance = 0.0f;
	    // If the ray is parallel to the plane, Raycast will return false.
	    if(bowPlane.Raycast(ray, out hitdistance)) 
		{
	        // Get the point along the ray that hits the calculated distance.
	        targetPoint = ray.GetPoint(hitdistance);
		}	
		
		return targetPoint;
	}		
}

