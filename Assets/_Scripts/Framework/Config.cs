using UnityEngine;
using System.Collections;

public class Config 	
{
	public const int SCREEN_DEFAULT_WIDTH = 480;
	public const int SCREEN_DEFAULT_HEIGHT = 320;
	
	public static float MusicVolumeRatio = 1.0f;
	public static float SfxVolumeRatio = 1.0f;
	public static float SpeechVolumeRatio = 1.0f;
	
	public static bool EventsDebugMode = false;
	
	public static bool HelpEnabled = true;
	
	public static float ScreenHeightRatio { get { return((float)Screen.height/SCREEN_DEFAULT_HEIGHT); } }	
}
