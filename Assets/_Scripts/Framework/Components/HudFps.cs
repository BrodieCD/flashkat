using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class HudFps : MonoBehaviour 
{
	// Attach this to a Unity UI text to make a frames/second indicator.
	//
	// It calculates frames/second over each updateInterval,
	// so the display does not keep changing wildly.
	//
	// It is also fairly accurate at very low FPS counts (<10).
	// We do this not by simply counting frames per interval, but
	// by accumulating FPS for each frame. This way we end up with
	// correct overall FPS even if the interval renders something like
	// 5.5 frames.
	 
	public  float UpdateInterval = 0.5F;
	public float MaxValueToColorYellow = 30;
	public float MaxValueToColorRed = 10;
	 
	private float _accum   = 0; // FPS accumulated over the interval
	private int   _frames  = 0; // Frames drawn over the interval
	private float _timeleft; // Left time for current interval
	
	private Text _text;

	void Awake()
	{
		_text = GetComponent<Text>();
	}
	 
	void Start()
	{
	    _timeleft = UpdateInterval;  
	}
	 
	void Update()
	{
	    _timeleft -= Time.deltaTime;
	    _accum += Time.timeScale/Time.deltaTime;
	    ++_frames;
	    
	    // Interval ended - update GUI text and start new interval
	    if( _timeleft <= 0.0 )
	    {
	        // display two fractional digits (f2 format)
		    float fps = _accum/_frames;
		    string format = System.String.Format("FPS: {0:F2}",fps);
			_text.text = format;
		
		    if(fps < MaxValueToColorYellow)
		        _text.color = Color.yellow;
		    else 
		        if(fps < MaxValueToColorRed)
		            _text.color = Color.red;
		        else
		            _text.color = Color.green;
		       //DebugConsole.Log(format,level);
		        _timeleft = UpdateInterval;
		        _accum = 0.0F;
		        _frames = 0;
	    }
	}
}
