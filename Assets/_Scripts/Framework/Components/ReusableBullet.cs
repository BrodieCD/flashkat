using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ActorHitBox2D))]
public class ReusableBullet : MonoBehaviour 
{
	[HideInInspector]
	public Vector3 Direction = Vector3.right;
	
	public float Acceleration = 0;
	public float Speed = 2;
	public float RotationSpeed = 0;
	
	public float LifeTime = 4;
	public float AvailabilityDelay = 2;
	
	public BulletBehaviorOption Behavior;
	
	private float _timeCountSinceActivation = 0;	
	
	//private EnemyHitBox2D _weaponTrigger;	
	private SimpleCallback _bulletBehaviourFunction;	
	
	private float _defaultAcceleration;
	private float _defaultSpeed;
	private float _defaultRotationSpeed;
	
	private float _defaultLifeTime;
	private float _defaultDeactivationDelay;
	
	
	private bool _lifeTimeIsUp
	{
		get
		{
			return (_timeCountSinceActivation >= LifeTime);
		}
	}
	
	public bool AvailableDelayIsUp
	{
		get
		{
			return (_timeCountSinceActivation >= (AvailabilityDelay +  LifeTime));
 		}
	}
	
	public bool Available
	{
		get;
		protected set;
	}

	void Start()
	{
		Available = true;
	    getDefaultValues();
		//_weaponTrigger = GetComponent<EnemyHitBox2D>();
		_bulletBehaviourFunction = getBehaviorFunction(Behavior);			
		Deactivate();
	}	
	
	void Update()
	{
		_bulletBehaviourFunction();
		handleDeactivation();	
	}
	
	public void Activate(Vector3 position, Vector3 direction)
	{
		if(!gameObject.activeSelf)
		{	
			Available = false;
			
			transform.position = position;		
			Direction = direction;	
			
			setDefaultValues();
			
		    gameObject.SetActive(true);	
			_timeCountSinceActivation = 0;
			
//			if(_weaponTrigger != null)
//			{
//				_weaponTrigger.RequiresDestroy = false;
//			}
			
			if(Behavior == BulletBehaviorOption.StraightRotateTowards)
			{
				Quaternion targetRot = Quaternion.LookRotation(direction, Vector3.up);
				transform.rotation = targetRot;
			}				
		}
		else
		{
			Debug.LogError("Bullet::Active -> Bullet is already active");
		}	
	}
		
	public void Deactivate()
	{
		if(gameObject.activeSelf)
		{
			gameObject.SetActive(false);
		}
		else
		{
			Debug.LogError("ReusableBullet::Active -> Bullet is already deactive");
		}
	}
	
	public void UpdateTimeCountSinceActivation()
	{
		_timeCountSinceActivation += Time.deltaTime;	
		
		if(AvailableDelayIsUp)
		{
			Available = true;
		}
	}
	
	private SimpleCallback getBehaviorFunction(BulletBehaviorOption behavior)
	{
	    SimpleCallback bulletBehaviorFunction = null;
		switch(behavior)
		{
			case BulletBehaviorOption.Straight:
			case BulletBehaviorOption.StraightRotateTowards:		
				bulletBehaviorFunction = _MoveStraight;
				break;
			case BulletBehaviorOption.StraightRotatingZ:
				bulletBehaviorFunction = _MoveStraightRotatingZ;
				break;
				default:
				bulletBehaviorFunction = _MoveStraight;
				break;
		}		
		
		return bulletBehaviorFunction;
	}
	
	private void getDefaultValues()
	{
		_defaultAcceleration = Acceleration;
		_defaultSpeed = Speed;
	 	_defaultRotationSpeed = RotationSpeed;
		_defaultLifeTime = LifeTime;
		_defaultDeactivationDelay = AvailabilityDelay;		
	}
	
	private void setDefaultValues()
	{
		Acceleration = _defaultAcceleration;
		Speed = _defaultSpeed;
	 	RotationSpeed = _defaultRotationSpeed;
		LifeTime = _defaultLifeTime;
		AvailabilityDelay = _defaultDeactivationDelay;			
	}
	
	private void handleDeactivation()
	{		
//		if(_weaponTrigger.RequiresDestroy || _lifeTimeIsUp)
//		{
//			Deactivate();
//
//			if(_lifeTimeIsUp)
//			{
//				_weaponTrigger.Explode();
//			}
//		}
	}

	
	#region Movement behaviours
	private void _MoveStraight()
	{
		float speedChange = Acceleration * Time.deltaTime;
		Speed += speedChange;
		
		float positionChange = Speed * Time.deltaTime;
		
		Vector3 translation = Direction * positionChange;
		
		transform.position += translation;			
	}
	
	private void _MoveStraightRotatingZ()
	{
		_MoveStraight();
		transform.Rotate(Vector3.forward, RotationSpeed * Time.deltaTime, Space.World);
	}		
	#endregion
}
