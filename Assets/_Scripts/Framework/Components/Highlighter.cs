using UnityEngine;
using System.Collections;

public class Highlighter : MonoBehaviour 
{
	public float HurtLightFrecuencyFactor = 2;
	public float HurtLightDuration = 3;	
	
	public float HealLightFrecuencyFactor = 2;
	public float HealLightDuration = 3;		
	
	[HideInInspector]
	public bool HasHurtLight = false;
	
	[HideInInspector]
	public bool HasHealLight = false;
	
	public GameObject Target;	
	
	private float _hurtLightTimeCount; 		
	private float _healLightTimeCount;
	
	void Update() 
	{
		if(HasHurtLight)
		{
			float otherColorsLevel = Mathf.Abs(Mathf.Sin(Time.timeSinceLevelLoad * HurtLightFrecuencyFactor));			
			changeMaterialsColors(new Color(1, otherColorsLevel, otherColorsLevel));
			
			//print("otherColorsLevel: " + otherColorsLevel);
			
			_hurtLightTimeCount -= Time.deltaTime;
			
			if(_hurtLightTimeCount <= 0)
			{
				HurtLightEnable(false);
			}
		}
		else if(HasHealLight)
		{
			float otherColorsLevel = Mathf.Abs(Mathf.Sin(Time.timeSinceLevelLoad * HealLightFrecuencyFactor));			
			changeMaterialsColors(new Color(otherColorsLevel, otherColorsLevel, 1));
			
			_healLightTimeCount -= Time.deltaTime;
			
			if(_healLightTimeCount <= 0)
			{
				HurtLightEnable(false);
			}			
		}
	}
	
	public void HurtLightEnable(bool enabled)
	{
		if(enabled)
		{
			if(!HasHurtLight)
			{
				HasHurtLight = true;
				_hurtLightTimeCount = HurtLightDuration;
			}			
		}
		else
		{
			if(HasHurtLight)
			{
				changeMaterialsColors(new Color(1, 1, 1, 1));
				
				HasHurtLight = false;
			}
		}
	}
	
	public void HealLightEnable(bool enabled)
	{
		if(enabled)
		{
			if(!HasHealLight)
			{
				HasHealLight = true;
				_healLightTimeCount = HealLightDuration;
			}			
		}
		else
		{
			if(HasHealLight)
			{
				changeMaterialsColors(new Color(1, 1, 1, 1));
				
				HasHealLight = false;
			}
		}
	}	
	
	private void changeMaterialsColors(Color color)
	{
		foreach(Renderer targetRenderer in Target.GetComponentsInChildren<Renderer>())
		{
			foreach(Material targetMaterial in targetRenderer.materials)
			{		
				if(targetMaterial.HasProperty("_Color"))
				{
					targetMaterial.color = color;
				}
			}
		}		
	}
}
