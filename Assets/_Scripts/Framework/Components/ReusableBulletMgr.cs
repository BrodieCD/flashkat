using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class ReusableBulletMgr : MonoBehaviour
{	
	public String BulletGameObjectContainerName = "BulletsContainer";
	
	public GameObject[] BulletGameObjects;
	public int[] InstancesAmount;
	public BulletNames[] AllBulletNames;
	
	[HideInInspector]
	public Dictionary<BulletNames, BulletGroup> _bulletGroups; 
	
	void Awake()
	{
		GameEvents.Instance.OnReusableBulletShot.Connect(this, Shoot);
	}
	
	void Start()
	{
		if(BulletGameObjects.Length != InstancesAmount.Length)
		{
			Debug.LogError("Sfx game objects and instances amount should match!");
		}
		
		GameObject bulletContainerGO = new GameObject(BulletGameObjectContainerName);
		
		_bulletGroups = new Dictionary<BulletNames, BulletGroup>();
		
		int bulletsAmount = BulletGameObjects.Length;
		
		for(int i = 0; i < bulletsAmount; i++)
		{
			BulletGroup newGroup = new BulletGroup(BulletGameObjects[i], InstancesAmount[i], bulletContainerGO.transform);
			_bulletGroups.Add(AllBulletNames[i], newGroup); 
		}
	}
	
	void Update()
	{
		foreach(KeyValuePair<BulletNames, BulletGroup> kvp in _bulletGroups)
		{
			kvp.Value.Update();
		}
	}
	
	void OnDestroy()
	{
		GameEvents.Instance.OnReusableBulletShot.Disconnect(this);
	}
		
	public void Shoot(object dispatcher, Vector3 position, Vector3 direction, BulletNames bulletName, GetReusableBulletCallback getBulletCallback)
	{
		if(_bulletGroups.ContainsKey(bulletName))
		{
			ReusableBullet bulletShot = _bulletGroups[bulletName].Activate(position, direction);
						
			if((getBulletCallback != null) && (bulletShot != null))
			{
				getBulletCallback(bulletShot);
			}
		}
		else
		{
			print("Bullet name : " + bulletName.ToString() + " was not found. Ignoring shoot attempts.");
		}
	}
		
	public class BulletGroup
	{
		private List<ReusableBullet> _bullets;
		private String prefabName = "";
		
		public BulletGroup(GameObject prefab, int instancesAmount, Transform container)
		{
			prefabName = prefab.name;
			_bullets = new List<ReusableBullet>();
			
			for(int i = 0; i < instancesAmount; i++)
			{
				GameObject newInstanceGO = (GameObject)GameObject.Instantiate(prefab);
				newInstanceGO.transform.parent = container;
				_bullets.Add(newInstanceGO.GetComponent<ReusableBullet>());
			}
		}
		
		public ReusableBullet Activate(Vector3 position, Vector3 direction)
		{
			ReusableBullet bulletToActivate = null;
			
			foreach(ReusableBullet bullet in _bullets)
			{
				if(bullet.Available)
				{
					bullet.Activate(position, direction);
					bulletToActivate = bullet;
					break;
				}
			}
			
			if(bulletToActivate != null)
			{
				_bullets.Remove(bulletToActivate);
				_bullets.Add(bulletToActivate);  //We remove and add so the activated item becomes last and it is not reused so quickly, creating trail renderer artifacts
			}
			else
			{
				print("A deactive instance of " + prefabName + " was not found. Skipping shot."); 
			}	
				
			return bulletToActivate;
		}
		
		public void Update()
		{
			foreach(ReusableBullet bullet in _bullets)
			{
				if(bullet.gameObject.activeSelf || !bullet.Available)
				{
					bullet.UpdateTimeCountSinceActivation();
				}
			}
		}
	}	
}	
