using UnityEngine;

public class InstantQuit : MonoBehaviour 
{
	void Update () 
	{
		if(Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
}
