using UnityEngine;

public class HideRecursivelyOnStart : MonoBehaviour
{
	void Start () 
	{
		SetRendererEnabledRecursively(gameObject, false);
	}
	
	private void SetRendererEnabledRecursively(GameObject target, bool enable)
	{	
		if(target.GetComponent<Renderer>() != null)
		{
			target.GetComponent<Renderer>().enabled = enable;
		}
		
		if(transform.childCount > 0)
		{
			foreach(Transform childTransform in target.transform)
			{
				SetRendererEnabledRecursively(childTransform.gameObject, enable);
			}
		}
	}
}
