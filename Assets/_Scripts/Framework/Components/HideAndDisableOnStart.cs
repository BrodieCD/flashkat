using UnityEngine;
using System.Collections;

public class HideAndDisableOnStart : MonoBehaviour 
{
	void Start () 
	{
		GetComponent<Renderer>().enabled = false;
		this.enabled = false;
	}
}
