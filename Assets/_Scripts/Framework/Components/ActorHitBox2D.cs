using UnityEngine;
using System.Collections;

public class ActorHitBox2D : MonoBehaviour 
{		
	public Actor User;

    void OnTriggerEnter2D(Collider2D other)
    {
        //print("OnTriggerEnter2D: " + other.name);

        if (other.GetComponent<Crate>() != null)
        {
            other.GetComponent<Crate>().GetHit(User.LastAttack);
        } else {

            Actor victim = other.GetComponent<ActorHurtBox2D>().ActorRef;
			Attack lastAttack = User.LastAttack;

            if (victim.IsPlayer)
            {
                PowerupController.PowerupData data = victim.GetComponent<PowerupController>().powerupData;
                if (data.UsingStrenghtPowerup)
                {
                    if (victim.GetHit(lastAttack, User.Position))
                    {
                        User.HitConfirmed = true;

                        DisplayFX(lastAttack, lastAttack.Damage * data.DamageReductionMultiplyer);
                    }
                }else
                {
                    if (victim.GetHit(lastAttack, User.Position))
                    {
                        User.HitConfirmed = true;
                        DisplayFX(lastAttack, lastAttack.Damage);
                    }
                }
            }

            if (User.IsPlayer)
            {
                PowerupController.PowerupData data = User.GetComponent<PowerupController>().powerupData;
                if (data.UsingStrenghtPowerup)
                {
                    if (victim.GetHit(lastAttack, User.Position, data.DamageMultiplyer))
                    {
                        User.HitConfirmed = true;

                        if (!User.IsPlayer)
                            User.GetComponent<Enemy>().HasHitPlayer = true;

                        DisplayFX(lastAttack, lastAttack.Damage * data.DamageMultiplyer);
                    }
                } else {
                    if (victim.GetHit(lastAttack, User.Position))
                    {
                        User.HitConfirmed = true;

                        if (!User.IsPlayer)
                            User.GetComponent<Enemy>().HasHitPlayer = true;

                        DisplayFX(lastAttack, lastAttack.Damage);
                    }
                }
            } else {

                if (victim.GetHit(lastAttack, User.Position))
                {
                    User.HitConfirmed = true;

                    if (!User.IsPlayer)
                        User.GetComponent<Enemy>().HasHitPlayer = true;

                    DisplayFX(lastAttack, lastAttack.Damage);
                }
            }
        }
	}

    void DisplayFX(Attack _attack, float damage)
    {
        if (_attack.ImpactSoundFx != FxAndSounds.SoundsNames.None)
            SoundManager.PlayOneShotSound(this, _attack.ImpactSoundFx, _attack.ImpactSoundVol);
        else
            SoundManager.PlayRandomOneShotSoundFromList(this, User.HitSounds);

        FxManager.EnableFx(this, _attack.HitFx, transform.position, Quaternion.identity);
        GameObject movingTextGO = MessageManager.FireMessage(MessageData.Names.DamageUpFade, transform.position);
        movingTextGO.GetComponent<MovingText>().TextDisplay.text = damage.ToString();
    }
}