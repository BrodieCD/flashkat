using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class InputUser : MonoBehaviour 
{
	public enum GamePads
	{ 
		None,
		P1,
		P2,
		P3,
		P4,
	}
	
	public enum ControllerTypes
	{ 
		None,
		KeybMouse,
		Gamepad,
		Touchpad, 
	}
	
	public enum GamePadButtons
	{ 
		X,
		Y,
		A,
		B,
		LB,
		LS,
		RB,
		RS,
		BACK,
		START, 
		LTD,
		RTD
	}
	
	public enum GamePadAxes
	{
		LH,
		LV,  
		RH,
		RV,
		DH,
		DV,
		TA
	}

	public enum AnalogTriggers
	{
		Left,
		Right
	}

	public const String L_CLICK_KM = "Lclick_KM";
	public const String R_CLICK_KM = "Rclick_KM";
	public const String M_CLICK_KM = "Mclick_KM";
	public const String SPACE_KM = "Space_KM";  
	public const String E_KM = "E_KM";
	public const String F_KM = "F_KM";
	public const String J_KM = "J_KM";
	public const String K_KM = "K_KM";
	public const String L_KM = "L_KM";
	public const String U_KM = "U_KM";
	public const String ENTER_KM = "Enter_KM";
	public const String ESC_KM = "Esc_KM";
	public const String SW_KM = "SW_KM";
	public const String AD = "AD_KM";
	public const String SHIFT = "Shift_KM";
	public const String CTRL = "Ctrl_KM";
	public const String WHEEL_KM = "Wheel_KM";

	public float DeadInputRange = 0.3f;
	public float SlowMoveInputRange = 0.75f;

	public float SlowMoveVelocityRatio = 0.5f;

	//public float AxisAsButtonTolerance = 0.5f;	

	public ControllerTypes ControllerType = ControllerTypes.None;
	public GamePads GamePadNum = GamePads.P1;
	
	private MobileGuiMgr _mobileGuiMgrRef;
	private Dictionary<AnalogTriggers,bool> _analogTriggersPressed;

	private bool _movingDownLastFrame = false;

	void Awake()
	{
		GameEvents.Instance.OnMobileGuiMgrSent.Connect(this, HandleMobileGuiMgrSent);
	}

	void Start()
	{
//		_analogTriggersPressed = new Dictionary<AnalogTriggers, bool>(1);
//
//		_analogTriggersPressed.Add(AnalogTriggers.Left, false);
//		_analogTriggersPressed.Add(AnalogTriggers.Right, false);

		if(Application.platform == RuntimePlatform.Android)
			ControllerType = InputUser.ControllerTypes.Touchpad;
		
		if(Application.platform == RuntimePlatform.WindowsPlayer)  //TODO: Remove
			ControllerType = InputUser.ControllerTypes.Gamepad;
	}

	void OnDestroy()
	{
		GameEvents.Instance.OnMobileGuiMgrSent.Disconnect(this);
	}

	#region Signal event handlers
	public void HandleMobileGuiMgrSent (object dispatcher, MobileGuiMgr mobileGuiMgrRef)
	{
		_mobileGuiMgrRef = mobileGuiMgrRef;
	}
	#endregion

	public void SetControllerTypeAndNum(ControllerTypes type, GamePads num)
	{
		//FwSignalEvents.Instance.OnPrintToConsole.EmitParams(this, "SetControllerTypeAndNum: " + type.ToString() + " " + num.ToString(), false);
		
		GamePadNum = num;
		ControllerType = type;
		//PlayerCam.SetControllerTypeAndNum(type, num);
	}

	public bool IsKeybMouse { get { return (ControllerType == ControllerTypes.KeybMouse); } }
	public bool IsGamepad { get { return (ControllerType == ControllerTypes.Gamepad); }}
	public bool IsTouchPad { get { return (ControllerType == ControllerTypes.Touchpad); }}
	
	public bool GetPunchButtonDown()
	{
		return getButtonDown(MobileButton.Names.Punch, J_KM, GamePadButtons.X);
	}

	public bool GetKickButtonDown()
	{
		return getButtonDown(MobileButton.Names.Kick, K_KM, GamePadButtons.Y);
	}

	public bool GetJumpButtonDown()
	{
		return getButtonDown(MobileButton.Names.Jump, SPACE_KM, GamePadButtons.A);
	}

	public bool GetJumpButton()
	{
		return GetButton(MobileButton.Names.Jump, SPACE_KM, GamePadButtons.A);
	}

	public bool GetJumpButtonUp()
	{
		return GetButtonUp(MobileButton.Names.Jump, SPACE_KM, GamePadButtons.A);
	}

	public bool GetDefenseButtonDown()
	{
		return getButtonDown(MobileButton.Names.Defense, L_KM, GamePadButtons.B);
	}

	public bool GetSpecialButtonDown()
	{
		return getButtonDown(MobileButton.Names.Special, U_KM, GamePadButtons.RB);
	}

	public bool GetSpecialButton()
	{
		return GetButton(MobileButton.Names.Special, U_KM, GamePadButtons.RB);
	}

	public bool GetSpecialButtonUp()
	{
		return GetButtonUp(MobileButton.Names.Special, U_KM, GamePadButtons.RB);
	}

	public bool GetOptionsButtonDown()
	{
		return getButtonDown(MobileButton.Names.Options, ESC_KM, GamePadButtons.START);
	}

	public bool GetButton(MobileButton.Names mobileButtonName, string keybMouseInputName, GamePadButtons gamepadButton)
	{
		bool isPressed = false;
		
		if(IsTouchPad)
			isPressed = _mobileGuiMgrRef.GetButton(mobileButtonName);
		else if(IsKeybMouse) 
			isPressed = Input.GetButton(keybMouseInputName);
		else if(IsGamepad) 
			isPressed = GetPlayerGamePadButton(gamepadButton);
		
		return isPressed;
	}

	public bool GetButtonUp(MobileButton.Names mobileButtonName, string keybMouseInputName, GamePadButtons gamepadButton)
	{
		bool isUp = false;
		
		if(IsTouchPad)
			isUp = _mobileGuiMgrRef.GetButtonUp(mobileButtonName);
		else if(IsKeybMouse) 
			isUp = Input.GetButtonUp(keybMouseInputName);
		else if(IsGamepad) 
			isUp = GetPlayerGamePadButtonUp(gamepadButton);
		
		return isUp;
	}

	public float GetMoveH()
	{
		return getMove(true, AD, GamePadAxes.LH);
	}

	public float GetMoveV()
	{
		return getMove(false, SW_KM, GamePadAxes.LV);
	}

	public bool GetMoveDownStart()
	{
		bool moveDownStart = false;

		if(GetMoveV() == -1)
		{
			if(!_movingDownLastFrame) 
				moveDownStart = true;

			_movingDownLastFrame = true;
		}
		else
			_movingDownLastFrame = false;
	
		return moveDownStart;
	}

	public bool GetMoveDownHold()
	{
		return (GetMoveV() < 0);
	}

	public bool GetResetButtonDown()
	{
		bool isResetButtonDown = false;
	
		if(IsTouchPad)
			isResetButtonDown = _mobileGuiMgrRef.GetButtonDown(MobileButton.Names.Options);
		else if(IsKeybMouse)
			isResetButtonDown = Input.GetButtonDown(ESC_KM);
		else if(IsGamepad)
			isResetButtonDown = GetPlayerGamePadButtonDown(GamePadButtons.BACK);

		return isResetButtonDown;
	}

//	public bool GetPlayerGamepadAxisAsButtonDown(AnalogTriggers trigger)
//	{
//		bool isDown = false;
//
//		if(!_analogTriggersPressed[trigger]) 
//		{
//			float axisValue = GetPlayerGamePadAxis(GamePadAxes.TA);
//			if(trigger == AnalogTriggers.Right)
//				axisValue = -axisValue;
//
//			//print("Trigger down: " + trigger + " Pressed: " + _analogTriggersPressed[trigger] + " value: " + getPlayerGamePadAxis(GamePadAxes.TA));
//
//			if(axisValue >= AxisAsButtonTolerance)
//			{
//				_analogTriggersPressed[trigger] = true;
//				isDown = true;
//			}
//		}
//
//		return isDown; 	
//	}
//
//	public bool GetPlayerGamepadAxisAsButtonPressed(AnalogTriggers trigger)
//	{
//		return _analogTriggersPressed[trigger]; 	
//	}
//
//	public bool GetPlayerGamepadAxisAsButtonUp(AnalogTriggers trigger)
//	{
//		bool isUp = false;
//	
//		if(_analogTriggersPressed[trigger]) 
//		{
//			float axisValue = GetPlayerGamePadAxis(GamePadAxes.TA);
//			if(trigger == AnalogTriggers.Right)
//				axisValue = -axisValue;
//
//			//print("Trigger up: " + trigger + " Pressed: " + _analogTriggersPressed[trigger] + " value: " + getPlayerGamePadAxis(GamePadAxes.TA));
//
//			if(axisValue < AxisAsButtonTolerance)
//			{
//				_analogTriggersPressed[trigger] = false;
//				isUp = true;
//			}
//		}
//		
//		return isUp; 	
//	}

	public bool GetPlayerGamePadButtonDown(GamePadButtons button)
	{
		return Input.GetButtonDown(button.ToString() + "_" + GamePadNum.ToString());
	}
	
	public bool GetPlayerGamePadButton(GamePadButtons button)
	{
		return Input.GetButton(button.ToString() + "_" + GamePadNum.ToString());
	}
	
	public bool GetPlayerGamePadButtonUp(GamePadButtons button)
	{
		return Input.GetButtonUp(button.ToString() + "_" + GamePadNum.ToString());
	}
	
	public float GetPlayerGamePadAxis(GamePadAxes axis)
	{
		return Input.GetAxis(axis.ToString() + "_" + GamePadNum.ToString());
	}
	
	public float GetPlayerGamePadAxisRaw(GamePadAxes axis)
	{
		return Input.GetAxisRaw(axis.ToString() + "_" + GamePadNum.ToString());
	}	

	public void UpdateSpecialButtonVisibility(bool visible)
	{
		if(_mobileGuiMgrRef != null)
			_mobileGuiMgrRef.SpecialButtonEnable(visible);
	}

	private float getMove(bool isHorizontal, string axisName, GamePadAxes gamepadAxis)
	{
		float m = 0;
	
		if(IsKeybMouse)		
		{
			m = Input.GetAxisRaw(axisName);

//			if(!isH)
//				print("move V input: " + m);

			if(isHorizontal && (Math.Abs(m) > 0))
			{
				if(Input.GetButton(SHIFT))
					m  *= SlowMoveVelocityRatio;  
			}
		}
		else 
		{
			if(IsTouchPad)
				m = isHorizontal? _mobileGuiMgrRef.GetStickAxisH() : _mobileGuiMgrRef.GetStickAxisV();
			else if(IsGamepad)
				m = GetPlayerGamePadAxis(gamepadAxis);

			float mAbs = Math.Abs(m);
			if(mAbs > DeadInputRange)
			{
				m = Mathf.Sign(m);
				if(mAbs <= SlowMoveInputRange)
					m *= SlowMoveVelocityRatio;
			}
			else
				m = 0;

//			if(isH)
//				print("move H input: " + m);
		}
		
		return m;
	}

	private bool getButtonDown(MobileButton.Names mobileButtonName, string keybMouseInputName, GamePadButtons gamepadButton)
	{
		bool isDown = false;
		
		if(IsTouchPad)
			isDown = _mobileGuiMgrRef.GetButtonDown(mobileButtonName);
		else if(IsKeybMouse) 
			isDown = Input.GetButtonDown(keybMouseInputName);
		else if(IsGamepad) 
			isDown = GetPlayerGamePadButtonDown(gamepadButton);
		
		return isDown;
	}
}
