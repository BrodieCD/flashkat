using UnityEngine;

public class DisableRecursivelyOnStart : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
	{
		gameObject.SetActive(false);
	}
}
