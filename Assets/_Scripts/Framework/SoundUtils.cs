using UnityEngine;

public class SoundUtils
{
    public static void UpdatePersistentSoundClip(bool enabled, AudioSource audioSource, float volume)
	{
		if(enabled)
		{
			if(!audioSource.isPlaying)
			{
				audioSource.volume = volume * GameConfig.SfxVolumeRatio;
				audioSource.Play();			
			}
		}
		else
		{
			if(audioSource.isPlaying)
			{
				audioSource.Stop();
			}
		}
	}
	
    public static void UpdatePersistentSoundClipWithBlend(bool enabled, AudioSource audioSourceA, AudioSource audioSourceB, float volume, ref float timeCount, float blendDelayTime)
	{
		if(enabled)
		{
			if(timeCount > 0)
			{
				timeCount -= Time.deltaTime;
				
				if(timeCount <= 0)
				{
					if(!audioSourceA.isPlaying)
					{
						audioSourceA.volume = volume * GameConfig.SfxVolumeRatio;
						audioSourceA.Play();	
						
						//Debug.Log("audioSource A play()");
					}
					else if(!audioSourceB.isPlaying)
					{
						audioSourceB.volume = volume * GameConfig.SfxVolumeRatio;
						audioSourceB.Play();	
						
						//Debug.Log ("audioSource B play()");
					}
					else
					{
						Debug.LogWarning("Both blending sounds are playing at the same time. Please check your blendDelayTime regarding: " + audioSourceA.gameObject.name);
					}
					
					timeCount = blendDelayTime;
				}
			}
		}
		else
		{
			if(audioSourceA.isPlaying)
			{
				audioSourceA.Stop();
			}
			
			if(audioSourceB.isPlaying)
			{
				audioSourceB.Stop();
			}
		}		
	}	
}


