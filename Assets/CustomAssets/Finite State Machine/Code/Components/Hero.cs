﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace FsmExample
{
	/// <summary>
	/// This class is used to control a Hero in the game world. This class uses the Fsm class to show hot it can be used to control something in an structured and controlled way. What we basically do in here is, have our
	/// hero look towards the camera in idle state, rotates back and forth on a waiting state after some time has passed in idle state,  move up and down and activate particles while computing distance increment to move left or right,
	/// spinning while computing physics for aerial movement while jumping, and keep using gravity to change vertical speed and position while falling.  Each state also changes the hero torso color.  
	/// </summary>
	public class Hero : MonoBehaviour 
	{
		public enum States
		{
			Idle,
			Waiting,
			MovingRight,
			MovingLeft,
			Jumping,
			Falling,
		}
	
		public enum Events
		{
			Rest,
			Wait,	
			MoveRight,
			MoveLeft,
			Jump,
			Fall
		}
	
		public float HorizontalSpeed = 5;
		public float MoveRotSpeed = 20;
		public float JumpRotSpeed = 5;
		public float JumpSpeed = 10;
		public float Gravity = 9.8f;
	
		public float SideDistanceLimit = 5;
	
		public float MoveSinAmplitudeFactor = 5;
		public float MoveSinFrequencyFactor = 5;
	
		public float WaitSinAmplitudeFactor = 5;
		public float WaitSinFrequencyFactor = 5;
	
		public float GroundPosY = 0;
	
		public float WaitDelay = 4;
	
		public bool EnableFsmEventsHandledPrint = true;
		public bool EnableStateChangesPrint = true;
		public String FsmDebugName = "Hero";
	
		public Material IdleMaterial;
		public Material WaitingMaterial;
		public Material MovingRightMaterial;
		public Material MovingLeftMaterial;
		public Material JumpingMaterial;
		public Material FallingMaterial;
	
		public Renderer TorsoRenderer;
		public ParticleSystem FeetParticles;
	
		public Text FsmStateText;
	
		private float _verticalSpeed = 0;
		private float _timer = 0;
		private Fsm _fsm;
		private Transform _cachedTransform;
	
	
		void Awake()
		{
			_cachedTransform = transform;
		}
	
		void Start () 
		{
			FeetParticles.Stop();
			initializeFsm();
		}
		
		void Update () 
		{
			_fsm.Update();
		}
	
		/// <summary>
		/// Handles jump input.
		/// </summary>
		/// <returns><c>true</c>, if hero jumped, <c>false</c> otherwise.</returns>
		private bool handleJumpInput()
		{
			bool jumped = false;
	
			if (Input.GetKeyDown (KeyCode.UpArrow))
			{
				jump ();
				jumped = true;
			}
	
			return jumped;
		}
	
		/// <summary>
		/// Handles the air vertical movement. This is, when the character is jumping or falling.
		/// </summary>
		private void handleAerialVerticalMovement()
		{
			float deltaTime = Time.deltaTime;
			float speedDelta = Gravity * deltaTime;
			_verticalSpeed -= speedDelta;	
	
			float verticalMoveDelta = _verticalSpeed * deltaTime;	
			_cachedTransform.Translate(0, verticalMoveDelta, 0);
	
			Vector3 pos = _cachedTransform.position;
			if(pos.y < GroundPosY)
				_cachedTransform.position = new Vector3(pos.x, GroundPosY, pos.z);
		}
	
		/// <summary>
		/// Handles ground movement input.
		/// </summary>
		/// <returns><c>true</c>, if hero moved, <c>false</c> otherwise.</returns>
		private bool handleGroundMovementInput ()
		{
			bool moved = false;
	
			if (Input.GetKey (KeyCode.RightArrow))
			{
				moveRight();
				moved = true;
			}
			else  if (Input.GetKey (KeyCode.LeftArrow))
			{
				moveLeft ();
				moved = true;
			}
	
			return moved;
		}
	
		/// <summary>
		/// Rotates towards given direction.
		/// </summary>
		/// <param name="direction">Direction to slowly rotate to.</param>
		private void rotateTowards(Vector3 direction)
		{
			Quaternion targetRotation = Quaternion.LookRotation(direction);
			_cachedTransform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, MoveRotSpeed * Time.deltaTime);	
		}
	
		/// <summary>
		/// Handles sin movement. This is, up and down movement based in the math Sin operation.
		/// </summary>
		private void handleSinMovement ()
		{
			float moveDeltaY = MoveSinAmplitudeFactor * Mathf.Sin(MoveSinFrequencyFactor * Time.timeSinceLevelLoad);
			_cachedTransform.position = new Vector3 (_cachedTransform.position.x, moveDeltaY, _cachedTransform.position.z);
		}
	
		/// <summary>
		/// Method to be called when fsm state is changed. 
		/// </summary>
		/// <param name="newState">New state.</param>
		private void OnStateChange(object newState)
		{
			FsmStateText.text = newState.ToString();
		}
		
		/// <summary>
		/// Makes fsm handle Rest event.
		/// </summary>
		private void rest()
		{
			_fsm.HandleEvent(Events.Rest);
		}
	
		/// <summary>
		/// Makes fsm handle Wait event.
		/// </summary>
		private void wait()
		{
			_fsm.HandleEvent(Events.Wait);
		}
	
		/// <summary>
		/// Makes fsm handle MoveRight event.
		/// </summary>
		private void moveRight()
		{
			_fsm.HandleEvent(Events.MoveRight);
		}
	
		/// <summary>
		/// Makes fsm handle MoveLeft event.
		/// </summary>
		private void moveLeft()
		{
			_fsm.HandleEvent(Events.MoveLeft);
		}
	
		/// <summary>
		/// Makes fsm handle Jump event.
		/// </summary>
		private void jump()
		{
			_fsm.HandleEvent(Events.Jump);
		}
	
		/// <summary>
		/// Makes fsm handle Fall event.
		/// </summary>
		private void fall()
		{
			_fsm.HandleEvent(Events.Fall);
		}
	
	
		/// <summary>
		/// Creates Fsm instance and sets it up.
		/// </summary>
		private void initializeFsm()
		{	
			_fsm = new Fsm();
	
			_fsm.AddState(States.Idle, idleInit, idleUpdate);
			_fsm.AddState(States.Waiting, waitingInit, waitingUpdate);
			_fsm.AddState(States.MovingLeft, movingLeftInit, movingLeftUpdate, movingLeftLeave);
			_fsm.AddState(States.MovingRight, movingRightInit, movingRightUpdate, movingRightLeave);
			_fsm.AddState(States.Jumping, jumpingInit, jumpingUpdate);
		    _fsm.AddState(States.Falling, fallingInit, fallingUpdate);
		
			foreach(Events e in Enum.GetValues(typeof(Events)))
				_fsm.AddEvent(e);
	
			_fsm.SetRelation(States.Idle, Events.Wait, States.Waiting);
			_fsm.SetRelation(States.Idle, Events.MoveLeft, States.MovingLeft);
			_fsm.SetRelation(States.Idle, Events.MoveRight, States.MovingRight);
			_fsm.SetRelation(States.Idle, Events.Jump, States.Jumping);
	
			_fsm.SetRelation(States.Waiting, Events.MoveLeft, States.MovingLeft);
			_fsm.SetRelation(States.Waiting, Events.MoveRight, States.MovingRight);
			_fsm.SetRelation(States.Waiting, Events.Jump, States.Jumping);
	
			_fsm.SetRelation(States.MovingRight, Events.Rest, States.Idle);
			_fsm.SetRelation(States.MovingRight, Events.Jump, States.Jumping);
			
			_fsm.SetRelation(States.MovingLeft, Events.Rest, States.Idle);
			_fsm.SetRelation(States.MovingLeft, Events.Jump, States.Jumping);
	
			_fsm.SetRelation(States.Jumping, Events.Fall, States.Falling);
		
			_fsm.SetRelation(States.Falling, Events.Rest, States.Idle);
	
			_fsm.StateChangeCallback = OnStateChange;
	
			_fsm.SetDebugLabel(FsmDebugName);
			_fsm.EnableEventsHandledPrint(EnableFsmEventsHandledPrint);
			_fsm.EnableStateChangesPrint(EnableStateChangesPrint);
	
			_fsm.Start(States.Idle);
		}	
	
		/// <summary>
		/// Does stuff at the beginning of the Idle state.
		/// </summary>
		private void idleInit()
		{
			_cachedTransform.position = new Vector3 (_cachedTransform.position.x, GroundPosY, _cachedTransform.position.z);
			_timer = WaitDelay;
			TorsoRenderer.material = IdleMaterial;
		}
	
		/// <summary>
		/// Does stuff during Idle state.
		/// </summary>
		private void idleUpdate()
		{
			if(!handleJumpInput())
			{
				handleGroundMovementInput();
				rotateTowards(Vector3.back);	
	
				_timer -= Time.deltaTime;
				if(_timer < 0)
					wait();
			}
		}
	
		// Waiting 
		/// <summary>
		/// Does stuff at the beginning of the Waiting state.
		/// </summary>
		private void waitingInit()
		{
			TorsoRenderer.material = WaitingMaterial;
		}
	
		/// <summary>
		/// Does stuff during Waiting state.
		/// </summary>
		private void waitingUpdate()
		{
			if(!handleJumpInput() && !handleGroundMovementInput())
			{
				float rotDeltaX = WaitSinAmplitudeFactor * Mathf.Sin(WaitSinFrequencyFactor * Time.timeSinceLevelLoad);
				_cachedTransform.Rotate(new Vector3 (rotDeltaX, 0, 0));		
			}
		}
	
		// Moving Right
		/// <summary>
		/// Does stuff at the beginning of the MovingRight state.
		/// </summary>
		private void movingRightInit()
		{
			TorsoRenderer.material = MovingRightMaterial;
			FeetParticles.Play();
		}
	
		/// <summary>
		/// Does stuff during MovingRight state.
		/// </summary>
		private void movingRightUpdate()
		{
			if(!handleJumpInput())
			{
				if(Input.GetKey(KeyCode.RightArrow))
				{
					float positionDelta = HorizontalSpeed * Time.deltaTime;
					_cachedTransform.position += new Vector3(positionDelta, 0, 0);
					
					if(_cachedTransform.position.x > SideDistanceLimit)
						_cachedTransform.position = new Vector3(SideDistanceLimit, _cachedTransform.position.y, _cachedTransform.position.z);
	
					handleSinMovement();
					rotateTowards(Vector3.right);	
				}
				else 
					rest();
			}
		}
	
		/// <summary>
		/// Does stuff at the end of the MovingRight state.
		/// </summary>
		private void movingRightLeave()
		{
			FeetParticles.Stop();
		}
	
		// Moving Left
		/// <summary>
		/// Does stuff at the beginning of the MovingLeft state.
		/// </summary>
		private void movingLeftInit()
		{
			TorsoRenderer.material = MovingLeftMaterial;		
			FeetParticles.Play();
		}
	
		/// <summary>
		/// Does stuff during MovingLeft state.
		/// </summary>
		private void movingLeftUpdate()
		{
			if(!handleJumpInput())
			{
				if(Input.GetKey(KeyCode.LeftArrow)) 
				{
					float positionDelta = HorizontalSpeed * Time.deltaTime;
					_cachedTransform.position += new Vector3(-positionDelta, 0, 0);	
					
					if(_cachedTransform.position.x < -SideDistanceLimit)
						_cachedTransform.position = new Vector3(-SideDistanceLimit, _cachedTransform.position.y, _cachedTransform.position.z);
	
					handleSinMovement();
					rotateTowards(Vector3.left);	
				}
				else
					rest();	
			}
		}
	
		/// <summary>
		/// Does stuff at the end of the MovingLeft state.
		/// </summary>
		private void movingLeftLeave()
		{
			FeetParticles.Stop();
		}
	
		// Jumping
		/// <summary>
		/// Does stuff at the beginning of the Jumping state.
		/// </summary>
		private void jumpingInit()
		{
			_verticalSpeed = JumpSpeed;
			TorsoRenderer.material = JumpingMaterial;
		}
	
		/// <summary>
		/// Does stuff during Jumping state.
		/// </summary>
		private void jumpingUpdate()
		{
			handleAerialVerticalMovement();
	
			float rotDelta = JumpRotSpeed * Time.deltaTime;
			_cachedTransform.Rotate(new Vector3(0, rotDelta, 0));
	
			if(_verticalSpeed < 0)
				fall();
		}
	
		// Falling
		/// <summary>
		/// Does stuff at the beginning of the Falling state.
		/// </summary>
		private void fallingInit()
		{
			TorsoRenderer.material = FallingMaterial;	
		}
	
		/// <summary>
		/// Does stuff during Falling state.
		/// </summary>
		private void fallingUpdate()
		{
			handleAerialVerticalMovement();
	
			if(_cachedTransform.position.y <= GroundPosY)
				rest();		
		}
	}
}