//Author: Irving Malcolm

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// An instance of this class creates a finite state machine that manages triggers, callbacks and debugging messages.  By using states and events a matrix is created internally, so certain events change a state to another.
/// Methods can be set to be called when a state starts,  while it is running and when the state goes away. The fsm can be given a name and debug messages will use it.  The fsm can be set to print messages when an event is handled
/// and/or when an event changes the current state. A callback can be passed to the fsm and it will be called when the state changes, returning the new state key.   
/// </summary>
public class Fsm
{
	public delegate void SimpleMethod();
	public delegate void StateKeyMethod(object stateKey);

	//----------------------------------
	//  Public Fields
	//----------------------------------	
	public StateKeyMethod StateChangeCallback;

    //----------------------------------
    //  Private Variables
    //----------------------------------	
    private int _statesAmount;
    private int _eventsAmount;

    private int _currentStateIndex;
    private int _previousStateIndex;

    private StateInfo _currentStateInfo;

    private Dictionary<object, int> _states;
    private Dictionary<object, int> _events;

    private ArrayList _stateInfos;
    private ArrayList _relations;
	
	//Debug variables
	private string _debugLabel;		
	private bool _printStateChanges;
	private bool _printEventsHandled;	
	//--


    //----------------------------------
    //  Public Properties
    //----------------------------------
    public object CurrentStateKey { get { return _currentStateInfo.key; } }
    public object PreviousStateKey { get { return (_stateInfos[_previousStateIndex] as StateInfo).key; } }


    //----------------------------------
    //  Public Methods
    //----------------------------------
    public Fsm()
    {
		_debugLabel = "";
		_printEventsHandled = false;
		_printStateChanges = false;
		
        _statesAmount = 0;
        _eventsAmount = 0;

        _states = new Dictionary<object, int>();
        _events = new Dictionary<object, int>();

        _stateInfos = new ArrayList();
        _relations = new ArrayList();

        _currentStateIndex = -1;
        _previousStateIndex = -1;
        _currentStateInfo = null;
    }

    public void Destroy()
    {
        _states.Clear();
        _events.Clear();

        _stateInfos.Clear();
        
        foreach(ArrayList eventRow in _relations)
            eventRow.Clear();
       
        _relations.Clear();

        _currentStateInfo = null;
        _states = null;
        _events = null;
        _stateInfos = null;
        _relations = null;
    }

	/// <summary>
	/// Adds a state to the finite state machine.
	/// </summary>
	/// <param name="stateKey">State key. Anything can be used as a key, but an Enum value is recommended.</param>
    public void AddState(object stateKey)
    {
        AddState(stateKey, null, null, null);
    }

	/// <summary>
	/// Adds a state to the finite state machine.
	/// </summary>
	/// <param name="stateKey">State key. Anything can be used as a key, but an Enum value is recommended.</param>
	/// <param name="initStateFunc">Method that will be called when the state starts. Passing null for no method works.</param>
	    public void AddState(object stateKey, SimpleMethod initStateFunc)
    {
        AddState(stateKey, initStateFunc, null, null);
    }

	/// <summary>
	/// Adds a state to the finite state machine.
	/// </summary>
	/// <param name="stateKey">State key. Anything can be used as a key, but an Enum value is recommended.</param>
	/// <param name="initStateFunc">Method that will be called when the state starts. Passing null for no method works.</param>
	/// <param name="updateStateFunc">Method that will be called each time Update() method on the fsm instance. Usually called once per frame, inside a monobehaviour Update or FixedUpdate methods.</param>
    public void AddState(object stateKey, SimpleMethod initStateFunc, SimpleMethod updateStateFunc)
    {
        AddState(stateKey, initStateFunc, updateStateFunc, null);
    }

	/// <summary>
	/// Adds a state to the finite state machine.
	/// </summary>
	/// <param name="stateKey">State key. Anything can be used as a key, but an Enum value is recommended.</param>
	/// <param name="initStateFunc">Method that will be called when the state starts. Null can be passed for no method.</param>
	/// <param name="updateStateFunc">Method that will be called each time Update() method on the fsm instance. Usually called once per frame, inside a monobehaviour Update or FixedUpdate methods. Null can be passed for no method</param>
	/// <param name="leaveStateFunc">Method that will be called when the state finishes. Null can be passed for no method</param>
    public void AddState(object stateKey, SimpleMethod initStateFunc, SimpleMethod updateStateFunc, SimpleMethod leaveStateFunc)
    {
        _states.Add(stateKey, _statesAmount);

        ArrayList eventsRow = new ArrayList();
        for(int j = 0; j < _eventsAmount; j++)
            eventsRow.Add(_statesAmount);

        _relations.Add(eventsRow);
        _stateInfos.Add(new StateInfo(stateKey, initStateFunc, updateStateFunc, leaveStateFunc));

        _statesAmount++;
    }

	/// <summary>
	/// Adds an event that can trigger state change in the finite state machine.
	/// </summary>
	/// <param name="eventKey">Event key. Anything can be used as a key, but an Enum value is recommended.</param>
    public void AddEvent(object eventKey)
    {
		#if UNITY_EDITOR
        if (_statesAmount == 0)
            Debug.LogError(_debugLabel + " -> It is necessary to add at least 1 state if you wish to add an Event");
        else
		#endif
        {
            _events.Add(eventKey, _eventsAmount);

            for (int i = 0; i < _statesAmount; i++)
                (_relations[i] as ArrayList).Add(i);

            _eventsAmount++;
        }
    }

	/// <summary>
	/// Sets a relation. This is, we define which events trigger which state change in the finite state machine.
	/// </summary>
	/// <param name="originalStateKey">Original state key. Referring to the state that will change with a trigger event</param>
	/// <param name="triggerEventKey">Trigger event key. Referring to the event that will cause the state to change.</param>
	/// <param name="resultStateKey">Result state key. Referring to the result state after the trigger is handled.</param>
    public void SetRelation(object originalStateKey, object triggerEventKey, object resultStateKey)
    {
        (_relations[getIndexByKey(originalStateKey, _states)] as ArrayList)[getIndexByKey(triggerEventKey, _events)] = getIndexByKey(resultStateKey, _states);
    }

	/// <summary>
	/// Starts the finite state machine.
	/// </summary>
	/// <param name="initialStateKey">Initial state key. Referring to the state in which the fsm starts.</param>
    public void Start(object initialStateKey)
    {
        _currentStateIndex = getIndexByKey(initialStateKey, _states);
		_previousStateIndex = _currentStateIndex;  //If initFunc asks for this, it will crash if we don't give a default value first

        _currentStateInfo = _stateInfos[_currentStateIndex] as StateInfo;
		
		if(_currentStateInfo.initFunc != null)
        	_currentStateInfo.initFunc();
    }

	/// <summary>
	/// Update this instance. Calls the update method of the current state, if there method was set.
	/// </summary>
    public void Update()
    {
		#if UNITY_EDITOR			
        if (_currentStateIndex == -1)  //TODO: Remove this check when the game is closed
            Debug.LogError(_debugLabel + " -> The fsm has not been started");
		#endif
		
		if(_currentStateInfo.updateFunc != null)
        	_currentStateInfo.updateFunc();
    }

	/// <summary>
	/// Handles a triggering event. If the relation exists between the current state and the event passed,  a state change will be triggered.  
	/// </summary>
	/// <param name="eventKey">Event key. Anything can be used as a key, but an Enum value is recommended.</param>
    public void HandleEvent(object eventKey)
    {
		HandleEvent(eventKey, false);
    }
	
	/// <summary>
	/// Handles a triggering event. If the relation exists between the current state and the event passed,  a state change will be triggered.  
	/// </summary>
	/// <param name="eventKey">Event key. Anything can be used as a key, but an Enum value is recommended.</param>
	/// <param name="resetStateKey">If set to <c>true</c> , the state will be reset by calling its init function even if the result state is the same as the original.  This way a state can reset into itself.</param>
	public void HandleEvent(object eventKey, bool resetStateKey)
	{
		if(_printEventsHandled)
			Debug.Log(_debugLabel + " -> event handled: " + eventKey);
		
		#if UNITY_EDITOR
        if(_currentStateIndex == -1)  //TODO: Remove this check when the game is closed
            Debug.LogError(_debugLabel + " -> The fsm has not been started");
		#endif

        int newStateIndex = (int)(_relations[_currentStateIndex] as ArrayList)[getIndexByKey(eventKey, _events)];

        if(_currentStateIndex != newStateIndex)
        {
            _previousStateIndex = _currentStateIndex;
            _currentStateIndex = newStateIndex;
			
			if(_currentStateInfo.leaveFunc != null)
            	_currentStateInfo.leaveFunc();  //Will still have the stateInfo corresponding to the previous state
			
            _currentStateInfo = _stateInfos[_currentStateIndex] as StateInfo;
			
			if(_printStateChanges)
				Debug.Log(_debugLabel + " -> event " + eventKey + " changed state to: " + _currentStateInfo.key); 

			if(StateChangeCallback != null)
				StateChangeCallback(_currentStateInfo.key);
			
			if(_currentStateInfo.initFunc != null)
            	_currentStateInfo.initFunc();
        }
		else if(resetStateKey)
		{
			if(_currentStateInfo.initFunc != null)
			{
			    if(_printStateChanges)
					Debug.Log(_debugLabel + " -> event " + eventKey + " caused a reset on: " + _currentStateInfo.key);

            	_currentStateInfo.initFunc();
			}	
			else
				Debug.LogWarning(_debugLabel + " -> You are resetting a state that does not have a initFunc"); 
		}		
	}

	/// <summary>
	/// Sets the debug label so further debug prints makes reference to this particular finite state machine instance.
	/// </summary>
	/// <param name="debugLabel">Debug label.</param>
	public void SetDebugLabel(string debugLabel)
	{
		_debugLabel = debugLabel;
	}
	
	/// <summary>
	/// Enables state changes print.
	/// </summary>
	/// <param name="enable">If set to <c>true</c> , state change printing is enabled.</param>
	public void EnableStateChangesPrint(bool enable)
	{
		_printStateChanges = enable;
	}
	
	/// <summary>
	/// Enables events handled print.
	/// </summary>
	/// <param name="enable">If set to <c>true</c> , events handled printing is enabled.</param>
	public void EnableEventsHandledPrint(bool enable)
	{
		_printEventsHandled = enable;
	}
	
	
    //----------------------------------
    //  Private Methods
    //----------------------------------	
	/// <summary>
	/// Gets index by key.
	/// </summary>
	/// <returns>Index of the given ke.</returns>
	/// <param name="key">Key to look for corresponding index.</param>
	/// <param name="dictionary">Dictionary to perform the operation.</param>
    private int getIndexByKey(object key, Dictionary<object, int> dictionary)
    {
        int index = -1;
		
		#if UNITY_EDITOR
        if(!dictionary.ContainsKey(key))
			Debug.LogError(_debugLabel + " -> " + key.ToString() + " is not a registered state/event");
		else
		#endif
            index = (int)dictionary[key];

        return index;
    }

	// <summary>
	/// State info. This class is used to store state key (its identifier) and methods. These methods are 3, those that are going to be called when: 1) State starts. 2) State is running (called each time _fsm.Update() is called). 3) State ends.
	/// </summary>
	class StateInfo
	{
	    public object key;
	    public SimpleMethod initFunc;
	    public SimpleMethod updateFunc;
	    public SimpleMethod leaveFunc;
	
	    public StateInfo(object key, SimpleMethod initFunc, SimpleMethod updateFunc, SimpleMethod leaveFunc)
	    {
	        this.key = key;
	        this.initFunc = initFunc;
	        this.updateFunc = updateFunc;
	        this.leaveFunc = leaveFunc;
	    }
	}
}




