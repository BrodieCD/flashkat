﻿using UnityEngine;

namespace CommunicatorExample
{
	/// <summary>
	/// Bullet that travels vertically down and goes towards left click world position.
	/// </summary>
	public class Box : Bullet 
	{
		override public void Awake()
		{
			base.Awake();
			GameSignalEvents.Instance.OnBoxCreated.Emit(this, this);
		}
		
		override public void Start () 
		{
			base.Start();
	
			_direction = new Vector3(1, 0, 0);
		}
		
		override public void Update () 
		{
			base.Update();
		}
	
		override public void OnDestroy()
		{
			base.OnDestroy();
			GameSignalEvents.Instance.OnBoxDestroyed.Emit(this);
		}
		
		/// <summary>
		/// Callback passed to OnMouseLeftClick signal. Receives click world position.It makes this instance go in the same direction as the click world position without stopping when reaching that position.Speed also changes.
		/// </summary>
		/// <param name="dispatcher">Reference to dispatcher object.</param>
		/// <param name="worldPos">Click world position.</param>
		override public void OnMouseLeftClick(object dispatcher, Vector3 worldPos)
		{
			if(_isRaw)
			{
				_direction = (worldPos - CachedTransform.position).normalized; 
				_direction.z = 0;
				_isRaw = false;
	
				checkDirectionNotZero();
				updateSpeed();
			}
		}
	}
}