﻿using UnityEngine;
using System.Collections;

namespace CommunicatorExample
{
	/// <summary>
	/// Bullet that travels horizontally to the right and goes away from left click world position.
	/// </summary>
	public class Drop : Bullet 
	{
		override public void Awake()
		{
			base.Awake();
			GameSignalEvents.Instance.OnDropCreated.Emit(this, this);
		}
	
		override public void Start () 
		{
			base.Start();
	
			_direction = new Vector3(0, -1, 0);
		}
		
		override public void Update () 
		{
			base.Update();
		}
	
		override public void OnDestroy()
		{
			base.OnDestroy();
			GameSignalEvents.Instance.OnDropDestroyed.Emit(this);
		}
	
		/// <summary>
		/// Callback passed to OnMouseLeftClick signal. Receives click world position. Makes this instance go away from this position in the exact opposite direction and change speed. 
		/// </summary>
		/// <param name="dispatcher">Reference to dispatcher object.</param>
		/// <param name="worldPos">Click world position.</param>
		override public void OnMouseLeftClick(object dispatcher, Vector3 worldPos)
		{
			if(_isRaw)
			{
				_direction = (CachedTransform.position - worldPos).normalized; 
				_direction.z = 0;
				_isRaw = false;
	
				checkDirectionNotZero();
				updateSpeed();
			}
		}
	}
}