using UnityEngine;
using System.Collections;

namespace CommunicatorExample
{
	/// <summary>
	/// Bullet is a base class for objects that are spawned from cannons.  These can be drop or boxes. 
	/// </summary>
	public class Bullet : MonoBehaviour
	{
		[HideInInspector]
		public Transform CachedTransform;
	
		protected float _speed;
		protected Vector3 _direction;
	
		protected bool _isRaw = true;
	
	
		virtual public void Awake()
		{
			FwSignalEvents.Instance.OnMouseLeftClick.Connect(this, OnMouseLeftClick);
			GameSignalEvents.Instance.OnBulletSpeedToggle.Connect(this, OnBulletSpeedToggle);
	
			CachedTransform = transform;
		}
	
		virtual public void Start () 
		{
			updateSpeed();
		}
		
		virtual public void Update () 
		{
			Vector3 deltaPos = _speed * Time.deltaTime * _direction;
			CachedTransform.position += deltaPos;	
		}
	
		virtual public void OnDestroy ()
		{
			FwSignalEvents.Instance.OnMouseLeftClick.Disconnect(this);
			GameSignalEvents.Instance.OnBulletSpeedToggle.Disconnect(this);
		}
	
		void OnTriggerEnter(Collider other)
		{
			GameObject.Destroy(this.gameObject);
		}
	
		/// <summary>
		/// Callback passed to OnMouseLeftClick signal.  Receives click world position.It is implemented on Drop and Box subclasses.
		/// </summary>
		/// <param name="dispatcher">Reference to dispatcher object.</param>
		/// <param name="worldPos">Click world position.</param>
		virtual public void OnMouseLeftClick(object dispatcher, Vector3 worldPos){}
	
		public void OnBulletSpeedToggle (object dispatcher)
		{
			updateSpeed();
		}
	
		/// <summary>
		/// Sets bullet direction.
		/// </summary>
		/// <param name="direction">New direction vector.</param>
		public void SetDirection(Vector3 direction)
		{
			_direction = direction;
		}
	
		/// <summary>
		/// Updates the speed.
		/// </summary>
		protected void updateSpeed()
		{
			if(_isRaw)
				_speed = CommunicatorExample.GameConfig.BulletRawSpeed;
			else
				_speed = CommunicatorExample.GameConfig.BulletPushedSpeed;
		}
	
		/// <summary>
		/// Checks if direction magnitude is zero and if it is, assigns a random direction.
		/// </summary>
		protected void checkDirectionNotZero()
		{
			if(_direction.magnitude == 0)
			{
				//print("Direction randomized");
				_direction.x = Random.value;
				_direction.y = Random.value;
				_direction.Normalize();
			}
		}
	}
}
