﻿using UnityEngine;
using System.Collections;

namespace CommunicatorExample
{
	/// <summary>
	/// Base class for entities that spawn bullets, that can be drops or boxes.
	/// </summary>
	public class Cannon : MonoBehaviour 
	{
		public float MaxDelay;
		public float MinDelay;
	
		public Transform SpawnPoint;
		public GameObject BulletPrefab;
	
		private float _timer;
		
		private Transform _cachedTransform;
	
		void Awake ()
		{
			_cachedTransform = transform;
		}
	
		void Start () 
		{
			resetTimer();
		}
		
		void Update () 
		{
			_timer -= Time.deltaTime;
	
			if(_timer <= 0)
			{
				resetTimer();
				GameSignalEvents.Instance.OnCreateBullet.EmitParams(this, BulletPrefab, SpawnPoint.position, _cachedTransform.forward); 
			}
		}
	
		/// <summary>
		/// Resets time count that control spawns. It assign a random value to the timer using Max and Min values set at inspector.
		/// </summary>
		private void resetTimer()
		{
			_timer = Random.Range(MinDelay, MaxDelay);
		}
	}
}