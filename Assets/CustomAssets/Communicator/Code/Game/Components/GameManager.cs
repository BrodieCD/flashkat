﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CommunicatorExample
{
	/// <summary>
	/// This class manages all core game logic that in this case is about creating containers, displaying bullet counts and create bullets.
	/// </summary>
	public class GameManager : MonoBehaviour 
	{
		public enum MouseButtons
		{
			Left = 0,
			Right,
			Middle
		}
	
		public GameObject ClickFxPrefab;
	
		public GameObject DropPrefab;
		public GameObject BoxPrefab;
	
		public Camera GameCamera;
		public Transform MouseWorldPosReference;
	
		public Text DropsCountDisplay;
		public Text BoxesCountDisplay;
	
		private const String _DROPS_CONTAINER_NAME = "DropsContainer";
		private const String _BOXES_CONTAINER_NAME = "BoxesContainer";
		private const String _CLICK_FXS_CONTAINER_NAME = "ClickFxsContainer";
	
		private const String _DROPS_DISPLAY_PREFIX = "Drops Count: ";
		private const String _BOXES_DISPLAY_PREFIX = "Boxes Count: ";
	
		private Transform _dropsContainer;
		private Transform _boxesContainer;
		private Transform _clickFxsContainer;
	
		private float _referenceDistanceFromCamera;
	 
		private int _dropsCount = 0;
		private int _boxesCount = 0;
	
		void Awake()
		{
			SignalBase.PrintAllSignalActions = true;
	
			GameSignalEvents.Instance.OnDropCreated.Connect(this, OnDropCreated);
			GameSignalEvents.Instance.OnDropDestroyed.Connect(this, OnDropDestroyed);
	
			GameSignalEvents.Instance.OnBoxCreated.Connect(this, OnBoxCreated);
			GameSignalEvents.Instance.OnBoxDestroyed.Connect(this, OnBoxDestroyed);
	
			GameSignalEvents.Instance.OnCreateBullet.Connect(this, OnCreateBullet);
		}
	
		void Start () 
		{
			_dropsContainer = (new GameObject(_DROPS_CONTAINER_NAME)).transform;
			_boxesContainer = (new GameObject(_BOXES_CONTAINER_NAME)).transform;
			_clickFxsContainer = (new GameObject(_CLICK_FXS_CONTAINER_NAME)).transform;
	
			_referenceDistanceFromCamera = MouseWorldPosReference.position.z - GameCamera.transform.position.z;
		}
		
		void Update () 
		{
			if(Input.GetMouseButtonDown((int)MouseButtons.Left))
			{
				Vector3 mouseWorldPos = getMouseWorldPos();
				FwSignalEvents.Instance.OnMouseLeftClick.EmitParams(this, mouseWorldPos);
				GameObject newFx = (GameObject)GameObject.Instantiate(ClickFxPrefab, mouseWorldPos, Quaternion.identity);
				newFx.transform.parent = _clickFxsContainer;
			}	
			else if(Input.GetMouseButtonDown((int)MouseButtons.Right))
				createBullet(DropPrefab, getMouseWorldPos(), Vector3.zero);
			else if(Input.GetMouseButtonDown((int)MouseButtons.Middle))
				createBullet(BoxPrefab, getMouseWorldPos(), Vector3.zero);
	
			if(Input.GetKeyDown(KeyCode.Space))
				CommunicatorExample.GameConfig.ToggleBulletSpeeds();
		}
	
		void OnDestroy()
		{
			GameSignalEvents.Instance.OnDropCreated.Disconnect(this);		
			GameSignalEvents.Instance.OnBoxCreated.Disconnect(this);
	
			GameSignalEvents.Instance.OnDropDestroyed.Disconnect(this);
			GameSignalEvents.Instance.OnBoxDestroyed.Disconnect(this);
	
			GameSignalEvents.Instance.OnCreateBullet.Disconnect(this);
		}
	
		/// <summary>
		/// Callback passed to OnDropCreated signal to be called when a drop is created in any way. It assigns parent container, and updates drop count.
		/// </summary>
		/// <param name="dispatcher">Signal emitter reference.</param>
		/// <param name="drop">Drop instance created.</param>
		public void OnDropCreated (object dispatcher, Drop drop)
		{
			drop.CachedTransform.parent = _dropsContainer;
			_dropsCount++;
			updateDropCountDisplay();
		}
	
		/// <summary>
		/// Callback passed to OnDropDestroyed signal to be called when a drop is destroyed. It updates drop count.
		/// </summary>
		/// <param name="dispatcher">Signal emitter reference.</param>
		public void OnDropDestroyed (object dispatcher)
		{
			_dropsCount--;	
			updateDropCountDisplay();
		}
	
		/// <summary>
		/// Callback passed to OnBoxCreated signal to be called when a drop is created in any way. It assigns parent container, and updates box count.
		/// </summary>
		/// <param name="dispatcher">Signal emitter reference.</param>
		/// <param name="box">Box instance created.</param>
		public void OnBoxCreated (object dispatcher, Box box)
		{
			box.CachedTransform.parent = _boxesContainer;
			_boxesCount++;
			updateBoxCount();
		}
	
		/// <summary>
		/// Callback passed to OnBoxDestroyed signal to be called when a drop is destroyed. It updates box count.
		/// </summary>
		/// <param name="dispatcher">Signal emitter reference.</param>
		public void OnBoxDestroyed (object dispatcher)
		{
			_boxesCount--;
			updateBoxCount();
		}
	
		/// <summary>
		/// Callback passed to OnCreateBullet signal to be called when a bullet creation is required.  Creates bullet using the provided prefab, position and direction.
		/// </summary>
		/// <param name="dispatcher">Signal emitter reference.</param>
		/// <param name="bulletPrefab">Prefab with Bullet superclass script, that can be Drop or Box.</param>
		/// <param name="position">Position to create bullet at.</param>
		/// <param name="direction">Direction for bullet to travel.</param>
		public void OnCreateBullet(object dispatcher, GameObject bulletPrefab, Vector3 position, Vector3 direction)
		{
			createBullet(bulletPrefab, position, direction);
		}
	
		/// <summary>
		/// Creates bullet using the provided prefab, position and direction.
		/// </summary>
		/// <param name="bulletPrefab">Prefab with Bullet superclass script, that can be Drop or Box.</param>
		/// <param name="position">Position to create bullet at.</param>
		/// <param name="direction">Direction for bullet to travel.</param>
		private void createBullet(GameObject bulletPrefab, Vector3 position, Vector3 direction)
		{
			GameObject gameObject = (GameObject)GameObject.Instantiate(bulletPrefab, position, Quaternion.identity);
	
			if(direction.magnitude != 0)
				gameObject.GetComponent<Bullet>().SetDirection(direction);
		}
		
		/// <summary>
		/// Updates drop count display.
		/// </summary>
		private void updateDropCountDisplay()
		{	
			if(DropsCountDisplay != null)
				DropsCountDisplay.text = _DROPS_DISPLAY_PREFIX + _dropsCount.ToString();
		}
	
		/// <summary>
		/// Updates box count display.
		/// </summary>
		private void updateBoxCount()
		{
			if(BoxesCountDisplay != null)
				BoxesCountDisplay.text = _BOXES_DISPLAY_PREFIX + _boxesCount.ToString();
		}
	
		/// <summary>
		/// Calculates mouse world position.
		/// </summary>
		/// <returns>Mouse world position.</returns>
		private Vector3 getMouseWorldPos()
		{
			Vector3 mouseWorldPos = GameCamera.ScreenToWorldPoint(new Vector3( Input.mousePosition.x, Input.mousePosition.y, _referenceDistanceFromCamera));
			return mouseWorldPos;
		}
	}
}