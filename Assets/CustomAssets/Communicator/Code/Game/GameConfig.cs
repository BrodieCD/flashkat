﻿using UnityEngine;

namespace CommunicatorExample
{
	/// <summary>
	/// Static class used to store game variables. In this case, bullet speed that can be changed at runtime and we use a signal event to update every existing bullet.
	/// </summary>
	static public class GameConfig
	{	
		static public float BulletRawSpeed = 5;
		static public float BulletPushedSpeed = 10;
	
		static private float _slowBulletRawSpeed = 2.5f;
		static private float _slowBulletPushedSpeed = 5;
	
		static private float _fastBulletRawSpeed = 5;
		static private float _fastBulletPushedSpeed = 10;
	
		static private bool _isBulletSpeedFast = true;
	
		/// <summary>
		/// Toggles bullet speeds for raw and pushed states. Sends a signal event to update all existing bullets that are going to be listening to this event since they are created.
		/// </summary>
		static public void ToggleBulletSpeeds()
		{
			if(_isBulletSpeedFast)
			{
				BulletRawSpeed = _slowBulletRawSpeed;
				BulletPushedSpeed = _slowBulletPushedSpeed;		
			}
			else
			{
				BulletRawSpeed = _fastBulletRawSpeed;
				BulletPushedSpeed = _fastBulletPushedSpeed;
			}
	
			_isBulletSpeedFast = !_isBulletSpeedFast;
	
			GameSignalEvents.Instance.OnBulletSpeedToggle.Emit("GameConfig"); //As there is no instance to change as dispatcher object, we can use a string describing our dispatcher. 
		}
	}
}
