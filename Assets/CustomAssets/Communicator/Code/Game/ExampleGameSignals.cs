﻿using System;
using UnityEngine;

namespace CommunicatorExample
{
	/// <summary>
	/// Signal that uses bullet prefab, bullet starting position and direction as parameters.Used to fire a bullet using an signal event.
	/// </summary>
	public class SignalBullet : Signal<GameObject, Vector3, Vector3>
	{
		public SignalBullet(String eventName) : base(eventName) { }
		
		/// <summary>
		/// Emits on this signal.
		/// </summary>
		/// <param name="dispatcher">Reference to dispatcher object.</param>
		/// <param name="bulletPrefab">Bullet prefab to instantiate a Drop or Box.</param>
		/// <param name="position">Position to place instance in world space.</param>
		/// <param name="direction">Direction for the instantiated bullet to travel.</param>
		public void EmitParams(object dispatcher, GameObject bulletPrefab, Vector3 position, Vector3 direction)
		{
			Emit(dispatcher, bulletPrefab, position, direction);
		}	
	}
}