﻿using UnityEngine;
using System.Collections;

namespace CommunicatorExample
{
	/// <summary>
	/// Class used to encapsulate signals of Game category.
	/// </summary>
	public class GameSignalEvents : Singleton<GameSignalEvents> 
	{
		public Signal<Drop> OnDropCreated = new Signal<Drop>("DropCreated");
		public SignalSimple OnDropDestroyed = new SignalSimple("DropDestroyed");
	
		public Signal<Box> OnBoxCreated = new Signal<Box>("BoxCreated");
		public SignalSimple OnBoxDestroyed = new SignalSimple("BoxDestroyed");
	
		public SignalBullet OnCreateBullet = new SignalBullet("CreateDrop");
		public SignalSimple OnBulletSpeedToggle = new SignalSimple("BulletSpeedToggle");
	}
}