using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
/// <summary>
/// Makes GameObject instance that uses this script to destroy itself when particle system is done playing.
/// </summary>
public class ParticlesAutoDestroy : MonoBehaviour 
{
	private ParticleSystem _particles;

	void Awake()
	{
		_particles = GetComponent<ParticleSystem>();
	}

	void Update () 
	{
		if(!_particles.isPlaying)
			GameObject.Destroy(gameObject);
	}
}
