using UnityEngine;
using System;

/// <summary>
/// Signal that uses a position parameter.
/// </summary>
public class SignalPosition : Signal<Vector3>
{
	public SignalPosition(String eventName) : base(eventName) { }
	
	/// <summary>
	/// Emits on this signal.
	/// </summary>
	/// <param name="dispatcher">Reference to dispatcher object.</param>
	/// <param name="pos">Position passed to signal.</param>
	public void EmitParams(object dispatcher, Vector3 pos)
	{
		Emit(dispatcher, pos);
	}	
}


