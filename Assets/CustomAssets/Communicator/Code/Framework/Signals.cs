using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the main class for Signals.  Signals manage listeners and communication without entities communicating holding references from each other. References could be sent once, but still,  if emitters or receivers are
/// missing, nothing happens, so we have decoupled communication. Using this class a communication system between game entities, we can also create emission methods that describe parameters, delegate check on
// callback passing, and connections, emissions and receptions can be printed for debugging.   
/// </summary>
public abstract class SignalBase
{
	static public bool PrintAllSignalActions = false;
	
	public bool PrintThisSignalActions = false;

	private String _eventName;

	private bool _printSignal { get { return (PrintAllSignalActions || PrintThisSignalActions); } }
	
	public SignalBase(String eventName)
	{
		_eventName = eventName;
	}
	
	/// <summary>
	/// Handles debug emitter message.
	/// </summary>
	/// <param name="dispatcher">Dispatcher object.</param>
	protected void handleDebugEmitterMessage(object dispatcher)
	{
		if(_printSignal)
			Debug.Log(dispatcher + " has emitted event: " + _eventName);	
	}
	
	/// <summary>
	/// Handles debug receiver message.
	/// </summary>
	/// <param name="dispatcher">Receiver object.</param>
	protected void handleDebugReceiversMessage(object receiver)
	{
		if(_printSignal)
			Debug.Log(receiver + " has received event: " + _eventName);		
	}
		
	/// <summary>
	/// Connects listener and callback to signal, checking first if it is already connected.
	/// </summary>
	/// <param name="connections">Connections dictionary to add to. It is a dictionary with listener and callback.</param>
	/// <param name="listener">Listener reference.</param>
	/// <param name="connection">Callback.</param>
	/// <typeparam name="ConnectionType">The delegate type of this connection.</typeparam>
	protected void safeConnect<ConnectionType>(Dictionary<object, ConnectionType> connections, object listener, ConnectionType connection)
	{
		if(connections.ContainsKey(listener))
		{
			if(_printSignal)	
				Debug.Log(listener + " is already listening to event: " + _eventName);			
		}
		else
		{
			if(_printSignal)
				Debug.Log(listener + " is now connected to event: " + _eventName);			
			
			connections.Add(listener, connection);
		}
	}
	
	/// <summary>
	/// Disconnects listener from signal, checking first if it is connected.
	/// </summary>
	/// <param name="connections">Connections dictionary to remove from. It is a dictionary with listener and callback.</param>
	/// <param name="listener">Listener.</param>
	/// <typeparam name="ConnectionType">The delegate type of this connection.</typeparam>
	protected void safeDisconnect<ConnectionType>(Dictionary<object, ConnectionType> connections, object listener)
	{
		if(connections.ContainsKey(listener))
		{
			if(_printSignal)
				Debug.Log(listener + " has disconnected from: " + _eventName);			
			
			connections.Remove(listener);
		}
		else if(_printSignal)	
			Debug.Log(listener + " is not listening to event: " + _eventName);								
	}
	
	/// <summary>
	/// Disconnects all listeners from this signal, checking first if there are connections.
	/// </summary>
	/// <param name="connections">Connections dictionary to remove from. It is a dictionary with listener and callback.</param>
	/// <typeparam name="ConnectionType">The delegate type of this connection.</typeparam>
	protected void safeDisconnectAll<ConnectionType>(Dictionary<object, ConnectionType> connections)
	{
		if(connections.Count > 0)
		{
			if(_printSignal)	
				Debug.Log("All listeners haven been disconnected from: " + _eventName);						
			
			connections.Clear();	
		}
		else if(_printSignal)	
			Debug.Log(_eventName + " has no listeners to remove.");						
	}	
}

/*! Signal with no arguments
 */
/// <summary>
/// This is a signal with no arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class SignalSimple : SignalBase
{
	public delegate void Connection(object dispatcher);
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	
	
	public SignalSimple(String eventName) : base(eventName)
	{

	}

	/// <summary>
	/// Emit signal. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	public void Emit(object dispatcher)
	{
		handleDebugEmitterMessage(dispatcher);

		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);							
			pair.Value(dispatcher);
		}
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}

	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}	

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}		

} // class Signal



/*! Signal with one argument
 */
/// <summary>
/// This is a signal with one arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class Signal<T1> : SignalBase
{
	public delegate void Connection(object dispatcher, T1 arg1);
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	 
	
	public Signal(String eventName) : base(eventName)
	{

	}

	/// <summary>
	/// Emits signal with its argument. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	/// <param name="arg1">First argument.</param>
	public void Emit(object dispatcher, T1 arg1)
	{
		handleDebugEmitterMessage(dispatcher);
		
		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);						
			pair.Value(dispatcher, arg1);
		}
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}

	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}		

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}	
	
} // class Signal<T1>



/*! Signal with two arguments
 */
/// <summary>
/// This is a signal with two arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class Signal<T1, T2> : SignalBase
{
	public delegate void Connection(object dispatcher, T1 arg1, T2 arg2);
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	 	
	
	public Signal(String eventName) : base(eventName)
	{

	}

	/// <summary>
	/// Emits signal with its arguments. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	/// <param name="arg1">First argument.</param>
	/// <param name="arg2">Second argument.</param>
	public void Emit(object dispatcher, T1 arg1, T2 arg2)
	{
		handleDebugEmitterMessage(dispatcher);
		
		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);	
			pair.Value(dispatcher, arg1, arg2);
		}	
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}

	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}	

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}	
	
} // class Signal<T1, T2>



/*! Signal with three arguments
 */
/// <summary>
/// This is a signal with three arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class Signal<T1, T2, T3> : SignalBase
{
	public delegate void Connection(object dispatcher, T1 arg1, T2 arg2, T3 arg3);
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	 
	
	public Signal(String eventName) : base(eventName)
	{

	}
	
	/// <summary>
	/// Emits signal with its arguments. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	/// <param name="arg1">First argument.</param>
	/// <param name="arg2">Second argument.</param>
	/// <param name="arg3">Third argument.</param>
	public void Emit(object dispatcher, T1 arg1, T2 arg2, T3 arg3)
	{
		handleDebugEmitterMessage(dispatcher);
		
		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);		
			pair.Value(dispatcher, arg1, arg2, arg3);
		}
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}

	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}	

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}	
	
} // class Signal<T1, T2, T3>



/*! Signal with four arguments
 */
/// <summary>
/// This is a signal with four arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class Signal<T1, T2, T3, T4> : SignalBase
{
	public delegate void Connection(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4);
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	 
	
	public Signal(String eventName) : base(eventName)
	{

	}	
	
	/// <summary>
	/// Emits signal with its arguments. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	/// <param name="arg1">First argument.</param>
	/// <param name="arg2">Second argument.</param>
	/// <param name="arg3">Third argument.</param>
	/// <param name="arg4">Fourth argument.</param>
	public void Emit(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
	{
		handleDebugEmitterMessage(dispatcher);
		
		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);	
			pair.Value(dispatcher, arg1, arg2, arg3, arg4);
		}		
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}

	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}		

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}		
	
} // class Signal<T1, T2, T3, T4>



/*! Signal with five arguments
 */
/// <summary>
/// This is a signal with five arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class Signal<T1, T2, T3, T4, T5> : SignalBase
{
	public delegate void Connection(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);	
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	 
	
	public Signal(String eventName) : base(eventName)
	{

	}
	
	/// <summary>
	/// Emits signal with its arguments. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	/// <param name="arg1">First argument.</param>
	/// <param name="arg2">Second argument.</param>
	/// <param name="arg3">Third argument.</param>
	/// <param name="arg4">Fourth argument.</param>
	/// <param name="arg5">Fifth argument.</param>
	public void Emit(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
	{
		handleDebugEmitterMessage(dispatcher);
		
		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);	
			pair.Value(dispatcher, arg1, arg2, arg3, arg4, arg5);
		}
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}

	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}		

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}	

} // class Signal<T1, T2, T3, T4, T5>



/*! Signal with six arguments
 */
/// <summary>
/// This is a signal with six arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class Signal<T1, T2, T3, T4, T5, T6> : SignalBase
{
	public delegate void Connection(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	 
	
	public Signal(String eventName) : base(eventName)
	{

	}
	
	/// <summary>
	/// Emits signal with its arguments. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	/// <param name="arg1">First argument.</param>
	/// <param name="arg2">Second argument.</param>
	/// <param name="arg3">Third argument.</param>
	/// <param name="arg4">Fourth argument.</param>
	/// <param name="arg5">Fifth argument.</param>
	/// <param name="arg6">Sixth argument.</param>
	public void Emit(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
	{
		handleDebugEmitterMessage(dispatcher);
		
		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);	
			pair.Value(dispatcher, arg1, arg2, arg3, arg4, arg5, arg6);
		}
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}

	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}		

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}		
	
} // class Signal<T1, T2, T3, T4, T5, T6>



/*! Signal with seven arguments
 */
/// <summary>
/// This is a signal with seven arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class Signal<T1, T2, T3, T4, T5, T6, T7> : SignalBase
{
	public delegate void Connection(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	 

	public Signal(String eventName) : base(eventName)
	{

	}
	
	/// <summary>
	/// Emits signal with its arguments. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	/// <param name="arg1">First argument.</param>
	/// <param name="arg2">Second argument.</param>
	/// <param name="arg3">Third argument.</param>
	/// <param name="arg4">Fourth argument.</param>
	/// <param name="arg5">Fifth argument.</param>
	/// <param name="arg6">Sixth argument.</param>
	/// <param name="arg7">Seventh argument.</param>
	public void Emit(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
	{
		handleDebugEmitterMessage(dispatcher);
		
		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);	
			pair.Value(dispatcher, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		}
		
		//handleLaterConnectionChanges<Connection>(_connections, _toConnect, _toDisconnect);	
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}
	
	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}		

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}		
	
} // class Signal<T1, T2, T3, T4, T5, T6, T7>



/*! Signal with eight arguments
 */
/// <summary>
/// This is a signal with eight arguments.  For signals for each amount of arguments,  delegates are differents and with them, dictionaries for connections.
/// </summary>
public class Signal<T1, T2, T3, T4, T5, T6, T7, T8> : SignalBase
{
	public delegate void Connection(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8);
	private Dictionary<object, Connection> _connections = new Dictionary<object, Connection>();	 
	
	public Signal(String eventName) : base(eventName)
	{

	}	
	
	/// <summary>
	/// Emits signal with its arguments. This is, dispatching an event for signal listeners to handle.
	/// </summary>
	/// <param name="dispatcher">Dispatcher reference.</param>
	/// <param name="arg1">First argument.</param>
	/// <param name="arg2">Second argument.</param>
	/// <param name="arg3">Third argument.</param>
	/// <param name="arg4">Fourth argument.</param>
	/// <param name="arg5">Fifth argument.</param>
	/// <param name="arg6">Sixth argument.</param>
	/// <param name="arg7">Seventh argument.</param>
	/// <param name="arg8">Eight argument.</param>
	public void Emit(object dispatcher, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
	{
		handleDebugEmitterMessage(dispatcher);
		
		Dictionary<object, Connection> connections = new Dictionary<object, Connection>(_connections);
		
		foreach(KeyValuePair<object, Connection> pair in connections)
		{
			handleDebugReceiversMessage(pair.Key);	
			pair.Value(dispatcher, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
		}
	}
	
	/// <summary>
	/// Adds listener and connection to a signal, so callback is called on signal emission.
	/// </summary>
	/// <param name="listener">Listener reference to add.</param>
	/// <param name="connection">Delegate to be called on signal emission.</param>
	public void Connect(object listener, Connection connection)
	{
		safeConnect<Connection>(_connections, listener, connection);
	}

	/// <summary>
	/// Removes listener from this signal.
	/// </summary>
	/// <param name="listener">Listener to be removed.</param>
	public void Disconnect(object listener)
	{
		safeDisconnect<Connection>(_connections, listener);
	}		

	/// <summary>
	/// Disconnects all listeners from this signal.
	/// </summary>
	public void DisconnectAll()
	{
		safeDisconnectAll<Connection>(_connections);		
	}	
	
} // class Signal<T1, T2, T3, T4, T5, T6, T7, T8>