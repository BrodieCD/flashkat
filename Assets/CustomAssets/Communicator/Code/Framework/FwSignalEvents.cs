using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Class used to encapsulate signals of Framework category.
/// </summary>
public class FwSignalEvents :Singleton<FwSignalEvents>
{	
	public SignalPosition OnMouseLeftClick = new SignalPosition("MouseLeftClick");
}

