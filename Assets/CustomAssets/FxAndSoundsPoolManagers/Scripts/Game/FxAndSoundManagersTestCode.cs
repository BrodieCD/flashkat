using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace FxAndSoundsManagersExample
{
	/// <summary>
	/// Fx and sound managers test code.
	/// </summary>
	public class FxAndSoundManagersTestCode : MonoBehaviour 
	{
		public bool AnimateSpheres = true;
	
		public bool PlayFireShotSound3d = true;
		public bool PlayFireWorks3d = true;
		public bool PlaySpark3d = false;
	
		public bool PlayFireStorm3d = true;
		public bool PlayWind3d = true;
		public bool PlayRain3d = false;
	
		public List<FxAndSounds.FxsNames> randomFireworksFxList; 
		public List<FxAndSounds.SoundsNames> randomFireworksSoundsList;
		public List<FxAndSounds.SoundsNames> randomFireworksSounds3dList;
	
		public Transform Rain;
		public Transform Wind;
		public Transform Flame;
	
		public Transform Music;
	
		private const int _MAX_SONG_INDEX = 2;
	
		private Camera _cachedCam;
	
		private bool _isBlizzardPlaying = false;
		private bool _isFireStormPlaying = false;
		private bool _isWindPlaying = false;
	
		private int _musicIndex = 0;
	
		private bool _wereSpheresAnimating = true;
	
		void Awake ()
		{
			_cachedCam = Camera.main;
		}
		
		void Update()
		{
			handleInput();
			handleAnimationChange();
		}
	
		/// <summary>
		/// Handles elemental spheres animators switch on/off based on AnimateSpheres bool value that can be changed via inspector at runtime. 
		/// </summary>
		private void handleAnimationChange()
		{
			if(AnimateSpheres)
			{
				if(!_wereSpheresAnimating)
				{
					stopSphere(Rain);
					stopSphere(Wind);
					stopSphere(Flame);
				}
			}
			else 
			{
				if(_wereSpheresAnimating)
				{
					animateSphere(Rain);
					animateSphere(Wind);
					animateSphere(Flame);
				}
			}
	
			_wereSpheresAnimating = AnimateSpheres;
		}
	
		/// <summary>
		/// Plays elemental sphere animation.
		/// </summary>
		/// <param name="sphere">Sphere.</param>
		private void animateSphere(Transform sphere)
		{
			sphere.GetComponent<Animator>().StartPlayback();
		}
	
		/// <summary>
		/// Stops elemental sphere animation.
		/// </summary>
		/// <param name="sphere">Sphere.</param>
		private void stopSphere(Transform sphere)
		{
			sphere.GetComponent<Animator>().StopPlayback();
		}
	
		/// <summary>
		/// Handles mouse input to play fx, sounds and toggle elemental spheres.
		/// </summary>
		private void handleInput()
		{
			int mouseButtonDown = -1;
			if (Input.GetMouseButtonDown (0))
				mouseButtonDown = 0;
			else if (Input.GetMouseButtonDown (1))
					mouseButtonDown = 1;
			else if (Input.GetMouseButtonDown (2))
						mouseButtonDown = 2;
	
			if (mouseButtonDown != -1) 
			{
				RaycastHit hit = new RaycastHit ();
				Ray ray = _cachedCam.ScreenPointToRay (Input.mousePosition);
	
				if (Physics.Raycast (ray, out hit, Mathf.Infinity)) 
				{
					Vector3 collisionPoint = hit.point;
					switch (mouseButtonDown) 
					{
						case 0:
							FxManager.EnableFx (this, FxAndSounds.FxsNames.Fire, collisionPoint, Quaternion.identity);
	
							if (PlayFireShotSound3d)
								SoundManager.PlayAutomaticSoundAtPoint (this, FxAndSounds.SoundsNames.Fire3d, collisionPoint);
							else
								SoundManager.PlayOneShotSound (this, FxAndSounds.SoundsNames.Fire);
							break;
	
						case 1:
							FxManager.EnableFx (this, FxAndSounds.FxsNames.Spark, collisionPoint, Quaternion.identity);
	
							if (PlaySpark3d)
								SoundManager.PlayAutomaticSoundAtPoint (this, FxAndSounds.SoundsNames.Spark3d, collisionPoint);
							else
								SoundManager.PlayOneShotSound (this, FxAndSounds.SoundsNames.Spark);
							break;
	
						case 2:
							FxManager.EnableRandomFxFromList (this, randomFireworksFxList, collisionPoint, Quaternion.identity);
	
							if (PlayFireWorks3d)
								SoundManager.PlayRandomAutomaticSoundFromListAtPoint (this, randomFireworksSounds3dList, collisionPoint);
							else
								SoundManager.PlayRandomOneShotSoundFromList (this, randomFireworksSoundsList);
							break;
					}
	
					Transform hitTransform = hit.transform;
					if (hitTransform == Rain)
						_isBlizzardPlaying = handleElementalSphereToggle (hitTransform, _isBlizzardPlaying, FxAndSounds.FxsNames.Rain, FxAndSounds.SoundsNames.Rain, "Rain", PlayRain3d);
					else if (hitTransform == Flame)
						_isFireStormPlaying = handleElementalSphereToggle (hitTransform, _isFireStormPlaying, FxAndSounds.FxsNames.Flame, FxAndSounds.SoundsNames.Flame, "Flame", PlayFireStorm3d);
					else if (hitTransform == Wind)
						_isWindPlaying = handleElementalSphereToggle (hitTransform, _isWindPlaying, FxAndSounds.FxsNames.Wind, FxAndSounds.SoundsNames.Wind, "Wind", PlayWind3d);
					else if (hitTransform == Music) 
					{
						_musicIndex++;
	
						if (_musicIndex > _MAX_SONG_INDEX)
							_musicIndex = 0;
	
						switch (_musicIndex) 
						{
							case 0:
								SoundManager.StopMusic (this);
								break;
							case 1:
								SoundManager.PlayMusic (this, FxAndSounds.SongsNames.Believe);
								break;
							case 2:
								SoundManager.PlayMusic (this, FxAndSounds.SongsNames.Will);
								break;
						}
					}
				}
			}
		}
	
		/// <summary>
		/// Handles elemental sphere toggle.
		/// </summary>
		/// <returns><c>true</c>, if bool to check has changed to true, <c>false</c> if has changed to false.</returns>
		/// <param name="sphere">Sphere.</param>
		/// <param name="boolToCheck">If set to <c>true</c> means the bool to check is true.</param>
		/// <param name="fxName">Fx name enum.</param>
		/// <param name="soundNameToToggle">Sound name enum to toggle.</param>
		/// <param name="setSoundAs3d">Play sphere sound with 3d or flat sound.</param>
		/// <param name="key">Key used to keep track of the sound to toggle.</param>
		private bool handleElementalSphereToggle(Transform sphere, bool boolToCheck, FxAndSounds.FxsNames fxName, FxAndSounds.SoundsNames soundNameToToggle, object key, bool setSoundAs3d)
		{
			bool boolToCheckNewValue = boolToCheck;
	
			if(boolToCheck)		
			{
				GameObject fxInstance = FxManager.DisableFX(this, key);
				GameObject soundInstance = SoundManager.StopManualSound(this, key);
				boolToCheckNewValue = false;
	
				FxManager.ReturnFxInstanceToContainer(fxInstance);
				SoundManager.ReturnSoundInstanceToContainer(soundInstance);
			}
			else 
			{
				GameObject fxInstance = FxManager.EnableFx(this, fxName, sphere.position, Quaternion.identity, key);
	
				GameObject soundInstance = null;
	
				if(setSoundAs3d)
					soundInstance = SoundManager.PlayManualSoundAtPoint(this, soundNameToToggle, key, sphere.position);
				else
					soundInstance = SoundManager.PlayManualSound(this, soundNameToToggle, key);
	
				boolToCheckNewValue = true;
	
				fxInstance.transform.parent = sphere;
				soundInstance.transform.parent = sphere;
			}
	
			return boolToCheckNewValue;
		}
	}
}