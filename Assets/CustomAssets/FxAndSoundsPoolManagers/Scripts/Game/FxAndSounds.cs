﻿using UnityEngine;
using System.Collections;

public class FxAndSounds 
{
	public enum FxsNames
	{
		None,
		Fire,
		Spark,
		FireworksBlue,
		FireworksRed,
		FireworksYellow,
		Wind,
		Flame,
		Rain,
		PlayerHit,
		EnemyHit,
        CrateHit,
        CrateDestroy
	}
	
	public enum SoundsNames
	{
		None,
		Fire,
		Fire3d,
		Spark,
		Spark3d,
		FireworksBlue,
		FireworksBlue3d,
		FireworksRed,
		FireworksRed3d,
		FireworksYellow,
		FireworksYellow3d,
		Wind,
		Flame,
		Rain,
		Hit1,
		Hit2,
		Hit3,
		Swing1,
		Swing2,
		Swing3,
		Jump,
		Land1,
		Land2,
		NotEnoughEnergy,
		ElectricShot,
		Blaze,
		SpeedAttack1,
		SpeedAttack2,
		MinionLaugh1,
		MinionLaugh2,
		MinionAttack1,
		MinionAttack2,
		MinionOuch1,
		MinionOuch2
	}
	
	public enum SongsNames
	{
		None,
		Believe,
		Will,
		TestSong
	}
}
