﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class PoolFx : PoolElement 
{
	private ParticleSystem _particleSystem; 

	void Awake()
	{
		_particleSystem = GetComponent<ParticleSystem>();
	}
	
	void OnEnable()
	{
		_particleSystem.Play();
	}
	
	void Update () 
	{
		if(AutomaticDisable && !_particleSystem.isPlaying)
			gameObject.SetActive(false);
	}
}
