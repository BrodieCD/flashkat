﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PoolSound : PoolElement 
{
	private AudioSource _audio; 

	void Awake()
	{
		_audio = GetComponent<AudioSource>();
	}
	
	void Update () 
	{
		if(AutomaticDisable && !_audio.isPlaying)
			gameObject.SetActive(false);
	}
}
