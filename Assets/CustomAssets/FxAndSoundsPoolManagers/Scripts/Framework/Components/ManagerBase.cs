﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ManagerBase : MonoBehaviour 
{
	protected String _grandContainerName;
	protected Transform _grandContainer;

	protected Dictionary<int, List<GameObject>> _instancesListsByIndex;
	protected Dictionary<int, Transform> _instancesContainersByIndex;
	
	protected Dictionary<object, GameObject> _instancesPerKey = new Dictionary<object, GameObject>(); 

	#if UNITY_EDITOR
	/// <summary>
	/// Checks arrays and displays an error message using array names if they do not have the same length.
	/// </summary>
	/// <param name="array1">First array.</param>
	/// <param name="arrayName1">String name of the first array.</param>
	/// <param name="array2">Second array.</param>
	/// <param name="arrayName2">String name of the second array.</param>
	static protected void checkArrays(Array array1, String arrayName1, Array array2, String arrayName2)
	{
		if(array1.Length != array2.Length)
			Debug.LogError(arrayName1 + " and " + arrayName2 + " arrays need to have the same lenght.");
	}
	#endif

	/// <summary>
	/// Gets random item from the list given.
	/// </summary>
	/// <returns>Random item list.</returns>
	/// <param name="list">List to get random item from.</param>
	/// <typeparam name="T">The item type.</typeparam>
	static protected T getRandomItemFromList<T>(List<T> list)
	{
		return list[UnityEngine.Random.Range(0, list.Count)];
	}	

	protected void initCollections(int lenght)
	{
		_instancesListsByIndex = new Dictionary<int, List<GameObject>> (lenght);
		_instancesContainersByIndex = new Dictionary<int, Transform> (lenght);
		_instancesPerKey = new Dictionary<object, GameObject> (lenght);
	}

	protected void setupListsAndContainers(int poolElementIndex, String nameString, int instancesAmount)
	{
		List<GameObject> newList = new List<GameObject> (instancesAmount);
		_instancesListsByIndex.Add(poolElementIndex, newList);
		
		Transform soundContainer = (new GameObject (nameString + "_container")).transform;
		_instancesContainersByIndex.Add (poolElementIndex, soundContainer);
		soundContainer.parent = _grandContainer;
		
		_instancesPerKey = new Dictionary<object, GameObject> ();
	}
	

	/// <summary>
	/// Returns pool element to its proper container.
	/// </summary>
	/// <param name="instance">FxInstance as a gameObject.</param>
	protected void returnElementToContainer(GameObject poolInstance)
	{
		PoolElement poolElement = poolInstance.GetComponent<PoolElement>();
		
		if(poolElement != null)
			poolInstance.transform.parent = getContainer(poolElement.Index);
		else
			Debug.LogError(poolElement.ToString() + " is not an element instance created by Manager.");
	}

	virtual protected Transform getContainer(int elementIndex) { return null; }
}
