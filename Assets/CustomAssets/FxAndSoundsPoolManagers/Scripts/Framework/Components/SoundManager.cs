using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FxAndSoundsManagersExample;

/// <summary>
/// Used to play one shot sounds, songs and looping sounds that can be tracked and switched on and off.  
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class SoundManager : ManagerBase 
{
	public SoundData[] SoundDatas;
	public SongData[] SongDatas;

	public AudioSource MusicPlayer;

	static private SoundManager _instance;

	private AudioSource _audio; 
	
	private Dictionary<FxAndSounds.SoundsNames, int> _indexBySoundName;
	private Dictionary<FxAndSounds.SongsNames, int> _indexBySongName;


	void Awake () 
	{
		_instance = this;

		_grandContainerName = "SoundManagerContainer";

		_audio = GetComponent<AudioSource>();
		_audio.loop = true;
		_audio.playOnAwake = false;
		_audio.Stop();	

		initializeSoundsAndSongs();
	}

	/// <summary>
	/// Plays one shot sound.
	/// </summary>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="soundName">SoundsNames enum value.</param>
	/// <param name="volume">Volume for this shot. Default -1 will use default sound volume set at inspector.</param>
	static public void PlayOneShotSound(object dispatcher, FxAndSounds.SoundsNames soundName, float volume = -1)
	{
		//Debug.Log("SoundManager::PlayOneShotSound -> dispatcher: " + dispatcher + " soundName: " + soundName.ToString() + " volume: " + volume);   
		_instance.playOneShotSound(soundName, volume);
	}

	/// <summary>
	/// Plays random one shot sound from a provided list of SoundsNames enum values.
	/// </summary>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="soundNames">Sound names.</param>
	/// <param name="volume">Volume for this shot. Default -1 will use default sound volume set at inspector.</param>
	static public void PlayRandomOneShotSoundFromList(object dispatcher, List<FxAndSounds.SoundsNames> soundNames, float volume = -1)
	{
		//Debug.Log("SoundManager::PlayRandomOneShotSoundFromList -> dispatcher: " + dispatcher + " soundNames: " + soundNames.ToString() + " volume: " + volume);   
		_instance.playOneShotSound(getRandomItemFromList<FxAndSounds.SoundsNames>(soundNames), volume);
	}

	/// <summary>
	/// Plays automatic sound. This is, a sound that plays once on the 3d space, and then it is disables in the pool, ready to be enabled again.
	/// </summary>
	/// <param name="soundName">SoundsNames enum value.</param>
	/// <param name="pos">Sound position in the 3d space.</param>
	/// <param name="volume">Volume for this sound play. Default -1 will use default sound volume set at inspector.</param>
	/// <param name="spatialBlend">From 0 to 1, how much 3d space position affects sound. Default -1 will use default spatial blend set at inspector.</param>
	static public void PlayAutomaticSoundAtPoint(object dispatcher, FxAndSounds.SoundsNames soundName, Vector3 pos, float volume = -1, float spatialBlend = -1)
	{
		//Debug.Log("SoundManager::PlayAutomaticSound -> dispatcher: " + dispatcher + " soundName: " + soundName + " pos" + pos.ToString() + " volume: " + volume + " spatialBlend: " + spatialBlend);   
		_instance.playPooledSound(soundName, null, true, pos, volume, spatialBlend);
	}

	/// <summary>
	/// Plays random automatic sound from a provided list of SoundsNames enum values. This is, a sound that plays once on the 3d space, and then it is disables in the pool, ready to be enabled again.
	/// </summary>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="soundName">Sound name.</param>
	/// <param name="pos">Sound position in the 3d space.</param>
	/// <param name="volume">Volume for this sound play. Default -1 will use default sound volume set at inspector.</param>
	/// <param name="spatialBlend">From 0 to 1, how much 3d space position affects sound. Default -1 will use default spatial blend set at inspector.</param>
	static public void PlayRandomAutomaticSoundFromListAtPoint(object dispatcher, List<FxAndSounds.SoundsNames> soundNames, Vector3 pos, float volume = -1, float spatialBlend = -1)
	{
		//Debug.Log("SoundManager::AutomaticSound -> dispatcher: " + dispatcher + " soundNames: " + soundNames.ToString() + " pos" + pos.ToString() + " volume: " + volume + " spatialBlend: " + spatialBlend);   
		_instance.playPooledSound(getRandomItemFromList<FxAndSounds.SoundsNames>(soundNames), null, true, pos, volume, spatialBlend);
	}

	/// <summary>
	/// Plays manual sound. This is, a sound that loops and lasts as long as it is manually stopped, using the StopManualSound method.
	/// </summary>
	/// <returns>Enabled manual sound instance.</returns>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="soundName">SoundsNames enum value.</param>
	/// <param name="key">Key used to keep track of the sound to be stopped later.</param>
	/// <param name="volume">Volume to be set to this sound play. Default -1 will use default sound volume set at inspector.</param>
	/// <param name="spatialBlend">From 0 to 1, how much 3d space position affects sound.</param>
	static public GameObject PlayManualSound(object dispatcher, FxAndSounds.SoundsNames soundName, object key, float volume = 1)
	{
		//Debug.Log("SoundManager::PlayManualSound -> dispatcher: " + dispatcher + " soundName: " + soundName + " key: " + key.ToString() + " volume: " + volume);   
		return _instance.playPooledSound(soundName, key, false, Vector3.zero, volume, 0);
	}

	/// <summary>
	/// Plays manual sound at a point in the 3d space. This is, a sound that loops and lasts as long as it is manually stopped, using the StopManualSound method.
	/// </summary>
	/// <returns>Enabled manual sound instance.</returns>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="soundName">SoundsNames enum value.</param>
	/// <param name="key">Key used to keep track of the sound to be stopped later.</param>
	/// <param name="volume">Volume to be set to this sound play. Default -1 will use default sound volume set at inspector.</param>
	/// <param name="spatialBlend">From 0 to 1, how much 3d space position affects sound. Default -1 will use default spatial blend set at inspector.</param>
	static public GameObject PlayManualSoundAtPoint(object dispatcher, FxAndSounds.SoundsNames soundName, object key, Vector3 pos, float volume = 1, float spatialBlend = -1)
	{
		//Debug.Log("SoundManager::PlayManualSound -> dispatcher: " + dispatcher + " soundName: " + soundName + " key: " + key.ToString() + " pos" + pos.ToString() + " volume: " + volume + " spatialBlend: " + spatialBlend);  
		return _instance.playPooledSound(soundName, key, true, pos, volume, spatialBlend);
	}

	/// <summary>
	/// Stops manual sound using key we used to play it.
	/// </summary>
	/// <returns>Disabled manual sound instance.</returns>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="key">Key used to play sound and keep track of it to stop it.</param>
	static public GameObject StopManualSound(object dispatcher, object key)
	{
		//Debug.Log("SoundManager::StopManualSound -> dispatcher: " + dispatcher + " key: " + key.ToString());  
		return _instance.stopPooledSound(key);
	}
	
	/// <summary>
	/// Plays song. If there is another song playing, it will be replaced.
	/// </summary>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="songName">SongsNames enum value.</param>
	static public void PlayMusic(object dispatcher, FxAndSounds.SongsNames songName)
	{
		//Debug.Log("SoundManager::PlayMusic -> dispatcher: " + dispatcher + " songName: " + songName.ToString());   
		_instance.playMusic(songName);
	}
	
	/// <summary>
	/// Stops music, regarding of which song is being played.
	/// </summary>
	/// <param name="dispatcher">Method caller.</param>
	static public void StopMusic(object dispatcher)  
	{
		//Debug.Log("SoundManager::StopMusic -> dispatcher: " + dispatcher);   
		_instance.stopMusic();
	}

	/// <summary>
	/// Returns sound instance to its proper container.
	/// </summary>
	/// <param name="soundInstance">SoundInstance as a gameObject.</param>
	static public void ReturnSoundInstanceToContainer(GameObject soundInstance)
	{
		_instance.returnElementToContainer(soundInstance);
	}

	/// <summary>
	/// Gets Sound container by SoundName.
	/// </summary>
	/// <returns>Sounds container.</returns>
	/// <param name="soundName">SoundsNames enum value.</param>
	static public Transform GetContainer(FxAndSounds.SoundsNames soundName)
	{
		return _instance.getContainer(soundName);		
	}

	/// <summary>
	/// Initializes sounds and songs. Containers and instances are created to have a pool to reuse. Dictionaries are created to access instances, lists, and containers easily later. 
	/// </summary>
	private void initializeSoundsAndSongs()
	{
		int soundsLength = SoundDatas.Length;
		
		if(soundsLength > 0)
		{	
			_grandContainer = (new GameObject(_grandContainerName)).transform;

			_indexBySoundName = new Dictionary<FxAndSounds.SoundsNames, int>(soundsLength);

			initCollections (soundsLength);
			
			for (int i = 0; i < soundsLength; i++)
			{
				SoundData soundData = SoundDatas[i];
				FxAndSounds.SoundsNames soundName = soundData.Name;
				_indexBySoundName.Add(soundName, i);

				if(soundData.Type != SoundData.Types.OneShot)
				{
					int instancesAmount = soundData.InstancesAmount;
					setupListsAndContainers(i, soundName.ToString(), instancesAmount);

					for( int j = 0; j < instancesAmount; j++)
						addSound(i); 	
				}	
			}

			if(_grandContainer.childCount == 0)
				GameObject.Destroy(_grandContainer.gameObject);	
		}

		int songsAmount = SongDatas.Length;
		_indexBySongName = new Dictionary<FxAndSounds.SongsNames, int>(songsAmount);
		for(int i = 0; i < songsAmount; i++)
			_indexBySongName.Add(SongDatas[i].Name, i);
	}

	/// <summary>
	/// Plays one shot sound.
	/// </summary>
	/// <param name="soundName">SoundsNames enum value.</param>
	/// <param name="volume">Volume for this shot. Default -1 will use default sound volume set at inspector.</param>
	private void playOneShotSound(FxAndSounds.SoundsNames soundName, float volume)
	{
		int soundIndex = getSoundNameIndex(soundName);

		if(soundIndex != -1)
		{
			volume = (volume == -1)? SoundDatas[soundIndex].DefaultVolume : volume;
			_audio.PlayOneShot(SoundDatas[soundIndex].Clip, volume);		
		}
	}
	
	/// <summary>
	/// Plays pooled sound. This is, a sound that is enabled, lasts as long as it is automatically stopped when it stops playing or manually stopped using the StopManualSound method.
	/// </summary>
	/// <returns>Enabled manual sound instance.</returns>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="soundName">SoundsNames enum value.</param>
	/// <param name="key">Key used to keep track of the sound to be stopped later.</param>
	/// <param name="volume">Volume to be set at this sound play. Default -1 will use default sound volume set at inspector. Default -1 will use default spatial blend set at inspector.</param>
	private GameObject playPooledSound(FxAndSounds.SoundsNames soundName, object key, bool is3d, Vector3 pos, float volume, float spatialBlend)
	{
		GameObject soundInstance = null; 

		int soundIndex = getSoundNameIndex(soundName);

		if(soundIndex != -1)
		{
			soundInstance = getNextSound(soundIndex);
	
			if(soundInstance != null)
			{
				soundInstance.transform.position = pos;
				soundInstance.SetActive(true);
		
				AudioSource audio = soundInstance.GetComponent<AudioSource>();
				audio.volume = (volume == -1)? SoundDatas[soundIndex].DefaultVolume : volume;
				audio.spatialBlend = is3d?((spatialBlend == -1)? SoundDatas[soundIndex].DefaultSpatialBlend : spatialBlend) : 0;
				audio.Play();
		
				if(key != null)
					_instancesPerKey.Add(key, soundInstance);
			}
		}
	
		return soundInstance;
	}
	
	/// <summary>
	/// Stops a pooled sound using the same key it was used to play it.
	/// </summary>
	/// <returns>Disabled manual sound instance.</returns>
	/// <param name="dispatcher">Method caller.</param>
	/// <param name="key">Key used to play sound and keep track of it to stop it.</param>
	private GameObject stopPooledSound(object key)
	{
		if(_instancesPerKey.ContainsKey(key))
		{
			GameObject manualSoundInstance = _instancesPerKey[key];
			manualSoundInstance.GetComponent<AudioSource>().Stop();
			manualSoundInstance.SetActive(false);
			_instancesPerKey.Remove(key);

			return manualSoundInstance;
		}
		else
		{
			print("SoundManager::stopPooledSound -> There is no active pooled sound with the " + key.ToString() + " key.");
			return null;
		}
	}
	
	/// <summary>
	/// Plays song. If there is another song playing, it will be replaced.
	/// </summary>
	/// <param name="songName">SongsNames enum value.</param>
	private void playMusic(FxAndSounds.SongsNames songName)
	{	
		if(MusicPlayer == null)
		{	
			print("MusicPlayer has not been assigned. Ignoring play music.");
			return;
		}

		int songIndex = getSongNameIndex(songName);

		if(songIndex != -1)
		{	
			MusicPlayer.clip = SongDatas[songIndex].Clip;
			
			if(!MusicPlayer.isPlaying)
				MusicPlayer.Play();
		}
	}
	
	/// <summary>
	/// Stops music, regarding of song being played. 
	/// </summary>
	/// <param name="songName">Song name to stop.</param>
	private void stopMusic()
	{
		if(_audio.isPlaying)
			_audio.Stop();
	}

	/// <summary>
	/// Creates and adds sound instance, attaching and setting up AudioSource script, parent container. Adds PoolSound script to the instance, feeding it with its appropiate index. The instance is stored into a list according to Sound index.
	/// </summary>
	/// <returns>Added sound instance.</returns>
	/// <param name="soundIndex">Sound index.</param>
	private GameObject addSound(int soundIndex)
	{
		//print("addSound -> soundIndex: " + soundIndex);
		SoundData soundData = SoundDatas[soundIndex];

		String soundInstanceName = soundData.Name.ToString() + "_Snd_" + _instancesListsByIndex[soundIndex].Count.ToString();
		GameObject soundInstance = new GameObject(soundInstanceName);

		soundInstance.SetActive (false);
		soundInstance.transform.parent = _instancesContainersByIndex[soundIndex];

		bool isAutomatic = (soundData.Type == SoundData.Types.Automatic);
		
		AudioSource audioSource = soundInstance.AddComponent<AudioSource> ();
		audioSource.clip = soundData.Clip;
		audioSource.loop = !isAutomatic;

		PoolSound poolSound = soundInstance.AddComponent<PoolSound>();
		poolSound.Index = soundIndex;
		poolSound.AutomaticDisable = isAutomatic;

		_instancesListsByIndex[soundIndex].Add (soundInstance);

		return soundInstance;
	}

	/// <summary>
	/// Gets the proper index of soundName.
	/// </summary>
	/// <returns>The soundName appropiate index.</returns>
	/// <param name="soundName">SoundsNames enum value.</param>
	private int getSoundNameIndex(FxAndSounds.SoundsNames soundName)
	{
		int index = -1;
		
		if(_indexBySoundName.ContainsKey(soundName))
			index = _indexBySoundName[soundName];
		else
			Debug.LogError("Sound Name: " +  soundName.ToString() + " not found. Check if SoundName was added to the SoundNames array.");
		
		return index;
	}

	/// <summary>
	/// Gets the proper index of songName.
	/// </summary>
	/// <returns>The songName appropiate index.</returns>
	/// <param name="soundName">SongsNames enum value.</param>
	private int getSongNameIndex(FxAndSounds.SongsNames songName)
	{
		int index = -1;
		
		if(_indexBySongName.ContainsKey(songName))
			index = _indexBySongName[songName];
		else
			Debug.LogError("Sound Name: " +  songName.ToString() + " not found. Check if SongName was added to the SongNames array.");
		
		return index;
	}

	/// <summary>
	/// Find next pooled sound instance that is available (disabled). If no instance is found, a new one is created and added to the pool.
	/// </summary>
	/// <returns>Available manual sound instance.</returns>
	/// <param name="soundIndex">Index of the sound to get.</param>
	private GameObject getNextSound(int soundIndex)
	{	
		GameObject nextSoundInstance = null;
	
		if(_instancesListsByIndex.ContainsKey(soundIndex))
		{
			List<GameObject> soundList = _instancesListsByIndex[soundIndex];
	
			int soundListAmount = soundList.Count;
			for(int i = 0; i < soundListAmount; i++)
			{
				if(!soundList[i].activeSelf)
				{
					nextSoundInstance = soundList[i];
					break;
				}
			}
	
			if(nextSoundInstance == null)
			{
				Debug.LogWarning("SoundManager::getNextSound -> A sound instance for " + SoundDatas[soundIndex].Name.ToString() + " was not found. A new one will be created and added to the pool.");
				nextSoundInstance = addSound(soundIndex); 
			}
		}
		else
			Debug.LogError("Sound Name: " +  SoundDatas[soundIndex].Name.ToString() + " not found. Check if Sound has de proper type. OneShot sounds cannot be played as automatic or manual");
	
		return nextSoundInstance;
	}

	/// <summary>
	/// Gets Fx container by FxName.
	/// </summary>
	/// <returns>Fx container.</returns>
	/// <param name="soundIndex">SoundIndex index.</param>
	override protected Transform getContainer(int soundIndex)
	{		
		return getContainer(SoundDatas[soundIndex].Name);		
	}
	
	/// <summary>
	/// Gets Sound container by SoundName.
	/// </summary>
	/// <returns>Sounds container.</returns>
	/// <param name="soundName">SoundsNames enum value.</param>
	private Transform getContainer(FxAndSounds.SoundsNames soundName)
	{
		Transform container = null;
		
		int soundIndex = getSoundNameIndex(soundName);		
		if(soundIndex != -1)		
			container = _instancesContainersByIndex[soundIndex];
		
		return container;		
	}
}
