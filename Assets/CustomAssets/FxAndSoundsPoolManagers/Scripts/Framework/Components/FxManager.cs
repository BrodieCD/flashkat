﻿using System;
using System.Collections; 
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Special Effects manager. Displays particles using a pool of instances that are reused.  
/// </summary>
public class FxManager : ManagerBase 
{
	public FxData[] FxDatas;

	private Dictionary<FxAndSounds.FxsNames, int> _indexByFxName;

	static private FxManager _instance;

	void Awake()
	{
		_instance = this;

		_grandContainerName = "FxManagerContainer";
	}

	void Start()
	{
		initializeFXs();
	}

	/// <summary>
	/// Enables Fx by name in the given position and with the given rotation. The key is used to keep track of a manual Fx to be Disabled later.
	/// </summary>
	/// <returns>Enabled Fx instance.</returns>
	/// <param name="dispatcher">Instance that called method.</param>
	/// <param name="fxName">FxNames enum value.</param>
	/// <param name="pos">Position to place fx transform.</param>
	/// <param name="rot">Rotation for the fx transform.</param>
	/// <param name="key">Key to keep track of Fx to be disabled later.</param>
	static public GameObject EnableFx(object dispatcher, FxAndSounds.FxsNames fxName, Vector3 pos, Quaternion rot, object key = null)
	{
		//Debug.Log("SoundManager::EnableFx -> dispatcher: " + dispatcher + " fxName: " + fxName + " pos: " + pos + " rot: " + rot + " key: " + (((key == null)? "null" : key.ToString())));   
		return _instance.enableFx(fxName, pos, rot, key);
	}

	/// <summary>
	/// Enables random fx from list of FxsNames enum values.
	/// </summary>
	/// <returns>Enabled Fx instance.</returns>
	/// <param name="dispatcher">Instance that called method.</param>
	/// <param name="fxNames">Fx name list.</param>
	/// <param name="pos">Position to place fx transform.</param>
	/// <param name="rot">Rotation for the fx transform.</param>
	/// <param name="key">Key to keep track of Fx to be disabled later.</param>
	static public GameObject EnableRandomFxFromList(object dispatcher, List<FxAndSounds.FxsNames> fxNames, Vector3 pos, Quaternion rot, object key = null)
	{
		//Debug.Log("SoundManager::EnableRandomFxFromList -> dispatcher: " + dispatcher + " fxNames: " + fxNames.ToString() + " pos: " + pos + " rot: " + rot + " key: " + ((key == null)? "null" : key.ToString()));   
		return _instance.enableFx(getRandomItemFromList<FxAndSounds.FxsNames>(fxNames), pos, rot, key);
	}

	/// <summary>
	/// Disables a specific Fx by key.
	/// </summary>
	/// <returns>Disabled Fx instance.</returns>
	/// <param name="dispatcher">Instance that called method.</param>
	/// <param name="fxName">Type of Fx to disable, by name.</param>
	/// <param name="key">Key of the Fx to disable.</param>
	static public GameObject DisableFX(object dispatcher, object key)
	{
		//Debug.Log("SoundManager::DisableFX -> dispatcher: " + dispatcher + " key: " + ((key == null)? "null" : key.ToString())); 
		return _instance.disableFx(key);
	}

	/// <summary>
	/// Returns fxInstance to its proper container.
	/// </summary>
	/// <param name="fxInstance">FxInstance as a gameObject.</param>
	static public void ReturnFxInstanceToContainer(GameObject fxInstance)
	{
		_instance.returnElementToContainer(fxInstance);
	}
	
	/// <summary>
	/// Gets Fx container by FxName.
	/// </summary>
	/// <returns>The fx container.</returns>
	/// <param name="fxName">FxNames enum value.</param>
	static public Transform GetFxContainer(FxAndSounds.FxsNames fxName)
	{		
		return _instance.getContainer(fxName);		
	}

	/// <summary>
	/// Enables Fx by name in the given position and with the given rotation. The key is used to keep track of a manual Fx to be Disabled later.
	/// </summary>
	/// <returns>Enabled Fx instance.</returns>
	/// <param name="fxName">FxNames enum value.</param>
	/// <param name="pos">Position to place fx transform.</param>
	/// <param name="rot">Rotation for the fx transform.</param>
	/// <param name="key">Key to keep track of Fx to be disabled later.</param>
	private GameObject enableFx(FxAndSounds.FxsNames fxName, Vector3 pos, Quaternion rot, object key = null)
	{
		int fxIndex = getFxNameIndex(fxName);
		GameObject fxInstance = null;

		if(fxIndex != -1)
		{
			fxInstance = getNextFx(fxIndex);
	
			fxInstance.transform.position = pos;
			fxInstance.transform.rotation = rot;
			fxInstance.SetActive(true);
	
			if(key != null)
				_instancesPerKey.Add(key, fxInstance);
		}
			
		return fxInstance;
	}

	/// <summary>
	/// Disables a specific Fx by key.
	/// </summary>
	/// <returns>Disabled Fx instance.</returns>
	/// <param name="key">Key of the Fx to disable.</param>
	private GameObject disableFx(object key)
	{
		if(_instancesPerKey.ContainsKey(key))
		{
			GameObject fxInstance = _instancesPerKey[key];
			fxInstance.SetActive(false);
			_instancesPerKey.Remove(key);

			return fxInstance;
		}
		else
		{
			print("FxMgr::disableFx -> There is no active FX with the " + key.ToString() + " key.");
			return null;
		}
	}
	
//	#if UNITY_EDITOR
//	/// <summary>
//	/// Checks array length against fx names array and display error message using array names.
//	/// </summary>
//	/// <param name="array">Array to check</param>
//	/// <param name="arrayName">String name of the array to check.</param>
//	private void checkArrayAgainstFxNames(Array array, String arrayName)
//	{
//		checkArrays(Fxs, "Fx_Names", array, arrayName);
//	}
//	#endif

	/// <summary>
	/// Initializes FXs. Containers and instances are created to have a pool to reuse.  ParticleFXdisabler is added to instances to check when particles are done so they are disabled.  Dictionaries are created
	/// to access instances, lists, and containers easily later. 
	/// </summary>
	private void initializeFXs()
	{
		int fxsAmount = FxDatas.Length;

		if(fxsAmount > 0)
		{
			_grandContainer = (new GameObject(_grandContainerName)).transform;
			
			_indexByFxName = new Dictionary<FxAndSounds.FxsNames, int>(fxsAmount);
			initCollections(fxsAmount);
	
			for (int i = 0; i < fxsAmount; i++) 
			{
				FxData fxData = FxDatas[i];
	
				int instancesAmount = fxData.InstancesAmount;

				FxAndSounds.FxsNames fxName = fxData.Name;  
				_indexByFxName.Add(fxName, i);

				setupListsAndContainers(i, fxName.ToString(), instancesAmount);

				for(int j = 0; j < instancesAmount; j++) 
					addFx(i);
			}
		}
	}

	/// <summary>
	/// Gets index of fxName. This index is used to get the proper element from many arrays related to the fx. 
	/// </summary>
	/// <returns>fxName index.</returns>
	/// <param name="fxName">FxNames enum value.</param>
	private int getFxNameIndex(FxAndSounds.FxsNames fxName)
	{
		int index = -1;
		
		if(_indexByFxName.ContainsKey(fxName))
			index = _indexByFxName[fxName];
		else
			Debug.LogError("FxName: " + fxName.ToString() + " not found. Please check if name was fxName was added to the FXs array."); 

		return index;
	}

	/// <summary>
	/// Finds next Fx instance that is available (disabled). If no instance is found, a new one is created and added to the pool.
	/// </summary>
	/// <returns>Available Fx instance.</returns>
	/// <param name="fxIndex">Fx index to get.</param>
	private GameObject getNextFx(int fxIndex)
	{
		GameObject nextFxInstance = null;
		
		List<GameObject> fxList = _instancesListsByIndex[fxIndex];
		
		int fxListAmount = fxList.Count;
		for(int i = 0; i < fxListAmount; i++)
		{
			if(!fxList[i].activeSelf)
			{
				nextFxInstance = fxList[i];
				break;
			}
		}
		
		if(nextFxInstance == null)
		{
			Debug.LogWarning("FxMgr::getNextFx -> An instance of " + FxDatas[fxIndex].Name.ToString() + " was not found. A new one will be created an added to the pool.");
			nextFxInstance = addFx(fxIndex);
		}
		
		return nextFxInstance;
	}

	/// <summary>
	/// Creates and adds Fx instance to the pool, setting up particles, parent container and adds PoolFx script to the instance, feeding it with its appropiate index. The instance is stored into a list according to Fx index.
	/// </summary>
	/// <returns>Added Fx instance.</returns>
	/// <param name="fxIndex">Fx index.</param>
	private GameObject addFx(int fxIndex)
	{
		FxData fxData = FxDatas[fxIndex];

		GameObject newFxInstance = (GameObject)GameObject.Instantiate (fxData.Prefab);
		newFxInstance.name = fxData.Name.ToString() + "_Fx_" + _instancesListsByIndex[fxIndex].Count.ToString();

		ParticleSystem particles = newFxInstance.GetComponent<ParticleSystem>();
		bool isAutomatic = (fxData.Type == FxData.Types.Automatic);

		if(particles == null)
		{
			#if UNITY_EDITOR
			if(isAutomatic)
				Debug.LogWarning("Fx prefabs are required to use the ParticleSystem component to be automatic. Manual disable required for: " + newFxInstance.name);
			#endif
	
			PoolElement poolElement = newFxInstance.AddComponent<PoolElement>();
			poolElement.Index = fxIndex;
			poolElement.AutomaticDisable = false;
		}
		else
		{
			particles.playOnAwake = true;
			particles.loop = !isAutomatic;  
			
			PoolFx poolFx = newFxInstance.AddComponent<PoolFx>();
			poolFx.Index = fxIndex;
			poolFx.AutomaticDisable = isAutomatic;
		}

		newFxInstance.SetActive(false);  
		newFxInstance.transform.SetParent(_instancesContainersByIndex[fxIndex]);
		
		_instancesListsByIndex[fxIndex].Add(newFxInstance);

		return newFxInstance;
	}

	/// <summary>
	/// Gets Fx container by FxName.
	/// </summary>
	/// <returns>The fx container.</returns>
	/// <param name="index">PoolElement index.</param>
	override protected Transform getContainer(int index)
	{		
		return getContainer(FxDatas[index].Name);		
	}
	
	/// <summary>
	/// Gets Fx container by FxName.
	/// </summary>
	/// <returns>The fx container.</returns>
	/// <param name="fxName">FxNames enum value.</param>
	private Transform getContainer(FxAndSounds.FxsNames fxName)
	{
		Transform container = null;
		
		int fxIndex = getFxNameIndex(fxName);		
		if(fxIndex != -1)		
			container = _instancesContainersByIndex[fxIndex];
		
		return container;		
	}
}
