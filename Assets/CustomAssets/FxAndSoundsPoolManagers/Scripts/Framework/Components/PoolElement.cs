﻿using UnityEngine;
using System.Collections;

public class PoolElement : MonoBehaviour 
{
	[HideInInspector]
	public int Index;

	[HideInInspector]
	public bool AutomaticDisable;
}
