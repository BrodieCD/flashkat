﻿using UnityEngine;

[System.Serializable]
public class SongData  
{
	public FxAndSounds.SongsNames Name;
	public AudioClip Clip;
}
