﻿using UnityEngine;

[System.Serializable]
public class FxData
{
	public enum Types
	{
		Automatic,
		Manual
	}

	public FxAndSounds.FxsNames Name = FxAndSounds.FxsNames.None;
	public GameObject Prefab;
	public Types Type = Types.Automatic;	
	public int InstancesAmount = 3;
}
