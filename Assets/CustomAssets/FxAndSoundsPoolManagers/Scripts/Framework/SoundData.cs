﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SoundData
{
	public enum Types
	{
		OneShot,
		Automatic,
		Manual
	}

	public FxAndSounds.SoundsNames Name = FxAndSounds.SoundsNames.None;
	public AudioClip Clip;
	public Types Type = Types.OneShot;
	public int InstancesAmount = 3;
	public float DefaultVolume = 1;
	public float DefaultSpatialBlend = 0;
}
