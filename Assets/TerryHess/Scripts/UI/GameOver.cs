﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class GameOver : MonoBehaviour {
	//test-----
	// remove timer when health is added
	public float timer;
	//----------
	//canvas's
	public Canvas mainCanvas;
	public Canvas gameOverCanvas;
	//-----------
	//components
	Animator gameOverAnimator;
	AudioSource buttonAudio; 

	void Awake () 
	{
		// get audio source component
		buttonAudio = GetComponent <AudioSource> ();
		//find game over canvas
		GameObject gameovercanvas = GameObject.Find ("GameOverCanvas");
		// get game over canvas animator
		gameOverAnimator = gameovercanvas.GetComponent<Animator>();
		// enable main canvas
		mainCanvas.enabled = true;
		// disable main canvas
		gameOverCanvas.enabled = false;
	}

	// Update is called once per frame
	void Update () 
	{
		//----------
		//remove timer and timer if statement when health is added
		timer += Time.deltaTime;
		if(timer > 5)
		{
			mainCanvas.enabled = false;
			gameOverCanvas.enabled = true;
			gameOverAnimator.SetTrigger ("GameOver");
		}
		//----------
		//Add if health is 0 or less in if statement
		//if(/*Add Health is 0 or Less*/)
		//{
		//	mainCanvas.enabled = false;
		//	gameOverCanvas.enabled = true;
		//	gameOverAnimator.SetTrigger ("GameOver");
		//}
	}
	//------------------------------------------------
	// ButtonFunctions
	//plays audio when button it pressed
	public void ClickSound()
	{
		buttonAudio.Play ();
	}
	//loads level string name 
	public void LoadNextLevel(string name)
	{
		StartCoroutine (LevelLoad (name));
	}
	// load levelafter one second delay
	IEnumerator LevelLoad(string name)
	{
		yield return new WaitForSeconds(1f);
		SceneManager.LoadScene(name);
	}
	//quits game 
	public void OnQuit() 
	{
		Application.Quit ();
	}
	// restart current scene
	public void RestartScene()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name);
	}
}
